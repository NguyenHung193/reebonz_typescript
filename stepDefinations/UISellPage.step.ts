import { Given, When, Then } from "cucumber";
import { browser, protractor, $, by, element } from "protractor";
import { register } from "../pageObjects/register";
import chai from "chai";
import { random } from "./random";
import { basePage } from "../pageObjects/basePage";
import { womanBagsPage } from "../pageObjects/womanBagsPage";
import { addressProfilePage } from "../pageObjects/addressProfilePage";
import { titanPage } from "../pageObjects/titanPage";
import { async } from "q";
import { myAccountPage } from "../pageObjects/myAccountPage";
import { sellPage } from "../pageObjects/sellPage";
var expect = chai.expect;
let registerPage = new register();
let rd = new random();
let base = new basePage();
let womanPage = new womanBagsPage();
let addressPage = new addressProfilePage();
let titan = new titanPage();
let MyAccount = new myAccountPage();
let sell = new sellPage();

Given("I will navigate to Reebonz site {string}", async string => {
  await browser.waitForAngularEnabled(false);
  await browser.get(registerPage.UATURL + string);
  let isVisible = await womanPage.userName.isPresent();
  if (isVisible) {
    // element is visible
    await browser
      .actions()
      .mouseMove(womanPage.userName)
      .perform();
    await base.checkElementVisible(womanPage.logoutButton);
    await base.waitForElementAndClick(womanPage.logoutButton);
  } else {
    // element is not visible
  }
});

When("I hover on Sell Menu {string}", async string => {
  if (string !== "Viet Nam") {
    await base.checkElementVisible(sell.sellPageMenu);
    await browser
      .actions()
      .mouseMove(sell.sellPageMenu)
      .perform();
    // await browser.sleep(5000);
  } else {
  }
});

When("I click on Sell menu {string}", async string => {
  if (
    string === "Singapore" ||
    string === "Australia" ||
    string === "Indonesia" ||
    string === "Malaysia"
  ) {
    await base.waitForElementAndClick(sell.sellPageMenu);
  } else {
    await console.log("I assert label");
    await base.waitForElement(sell.menuClearfix);
    await base.checkElementVisible(sell.sellbackHeader);
    await base.checkElementDisplay(sell.sellbackHeader);
  }
});

Then("I direct to Sell service page {string}", async string => {
  if (
    string === "Singapore" ||
    string === "Australia" ||
    string === "Indonesia" ||
    string === "Malaysia"
  ) {
    await base.waitForUrlAndAssert("sell-services");
  } else {
  }
});

Then("I assert Sell service element {string}", async string => {
  if (
    string === "Singapore" ||
    string === "Australia" ||
    string === "Malaysia"
  ) {
    await base.waitForElement(sell.benefitLabel);
    await base.checkElementVisible(sell.benefitLabel);
    await base.checkElementVisible(sell.sellBackLabel);
    await base.checkElementVisible(sell.sellServiceSellBackButton);
    await base.checkElementVisible(sell.sellServiceSellNowButton);
    await base.checkElementVisible(sell.sellServiceWhiteGlovesButton);
    await base.checkElementVisible(sell.sellToReebonzLabel);
    await base.checkElementVisible(sell.whiteGloveLabel);
  }
  if (string === "Indonesia") {
    await base.waitForElement(sell.benefitLabel);
    await base.checkElementVisible(sell.benefitLabel);
    await base.checkElementVisible(sell.sellBackLabel);
    await base.checkElementVisible(sell.sellServiceSellBackButton);
    await base.checkElementVisible(sell.sellServiceSellNowButton);
    await base.checkElementVisible(sell.sellToReebonzLabel);
  }
});

When("I click on SellNow button {string}", async string => {
  if (
    string === "Singapore" ||
    string === "Australia" ||
    string === "Indonesia" ||
    string === "Malaysia"
  ) {
    await base.closePopUpCookiesFunction();
    await base.waitForElementAndClick(sell.sellServiceSellNowButton);
  }
});

When("I direct to Sell product listing page {string}", async string => {
  if (
    string === "Singapore" ||
    string === "Australia" ||
    string === "Indonesia" ||
    string === "Malaysia"
  ) {
    await base.waitForElement(sell.titleSellListingPage);
  }
});

When("I click on first product {string}", async string => {
  if (
    string === "Singapore" ||
    string === "Australia" ||
    string === "Indonesia" ||
    string === "Malaysia"
  ) {
    await base.waitForElementAndClick(sell.firstProduct);
  }
});

When("I direct to Sell product details page {string}", async string => {
  if (
    string === "Singapore" ||
    string === "Australia" ||
    string === "Indonesia" ||
    string === "Malaysia"
  ) {
    await base.waitForElement(sell.sellNote);
  }
});

When("I click on Sell to Reebonz button {string}", async string => {
  if (
    string === "Singapore" ||
    string === "Australia" ||
    string === "Indonesia" ||
    string === "Malaysia"
  ) {
    await base.waitForElementAndClick(sell.sellToReebonzPDP);
  }
});

When("I choose first Condition {string}", async string => {
  if (
    string === "Singapore" ||
    string === "Australia" ||
    string === "Indonesia" ||
    string === "Malaysia"
  ) {
    await base.waitForElement(sell.conditionModal);
    await base.waitForElementAndClick(sell.firstCondition);
  }
});

When("I click on Sell Now button on PDP {string}", async string => {
  if (
    string === "Singapore" ||
    string === "Australia" ||
    string === "Indonesia" ||
    string === "Malaysia"
  ) {
    await base.waitForElementAndClick(sell.sellNowButtonConditionModal);
  }
});

Then("I direct to Sell confimation page {string}", async string => {
  if (
    string === "Singapore" ||
    string === "Australia" ||
    string === "Indonesia" ||
    string === "Malaysia"
  ) {
    await base.waitForElement(sell.itemSummaryLabel);
  }
});

When("I choose drop off method {string}", async string => {
  if (
    string === "Singapore" ||
    string === "Indonesia" ||
    string === "Malaysia"
  ) {
    await base.waitForElementAndClick(sell.dropOffOption);
  }
  if (string === "Australia") {
    await base.waitForElementAndClick(sell.sendInMethod);
  }
});

When("I select drop off date {string}", async string => {
  if (
    string === "Singapore" ||
    string === "Indonesia" ||
    string === "Malaysia"
  ) {
    await base.waitForElementAndSendkeys(sell.dropOffDate, "T");
  }
});

When("I select drop off time {string}", async string => {});

When("I enter phone number {string}", async string => {});

When("I select Terms and Conditions {string}", async string => {
  if (
    string === "Singapore" ||
    string === "Australia" ||
    string === "Indonesia" ||
    string === "Malaysia"
  ) {
    await base.waitForElementAndClick(sell.termAndConditions);
  }
});

When(
  "I click on Sell Now button on confirmation page {string}",
  async string => {
    if (
      string === "Singapore" ||
      string === "Australia" ||
      string === "Indonesia" ||
      string === "Malaysia"
    ) {
      await base.waitForElementAndClick(sell.sellNowButtonOnConfirmationPage);
    }
  }
);

Then("I direct to Sell submitted page {string}", async string => {
  if (
    string === "Singapore" ||
    string === "Australia" ||
    string === "Indonesia" ||
    string === "Malaysia"
  ) {
    await base.waitForElement(sell.sellSubmmitedLabel);
  }
});

When("I enter {string} on search box {string}", async (string, string2) => {
  if (
    string2 === "Singapore" ||
    string2 === "Australia" ||
    string2 === "Indonesia" ||
    string2 === "Malaysia"
  ) {
    await base.waitForElementAndSendkeys(sell.searchBox, string);
    await sell.searchBox.sendKeys(protractor.Key.ENTER);
  }
});

Then("I direct to {string} page {string}", async (string, string2) => {
  if (
    string2 === "Singapore" ||
    string2 === "Australia" ||
    string2 === "Indonesia" ||
    string2 === "Malaysia"
  ) {
    await base.waitForElementAndGetText(sell.firstProductBrand, string);
    await browser.sleep(50000);
  }
});
