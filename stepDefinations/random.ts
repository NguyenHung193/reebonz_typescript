export class random {
  random: string;
  randomEmail: string;
  constructor() {
    this.random = Math.random()
      .toString(36)
      .replace(/[^a-z]+/g, "")
      .substr(0, 7);

    this.randomEmail = "bi.nguyen+" + this.random + "@reebonz.com";
  }
}
