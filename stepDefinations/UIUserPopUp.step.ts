import { Given, When, Then } from "cucumber";
import { browser, protractor, $, by, element } from "protractor";
import { register } from "../pageObjects/register";
import chai from "chai";
import { random } from "./random";
import { basePage } from "../pageObjects/basePage";
import { womanBagsPage } from "../pageObjects/womanBagsPage";
import { addressProfilePage } from "../pageObjects/addressProfilePage";
import { titanPage } from "../pageObjects/titanPage";
import { async } from "q";
import { myAccountPage } from "../pageObjects/myAccountPage";
import { UIUserPopUp } from "../pageObjects/UIUserPopUp";
var expect = chai.expect;
let registerPage = new register();
let rd = new random();
let base = new basePage();
let womanPage = new womanBagsPage();
let addressPage = new addressProfilePage();
let titan = new titanPage();
let MyAccount = new myAccountPage();
let UserPopUp = new UIUserPopUp();

When("I hover userName", async () => {
  base.waitForElement(womanPage.userName);
  let isVisible = await womanPage.userName.isPresent();
  if (isVisible) {
    // element is visible
    await browser
      .actions()
      .mouseMove(womanPage.userName)
      .perform();
    await base.waitForElement(MyAccount.userName);
  } else {
    await console.log("element is not visible");
    // element is not visible
  }
});

Then("I am checking order", async () => {
  await base.waitForElement(UserPopUp.orders);
  await base.checkElementVisible(UserPopUp.orders);
});

Then("I am checking sellback based on {string}", async string => {
  if (string === "Singapore") {
    await base.waitForElement(UserPopUp.sellback);
    await base.checkElementVisible(UserPopUp.sellback);
  }
});

Then("I am checking white glove based on {string}", async string => {
  if (string === "Singapore") {
    await base.waitForElement(UserPopUp.whiteGlove);
    await base.checkElementVisible(UserPopUp.whiteGlove);
  }
});

Then("I am checking credits", async () => {
  await base.waitForElement(UserPopUp.credits);
  await base.checkElementVisible(UserPopUp.credits);
});

Then("I am checking wishlist", async () => {
  await base.waitForElement(UserPopUp.wishlist);
  await base.checkElementVisible(UserPopUp.wishlist);
});

Then("I am checking address", async () => {
  await base.waitForElement(UserPopUp.address);
  await base.checkElementVisible(UserPopUp.address);
});

Then("I am checking inbox", async () => {
  await base.waitForElement(UserPopUp.inbox);
  await base.checkElementVisible(UserPopUp.inbox);
});

Then("I am checking boutique | follow", async () => {
  await base.waitForElement(UserPopUp.boutique);
  await base.checkElementVisible(UserPopUp.boutique);
});
