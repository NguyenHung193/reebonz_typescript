import { Given, When, Then } from "cucumber";
import { browser, protractor, $, by, element } from "protractor";
import { register } from "../pageObjects/register";
import chai from "chai";
import { random } from "../stepDefinations/random";
import { basePage } from "../pageObjects/basePage";
import { womanBagsPage } from "../pageObjects/womanBagsPage";
import { addressProfilePage } from "../pageObjects/addressProfilePage";
import { titanPage } from "../pageObjects/titanPage";
import { async } from "q";
import { myAccountPage } from "../pageObjects/myAccountPage";
var expect = chai.expect;
let registerPage = new register();
let rd = new random();
let base = new basePage();
let womanPage = new womanBagsPage();
let addressPage = new addressProfilePage();
let titan = new titanPage();
let MyAccount = new myAccountPage();

When("I hover userName and click on userName Popup", async () => {
  let unVisible = await MyAccount.label.isPresent();
  if (!unVisible) {
    let isVisible = await womanPage.userName.isPresent();
    if (isVisible) {
      // element is visible
      await browser
        .actions()
        .mouseMove(womanPage.userName)
        .perform();
      await base.checkElementVisible(MyAccount.userName);
      await base.waitForElementAndClick(MyAccount.userName);
    } else {
      await console.log("element is not visible");
      // element is not visible
    }
  } else {
    //do the next step
  }
});

Then("I direct to my account page", async () => {
  await base.checkElementVisible(MyAccount.label);
});

Then("I am checking element on Myaccount page", async () => {
  await base.checkElementVisible(MyAccount.firstName);
  await base.checkElementVisible(MyAccount.lastName);
  await base.checkElementVisible(MyAccount.gender);
  await base.checkElementVisible(MyAccount.language);
  await base.checkElementVisible(MyAccount.newPassword);
  await base.checkElementVisible(MyAccount.oldPassword);
  await base.checkElementVisible(MyAccount.phoneNumber);
  await base.checkElementVisible(MyAccount.email);
  await base.checkElementVisible(MyAccount.country);
});

Then("I am checking fistName on Myaccount page {string}", async string => {
  await base.checkElementVisible(MyAccount.firstName);
  await base.getElementValue(MyAccount.firstName, string);
});

Then("I am checking lastName on Myaccount page {string}", async string => {
  await base.checkElementVisible(MyAccount.lastName);
  await base.getElementValue(MyAccount.lastName, string);
});

Then("I am checking country on Myaccount page  {string}", async string => {
  await base.checkElementVisible(MyAccount.country);
  await base.getElementValue(MyAccount.country, string);
});

Then("I am checking language on Myaccount page {string}", async string => {
  await base.checkElementVisible(MyAccount.language);
  await base.getElementValue(MyAccount.language, string);
});

Then("I am checking email on Myaccount page {string}", async string => {
  await base.checkElementVisible(MyAccount.email);
  await base.getElementValue(MyAccount.email, string);
});
