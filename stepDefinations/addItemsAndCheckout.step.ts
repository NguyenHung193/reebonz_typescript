import { Given, Then, When } from "cucumber";
import { browser } from "protractor";
import { womanBagsPage } from "../pageObjects/womanBagsPage";
import { basePage } from "../pageObjects/basePage";
import { productDetailsPage } from "../pageObjects/productDetailsPage";
import { checkoutPage } from "../pageObjects/checkoutPage";
import { thankyouPage } from "../pageObjects/thankyouPage";
import { async } from "q";
import chai from "chai";

var expect = chai.expect;
let womanPage = new womanBagsPage();
let base = new basePage();
let productPage = new productDetailsPage();
let checkout = new checkoutPage();
let thankyou = new thankyouPage();

//<-------------------------------------//--------------------------------------->>
//add items steps
Given("Direct to woman bags page", async () => {
  await browser.waitForAngularEnabled(false);
  await browser.get(womanPage.womanBagsUrl);
  await base.popUpCookies.isPresent().then(async isVisible => {
    if (isVisible) {
      // element is visible
      await base.closePopUpCookies();
    } else {
      // element is not visible
    }
  });
});

When("Click on product", async () => {
  // await base.waitForElementAndClick(womanPage.product);
});

When("Direct to product details page and check basketQty", async () => {});

When("Add product into basketQty", async () => {
  await base.waitForElement(checkout.shoppingBag);
  let bagQty = await base.checkElementIsPresentAndConvertToNumber(
    checkout.shoppingBag
  );
  await console.log("+++++++++++" + bagQty);

  if (bagQty !== 0) {
    await browser.sleep(1000);
  } else {
    for (let i = 1; i < 100; i++) {
      await base.waitForElementAndClick(womanPage.productN(i));
      await base.waitForElement(productPage.productName);
      let isVisible = await productPage.addToBasketButton.isPresent();
      // let a = isVisible;
      if (isVisible) {
        // element is visible

        await base.waitForElementAndClick(productPage.addToBasketButton);
        return;
      } else {
        // element is not visible
        await browser.get(womanPage.womanBagsUrl);
      }
    }
  }
});

When("Hover over basket icon", async () => {
  await browser
    .actions()
    .mouseMove(womanPage.basketQty)
    .perform();
});

When("Click on checkout button on popup", async () => {
  // await base.checkElementVisible(womanPage.viewBagButton);
  await base.waitForElement(womanPage.checkoutPopupButton);
  await base.waitForElementAndClick(womanPage.checkoutPopupButton);
});

Then("Direct to checkout page", async () => {
  await browser.getCurrentUrl().then(async result => {
    await expect(result).contain("checkout");
  });
  // await base.waitForElementAndGetText(checkout.checkoutTitle,"CHECKOUT")
});

//<-------------------------------------//--------------------------------------->>
//checkout steps

When("Select payment method", async () => {
  await base.waitForElementAndClick(checkout.selectPaymentMethod);
});

When("Choose Banktransfer method", async () => {
  await base.waitForElementAndClick(checkout.banktransfer);
});

When("Choose CreditCard method", async () => {
  await base.waitForElementAndClick(checkout.creditDebitCard);
});

When("I enter CardNumber", async () => {
  await base.waitForElementAndSendkeys(checkout.cardNumber, "4111111111111111");
});

When("I enter expiry date", async () => {
  await base.waitForElementAndSendkeys(checkout.expiryDate, "0330");
});

When("I enter security code", async () => {
  await base.waitForElementAndSendkeys(checkout.securityCode, "737");
});

When("I enter cardholder name", async () => {
  await base.waitForElementAndSendkeys(checkout.cardHolderName, "Bi Nguyen");
});

When("Save payment method", async () => {
  await base.waitForElementAndClick(checkout.saveMethodButton);
});

//apply credit at checkout page
When("Apply credits", async () => {
  await base.waitForElement(checkout.checkCredits);
  let isSelected = await checkout.checkCredits.isSelected();
  if (!isSelected) {
    await base.waitForElementAndClick(checkout.checkCredits);
  let isSelect = await checkout.checkCredits.isSelected();
  if (!isSelect) {
    await base.waitForElementAndClick(checkout.checkCredits);
  }
  }
  
});
When("Apply policy", async () => {
  await base.waitForElementAndClick(checkout.policy);
  let isSelect = await checkout.policy.isSelected();
  if (!isSelect) {
    await base.waitForElementAndClick(checkout.policy);
  }

  let SubTotal = 0;
  let credits = 0;
  let discountCode = 0;
  let totalCheckout = 0;
  let shippingFee = 0;

  SubTotal = await base.checkElementIsPresentAndConvertToNumber(
    checkout.SubTotal
  );
  await console.log("subtotal" + SubTotal);

  totalCheckout = await base.checkElementIsPresentAndConvertToNumber(
    checkout.totalCheckout
  );
  await console.log("totalCheckout" + totalCheckout);

  discountCode = await base.checkElementIsPresentAndConvertToNumber(
    checkout.discountCode
  );
  await console.log("discountCode" + discountCode);

  credits = await base.checkElementIsPresentAndConvertToNumber(
    checkout.creditApply
  );
  await console.log("credits" + credits);

  shippingFee = await base.checkElementIsPresentAndConvertToNumber(
    checkout.shippingFee
  );
  await console.log("shippingFee" + shippingFee);

  await expect(totalCheckout).to.equal(
    SubTotal + shippingFee - credits - discountCode
  );
});

When("Click on Comfirm and Pay button", async () => {
  await base.waitForElementAndClick(checkout.confirmAndPayButton);
  await browser.sleep(3000);
  await checkout.alertPopup.isPresent().then(async isVisible => {
    if (isVisible) {
      // element is visible
      // await base.waitForElementAndClick(checkout.closePopup);
      await browser.refresh();
      await base.waitForElementAndClick(checkout.policy);
      await base.waitForElementAndClick(checkout.confirmAndPayButton);
    } else {
      // element is not visible
    }
  });
});

Then("Payment successful", async () => {
  await base.waitForUrlAndAssert("thankyou");
  // await browser.manage().deleteAllCookies();
  // await browser.get("https://uat-consumer-api.reebonz-dev.com/api/cache/flush")
});

//<-------------------------------------//--------------------------------------->>
