// import { Given, When, Then } from "cucumber";
// import { browser, protractor, $, by } from "protractor";
// import { register } from "../pageObjects/register";
// import chai from "chai";
// import { random } from "../stepDefinations/random";
// import { basePage } from "../pageObjects/basePage";
// import { womanBagsPage } from "../pageObjects/womanBagsPage";
// import { addressProfilePage } from "../pageObjects/addressProfilePage";
// import { titanPage } from "../pageObjects/titanPage";
// var expect = chai.expect;
// let registerPage = new register();
// let rd = new random();
// let base = new basePage();
// let womanPage = new womanBagsPage();
// let addressPage = new addressProfilePage;
// let titan = new titanPage;

//   Given('Direct to Titan page', async ()=> {
//     await browser.get(titan.titanCreditsUrl);
//   });

//   Then('I click on Login button at Titan page', async ()=> {
//     await base.waitForElementAndClick(titan.loginButton);
//   });

//   Then('I enter email for Titan', async ()=> {
//     await base.waitForElementAndSendkeys(titan.userName,"bi.nguyen@reebonz.com")
//   });

//   Then('I enter password for Titan', async ()=> {
//     await base.waitForElementAndSendkeys(titan.password,"Enouvo123")
//   });

//   Then('I click on next button', async ()=> {
//     await browser.actions().sendKeys(protractor.Key.ENTER).perform();
//   });

//   Then('I enter email address to add credits', async ()=> {
//     await base.waitForElementAndSendkeys(titan.emailToAddcredits, rd.randomEmail)
//   });

//   Then('I click on search button on Titan page', async ()=> {
//     await base.waitForElementAndClick(titan.searchButton)
//   });

//   Then('I click on add button on Titan page', async ()=> {
//     await base.waitForElementAndClick(titan.addButton)
//   });

//   Then('the add credit popup is shown', async ()=> {
//     await browser.sleep(5000)
//   });

//   Then('I select Category on Titan page', async ()=> {
//     await base.waitForElementAndSendkeys(titan.selectCategory,"Credit Refund")
//   });

//   Then('I add credits on Titan page', async ()=> {
//     await base.waitForElementAndSendkeys(titan.credits,"500")
//   });

//   Then('I click on submit credits button', async ()=> {
//     await base.waitForElementAndClick(titan.submitButton)
//   });

//   Then('I add credits successful', async ()=> {
//     await base.waitForElementToCheckContainText(titan.assertCredits,"500")
//   });
