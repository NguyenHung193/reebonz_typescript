import { Given, When, Then } from "cucumber";
import { browser, protractor, $, by, element } from "protractor";
import { register } from "../pageObjects/register";
import chai from "chai";
import { random } from "./random";
import { basePage } from "../pageObjects/basePage";
import { womanBagsPage } from "../pageObjects/womanBagsPage";
import { addressProfilePage } from "../pageObjects/addressProfilePage";
import { titanPage } from "../pageObjects/titanPage";
import { async } from "q";
import { myAccountPage } from "../pageObjects/myAccountPage";
var expect = chai.expect;
let registerPage = new register();
let rd = new random();
let base = new basePage();
let womanPage = new womanBagsPage();
let addressPage = new addressProfilePage();
let titan = new titanPage();
let MyAccount = new myAccountPage();

When("I hover userName and click on address Popup", async () => {
  let unVisible = await addressPage.addressLabel.isPresent();
  if (!unVisible) {
    let isVisible = await womanPage.userName.isPresent();
    if (isVisible) {
      // element is visible
      await browser
        .actions()
        .mouseMove(womanPage.userName)
        .perform();
      await base.checkElementVisible(womanPage.address);
      await base.waitForElementAndClick(womanPage.address);
    } else {
      await console.log("element is not visible");
      // element is not visible
    }
  } else {
    //do the next step
  }
});

Then("I direct to my address page", async () => {
  await base.waitForElement(addressPage.addressLabel);
});

Then("I am checking exist name on Address page {string}", async string => {
  await base.waitForElementAndGetText(addressPage.subName, string);
});

Then("I am checking exist address on Address page {string}", async string => {
  await base.waitForElementAndGetText(addressPage.subAddress, string);
});

Then("I am checking exist country on Address page  {string}", async string => {
  await base.waitForElementAndGetText(addressPage.subCountry, string);
});

Then(
  "I am checking post code on Address page {string} for {string}",
  async (string, string2) => {
    if (string2 !== "Hong Kong") {
      await base.waitForElementAndGetText(addressPage.subPostCode, string);
    }
  }
);

Then("I am checking exist phone on Address page {string}", async string => {
  await base.waitForElementAndGetText(addressPage.subPhone, string);
});
