import { After, Before, Status } from "cucumber";
import { browser } from "protractor";
import { async } from "q";
import { basePage } from "../pageObjects/basePage";
import { womanBagsPage } from "../pageObjects/womanBagsPage";

let base = new basePage();
let womanPage = new womanBagsPage();
Before({ tags: "@calculatortesting" }, function() {
  // This hook will be executed before scenarios tagged with @foo
  browser
    .manage()
    .window()
    .maximize();
});

After({ tags: "@calculatortesting" }, function() {
  // This hook will be executed before scenarios tagged with @foo
  console.log("Test is completed");
});

Before({ tags: "@registerAndAddress" }, async () => {
  await browser.waitForAngularEnabled(false);
  await browser.manage().deleteAllCookies();
  // await browser.executeScript("window.sessionStorage.clear()");
  // await browser.executeScript("window.localStorage.clear()");
  await browser.refresh();
});

Before({ tags: "@checkoutUsingCreditCard" }, async () => {
  await browser.waitForAngularEnabled(false);
  await browser.manage().deleteAllCookies();
  await browser.refresh();
});

Before({ tags: "@checkoutUsingFullCredits" }, async () => {
  await browser.waitForAngularEnabled(false);
  await browser.manage().deleteAllCookies();
  // await browser.executeScript('window.sessionStorage.clear();');
  // await browser.executeScript('window.localStorage.clear();');
  await browser.refresh();
});

Before({ tags: "@LoginFunction" }, async () => {
  await browser.waitForAngularEnabled(false);
  await browser.manage().deleteAllCookies();
  // await browser.executeScript('window.sessionStorage.clear();');
  // await browser.executeScript('window.localStorage.clear();');
  await browser.refresh();
});

Before({ tags: "@loginWithExistUser" }, async () => {
  await browser.waitForAngularEnabled(false);
  await browser.manage().deleteAllCookies();
  // await browser.executeScript('window.sessionStorage.clear();');
  // await browser.executeScript('window.localStorage.clear();');
  await browser.refresh();
});

Before({ tags: "@default" }, async () => {
  await browser.waitForAngularEnabled(false);
  await browser.manage().deleteAllCookies();
  // await browser.executeScript('window.sessionStorage.clear();');
  // await browser.executeScript('window.localStorage.clear();');
  await browser.refresh();
});

Before({ tags: "@UIMyAccountPage" }, async () => {
  await browser.waitForAngularEnabled(false);
  await browser.manage().deleteAllCookies();
  // await browser.executeScript('window.sessionStorage.clear();');
  // await browser.executeScript('window.localStorage.clear();');
  await browser.refresh();
});

Before({ tags: "@UIAddressPageExistUser" }, async () => {
  await browser.waitForAngularEnabled(false);
  await browser.manage().deleteAllCookies();
  // await browser.executeScript('window.sessionStorage.clear();');
  // await browser.executeScript('window.localStorage.clear();');
  await browser.refresh();
});

Before({ tags: "@UIAddressPageNewUser" }, async () => {
  await browser.waitForAngularEnabled(false);
  await browser.manage().deleteAllCookies();
  // await browser.executeScript('window.sessionStorage.clear();');
  // await browser.executeScript('window.localStorage.clear();');
  await browser.refresh();
});

After(async function(scenario) {
  // This hook will be executed before scenarios tagged with @foo
  console.log("Test is completed");
  if (scenario.result.status === Status.FAILED) {
    //code to take screesnhot
    const screenshot = await browser.takeScreenshot();

    this.attach(screenshot, "image/png");
  }
});
