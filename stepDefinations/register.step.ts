import { Given, When, Then } from "cucumber";
import { browser, protractor, $, by, element } from "protractor";
import { register } from "../pageObjects/register";
import chai from "chai";
import { random } from "../stepDefinations/random";
import { basePage } from "../pageObjects/basePage";
import { womanBagsPage } from "../pageObjects/womanBagsPage";
import { addressProfilePage } from "../pageObjects/addressProfilePage";
import { titanPage } from "../pageObjects/titanPage";
import { async } from "q";
var expect = chai.expect;
let registerPage = new register();
let rd = new random();
let base = new basePage();
let womanPage = new womanBagsPage();
let addressPage = new addressProfilePage();
let titan = new titanPage();

//<-------------------------------------//--------------------------------------->>
//login steps

When("I click on Login button", async () => {
  await base.waitForElementAndClick(registerPage.loginButton);
});

Then("The Login popup is displayed", async () => {
  await base.waitForElementAndGetText(registerPage.loginTitle, "Sign In");
});

When("I enter email user {string}", async string => {
  await base.waitForElementAndSendkeys(
    registerPage.loginEmail,
    "bi.nguyen+" + string + rd.random + "@reebonz.com"
  );
});

When("I enter user {string} for {string}", async (string, string2) => {
  await base.waitForElementAndSendkeys(registerPage.loginEmail, string);
});

Then("I enter password - login page", async () => {
  await base.waitForElementAndSendkeys(registerPage.loginPassword, "123456");
});

When("I click on login submit", async () => {
  await base.waitForElementAndClick(registerPage.loginSubmit);
});

Then("I login successfully", async () => {
  await browser.sleep(1000);
  await base.waitForElementAndGetText(womanPage.userName, "Bi");
});

Then("It shows nameError", async () => {
  await base.checkElementVisible(registerPage.userNameError);
});

Then("It shows passwordError", async () => {
  await base.checkElementVisible(registerPage.passwordError);
});

//<-------------------------------------//--------------------------------------->>
//register steps

Given("I will navigate to Reebonz site", async () => {
  await browser.waitForAngularEnabled(false);
  await browser.get(registerPage.UATURL);

  let isVisible = await womanPage.userName.isPresent();
  if (isVisible) {
    // element is visible
    await browser
      .actions()
      .mouseMove(womanPage.userName)
      .perform();
    await base.checkElementVisible(womanPage.logoutButton);
    await base.waitForElementAndClick(womanPage.logoutButton);
  } else {
    // element is not visible
  }
});

When("I click on register button", async () => {
  await base.waitForElementAndClick(registerPage.registerButton);
});

Then("The register popup is displayed", async () => {
  await base.waitForElementAndGetText(registerPage.registerTitle, "Register");
});

When("I enter FirstName", async () => {
  await base.waitForElementAndSendkeys(registerPage.firstName, "Bi");
});
When("I enter LastName", async () => {
  await base.waitForElementAndSendkeys(registerPage.lastName, rd.random);
});
//enter mutiple lastName - outline
When("I enter lastName {string}", async string => {
  await base.waitForElementAndSendkeys(
    registerPage.lastName,
    string + rd.random
  );
});

When("I enter email address", async () => {
  await base.waitForElementAndSendkeys(
    registerPage.emailAddress,
    rd.randomEmail
  );
  await console.log(rd.randomEmail);
});
//enter mutilple email - outline
When("I enter email {string}", async string => {
  await base.waitForElementAndSendkeys(
    registerPage.emailAddress,
    "bi.nguyen+" + string + rd.random + "@reebonz.com"
  );
});
When("I enter password", async () => {
  await base.waitForElementAndSendkeys(registerPage.password, "123456");
});
When("I select country", async () => {
  await base.waitForElementAndSendkeys(registerPage.country, "Singapore");
});
//mutilpe countries - outline
When("I select {string}", async countries => {
  await base.waitForElementAndSendkeys(registerPage.country, countries);
});
When("I click on Register button", async () => {
  await base.waitForElementAndClick(registerPage.submit);
});
Then("I register successful", async () => {
  await base.waitForElementAndGetText(womanPage.userName, "Bi");
});

Then("I am checking WhatsApp based on {string}", async string => {
  if (
    string === "Singapore" ||
    string === "Australia" ||
    string === "Indonesia" ||
    string === "HongKong"
  ) {
    await base.checkElementVisible(womanPage.whatsApp);
    await base.checkElementHavingHref(womanPage.whatsApp);
  } else {
    await base.checkElementInVisible(womanPage.whatsApp);
  }
});

//<-------------------------------------//--------------------------------------->>
//add shipment address steps

Given("I logged in successful", async () => {
  await base.waitForElementAndGetText(womanPage.userName, "Bi");
});

When("I am navigate to addAddress page", async () => {
  await browser.get("https://uat-www.reebonz-dev.com/sg/account/addresses");
  await base.popUpCookies.isPresent().then(async isVisible => {
    if (isVisible) {
      // element is visible
      await base.closePopUpCookies();
    } else {
      // element is not visible
    }
  });
  // await base.closePopup();
});

When("I click on add new address button", async () => {
  await base.waitForElementAndClick(addressPage.addNewAddressButton);
});

When("I enter fullname", async () => {
  await base.waitForElementAndSendkeys(
    addressPage.fullName,
    "Bi" + " " + rd.random
  );
});

When("I enter address", async () => {
  await base.waitForElementAndSendkeys(addressPage.address, "15 Ta My Duat");
});

When("I enter country", async () => {});

When("I enter postcode", async () => {
  await base.waitForElementAndSendkeys(addressPage.postalCode, "088278");
});

When("I enter postcode {string}", async string => {
  if (string !== "Hong Kong" && string !== "Macau") {
    await base.waitForElementAndSendkeys(addressPage.postalCode, "088278");
  }
});

When("I enter phonenumber", async () => {
  await base.waitForElementAndSendkeys(addressPage.phoneNumber, "0905495922");
});

When("I enter city {string}", async string => {
  if (string !== "Singapore" && string !== "Hong Kong" && string !== "Macau") {
    await base.waitForElementAndSendkeys(addressPage.cityName, "Reebonz");
  }
});

When("I click on submit button", async () => {
  await base.waitForElementAndClick(addressPage.submitButton);
});

Then("I add new address successful", async () => {
  await base.waitForElementAndGetText(
    addressPage.assertName,
    "Bi" + " " + rd.random
  );
});

Then("I add new address successful - Assert {string}", async string => {
  await base.waitForElementAndGetText(
    addressPage.assertName,
    "Bi" + " " + string + rd.random
  );
});
//<-----------------------------//----------------------------->//
//add credits step
Given("Direct to Titan page", async () => {
  await browser.get(titan.titanCreditsUrl);
});

Then("I click on Login button at Titan page", async () => {
  await base.waitForElementAndClick(titan.loginButton);
});

Then("I enter email for Titan", async () => {
  await browser.sleep(2000);
  let isVisible = await titan.userName.isPresent();
  if (isVisible) {
    // element is visible
    await base.waitForElementAndSendkeys(
      titan.userName,
      "bi.nguyen@reebonz.com"
    );
    await base.waitForElementAndClick(titan.nextButtonEmail);
  } else {
    // element is not visible
  }
});
// await base.waitForElementAndSendkeys(titan.userName,"bi.nguyen@reebonz.com")

Then("I enter password for Titan", async () => {
  await browser.sleep(3000);
  let isVisible = await titan.password.isPresent();
  if (isVisible) {
    // element is visible
    await base.waitForElementAndSendkeys(titan.password, "Enouvo123");
    await base.waitForElementAndClick(titan.nextButtonPassword);
  } else {
    // element is not visible
  }
});
// await base.waitForElementAndSendkeys(titan.password,"Enouvo123")

Then("I enter email address to add credits", async () => {
  await base.waitForElementAndSendkeys(titan.emailToAddcredits, rd.randomEmail);
});

Then("I enter email address to add credits {string}", async string => {
  await base.waitForElementAndSendkeys(
    titan.emailToAddcredits,
    "bi.nguyen+" + string + rd.random + "@reebonz.com"
  );
});

Then("I click on search button on Titan page", async () => {
  await base.waitForElementAndClick(titan.searchButton);
});

Then("I click on add button on Titan page", async () => {
  await base.waitForElementAndClick(titan.addButton);
});

Then("the add credit popup is shown", async () => {
  await base.switchIframe(titan.addCreditsIframe);
});

Then("I select Category on Titan page", async () => {
  await base.waitForElementAndSendkeys(
    titan.selectCategory,
    "RB Loyalty Credits"
  );
});

Then("I add credits on Titan page", async () => {
  await base.waitForElementAndSendkeys(titan.credits, "500");
});

Then("I add full credits on Titan page", async () => {
  await base.waitForElementAndSendkeys(titan.credits, "500000");
});

Then("I click on submit credits button", async () => {
  await base.waitForElementAndClick(titan.submitButton);
});

Then("I add credits successful", async () => {
  await base.switchParrentWindow();
  await base.waitForElementToCheckContainText(titan.assertCredits, "500");
  await browser.executeScript("window.sessionStorage.clear()");
  await browser.executeScript("window.localStorage.clear()");
  //  await base.waitForElementAndClick(titan.logoutButton);
  await browser.driver.manage().deleteAllCookies();
  //  await browser.sleep(5000)
  await browser.get(addressPage.creditProfileUrl);
  await base.waitForElementAndGetText(womanPage.userName, "Bi");
});
//<-------------------------------------//--------------------------------------->>
