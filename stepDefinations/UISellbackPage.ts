import { Given, When, Then } from "cucumber";
import { browser, protractor, $, by, element } from "protractor";
import { register } from "../pageObjects/register";
import chai from "chai";
import { random } from "../stepDefinations/random";
import { basePage } from "../pageObjects/basePage";
import { womanBagsPage } from "../pageObjects/womanBagsPage";
import { addressProfilePage } from "../pageObjects/addressProfilePage";
import { titanPage } from "../pageObjects/titanPage";
import { async } from "q";
var expect = chai.expect;
let registerPage = new register();
let rd = new random();
let base = new basePage();
let womanPage = new womanBagsPage();
let addressPage = new addressProfilePage();
let titan = new titanPage();

//<------------------------------------------//--------------------------------------------------------->>
