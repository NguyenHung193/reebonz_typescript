"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const register_1 = require("../pageObjects/register");
const chai_1 = __importDefault(require("chai"));
const random_1 = require("../stepDefinations/random");
const basePage_1 = require("../pageObjects/basePage");
const womanBagsPage_1 = require("../pageObjects/womanBagsPage");
const addressProfilePage_1 = require("../pageObjects/addressProfilePage");
const titanPage_1 = require("../pageObjects/titanPage");
const myAccountPage_1 = require("../pageObjects/myAccountPage");
var expect = chai_1.default.expect;
let registerPage = new register_1.register();
let rd = new random_1.random();
let base = new basePage_1.basePage();
let womanPage = new womanBagsPage_1.womanBagsPage();
let addressPage = new addressProfilePage_1.addressProfilePage();
let titan = new titanPage_1.titanPage();
let MyAccount = new myAccountPage_1.myAccountPage();
cucumber_1.When("I hover userName and click on userName Popup", () => __awaiter(this, void 0, void 0, function* () {
    let unVisible = yield MyAccount.label.isPresent();
    if (!unVisible) {
        let isVisible = yield womanPage.userName.isPresent();
        if (isVisible) {
            // element is visible
            yield protractor_1.browser
                .actions()
                .mouseMove(womanPage.userName)
                .perform();
            yield base.checkElementVisible(MyAccount.userName);
            yield base.waitForElementAndClick(MyAccount.userName);
        }
        else {
            yield console.log("element is not visible");
            // element is not visible
        }
    }
    else {
        //do the next step
    }
}));
cucumber_1.Then("I direct to my account page", () => __awaiter(this, void 0, void 0, function* () {
    yield base.checkElementVisible(MyAccount.label);
}));
cucumber_1.Then("I am checking element on Myaccount page", () => __awaiter(this, void 0, void 0, function* () {
    yield base.checkElementVisible(MyAccount.firstName);
    yield base.checkElementVisible(MyAccount.lastName);
    yield base.checkElementVisible(MyAccount.gender);
    yield base.checkElementVisible(MyAccount.language);
    yield base.checkElementVisible(MyAccount.newPassword);
    yield base.checkElementVisible(MyAccount.oldPassword);
    yield base.checkElementVisible(MyAccount.phoneNumber);
    yield base.checkElementVisible(MyAccount.email);
    yield base.checkElementVisible(MyAccount.country);
}));
cucumber_1.Then("I am checking fistName on Myaccount page {string}", (string) => __awaiter(this, void 0, void 0, function* () {
    yield base.checkElementVisible(MyAccount.firstName);
    yield base.getElementValue(MyAccount.firstName, string);
}));
cucumber_1.Then("I am checking lastName on Myaccount page {string}", (string) => __awaiter(this, void 0, void 0, function* () {
    yield base.checkElementVisible(MyAccount.lastName);
    yield base.getElementValue(MyAccount.lastName, string);
}));
cucumber_1.Then("I am checking country on Myaccount page  {string}", (string) => __awaiter(this, void 0, void 0, function* () {
    yield base.checkElementVisible(MyAccount.country);
    yield base.getElementValue(MyAccount.country, string);
}));
cucumber_1.Then("I am checking language on Myaccount page {string}", (string) => __awaiter(this, void 0, void 0, function* () {
    yield base.checkElementVisible(MyAccount.language);
    yield base.getElementValue(MyAccount.language, string);
}));
cucumber_1.Then("I am checking email on Myaccount page {string}", (string) => __awaiter(this, void 0, void 0, function* () {
    yield base.checkElementVisible(MyAccount.email);
    yield base.getElementValue(MyAccount.email, string);
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVUlNeUFjY291bnRQYWdlLnN0ZXAuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zdGVwRGVmaW5hdGlvbnMvVUlNeUFjY291bnRQYWdlLnN0ZXAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQUFBLHVDQUE2QztBQUM3QywyQ0FBaUU7QUFDakUsc0RBQW1EO0FBQ25ELGdEQUF3QjtBQUN4QixzREFBbUQ7QUFDbkQsc0RBQW1EO0FBQ25ELGdFQUE2RDtBQUM3RCwwRUFBdUU7QUFDdkUsd0RBQXFEO0FBRXJELGdFQUE2RDtBQUM3RCxJQUFJLE1BQU0sR0FBRyxjQUFJLENBQUMsTUFBTSxDQUFDO0FBQ3pCLElBQUksWUFBWSxHQUFHLElBQUksbUJBQVEsRUFBRSxDQUFDO0FBQ2xDLElBQUksRUFBRSxHQUFHLElBQUksZUFBTSxFQUFFLENBQUM7QUFDdEIsSUFBSSxJQUFJLEdBQUcsSUFBSSxtQkFBUSxFQUFFLENBQUM7QUFDMUIsSUFBSSxTQUFTLEdBQUcsSUFBSSw2QkFBYSxFQUFFLENBQUM7QUFDcEMsSUFBSSxXQUFXLEdBQUcsSUFBSSx1Q0FBa0IsRUFBRSxDQUFDO0FBQzNDLElBQUksS0FBSyxHQUFHLElBQUkscUJBQVMsRUFBRSxDQUFDO0FBQzVCLElBQUksU0FBUyxHQUFHLElBQUksNkJBQWEsRUFBRSxDQUFDO0FBRXBDLGVBQUksQ0FBQyw4Q0FBOEMsRUFBRSxHQUFTLEVBQUU7SUFDOUQsSUFBSSxTQUFTLEdBQUcsTUFBTSxTQUFTLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ2xELElBQUksQ0FBQyxTQUFTLEVBQUU7UUFDZCxJQUFJLFNBQVMsR0FBRyxNQUFNLFNBQVMsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDckQsSUFBSSxTQUFTLEVBQUU7WUFDYixxQkFBcUI7WUFDckIsTUFBTSxvQkFBTztpQkFDVixPQUFPLEVBQUU7aUJBQ1QsU0FBUyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUM7aUJBQzdCLE9BQU8sRUFBRSxDQUFDO1lBQ2IsTUFBTSxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ25ELE1BQU0sSUFBSSxDQUFDLHNCQUFzQixDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUN2RDthQUFNO1lBQ0wsTUFBTSxPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixDQUFDLENBQUM7WUFDNUMseUJBQXlCO1NBQzFCO0tBQ0Y7U0FBTTtRQUNMLGtCQUFrQjtLQUNuQjtBQUNILENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsNkJBQTZCLEVBQUUsR0FBUyxFQUFFO0lBQzdDLE1BQU0sSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztBQUNsRCxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLHlDQUF5QyxFQUFFLEdBQVMsRUFBRTtJQUN6RCxNQUFNLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDcEQsTUFBTSxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ25ELE1BQU0sSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNqRCxNQUFNLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDbkQsTUFBTSxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQ3RELE1BQU0sSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUN0RCxNQUFNLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDdEQsTUFBTSxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2hELE1BQU0sSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztBQUNwRCxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLG1EQUFtRCxFQUFFLENBQU0sTUFBTSxFQUFDLEVBQUU7SUFDdkUsTUFBTSxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ3BELE1BQU0sSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0FBQzFELENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsbURBQW1ELEVBQUUsQ0FBTSxNQUFNLEVBQUMsRUFBRTtJQUN2RSxNQUFNLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDbkQsTUFBTSxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7QUFDekQsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyxtREFBbUQsRUFBRSxDQUFNLE1BQU0sRUFBQyxFQUFFO0lBQ3ZFLE1BQU0sSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNsRCxNQUFNLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztBQUN4RCxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLG1EQUFtRCxFQUFFLENBQU0sTUFBTSxFQUFDLEVBQUU7SUFDdkUsTUFBTSxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ25ELE1BQU0sSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0FBQ3pELENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsZ0RBQWdELEVBQUUsQ0FBTSxNQUFNLEVBQUMsRUFBRTtJQUNwRSxNQUFNLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDaEQsTUFBTSxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUM7QUFDdEQsQ0FBQyxDQUFBLENBQUMsQ0FBQyJ9