// import { Given, When, Then } from "cucumber";
// import { browser, protractor, $, by } from "protractor";
// import { register } from "../pageObjects/register";
// import chai from "chai";
// import { random } from "../stepDefinations/random";
// import { basePage } from "../pageObjects/basePage";
// import { womanBagsPage } from "../pageObjects/womanBagsPage";
// import { addressProfilePage } from "../pageObjects/addressProfilePage";
// import { titanPage } from "../pageObjects/titanPage";
// var expect = chai.expect;
// let registerPage = new register();
// let rd = new random();
// let base = new basePage();
// let womanPage = new womanBagsPage();
// let addressPage = new addressProfilePage;
// let titan = new titanPage;
//   Given('Direct to Titan page', async ()=> {
//     await browser.get(titan.titanCreditsUrl);
//   });
//   Then('I click on Login button at Titan page', async ()=> {
//     await base.waitForElementAndClick(titan.loginButton);
//   });
//   Then('I enter email for Titan', async ()=> {
//     await base.waitForElementAndSendkeys(titan.userName,"bi.nguyen@reebonz.com")
//   });
//   Then('I enter password for Titan', async ()=> {
//     await base.waitForElementAndSendkeys(titan.password,"Enouvo123")
//   });
//   Then('I click on next button', async ()=> {
//     await browser.actions().sendKeys(protractor.Key.ENTER).perform();
//   });
//   Then('I enter email address to add credits', async ()=> {
//     await base.waitForElementAndSendkeys(titan.emailToAddcredits, rd.randomEmail)
//   });
//   Then('I click on search button on Titan page', async ()=> {
//     await base.waitForElementAndClick(titan.searchButton)
//   });
//   Then('I click on add button on Titan page', async ()=> {
//     await base.waitForElementAndClick(titan.addButton)
//   });
//   Then('the add credit popup is shown', async ()=> {
//     await browser.sleep(5000)
//   });
//   Then('I select Category on Titan page', async ()=> {
//     await base.waitForElementAndSendkeys(titan.selectCategory,"Credit Refund")
//   });
//   Then('I add credits on Titan page', async ()=> {
//     await base.waitForElementAndSendkeys(titan.credits,"500")
//   });
//   Then('I click on submit credits button', async ()=> {
//     await base.waitForElementAndClick(titan.submitButton)
//   });
//   Then('I add credits successful', async ()=> {
//     await base.waitForElementToCheckContainText(titan.assertCredits,"500")
//   });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRkQ3JlZGl0cy5zdGVwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3RlcERlZmluYXRpb25zL2FkZENyZWRpdHMuc3RlcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnREFBZ0Q7QUFDaEQsMkRBQTJEO0FBQzNELHNEQUFzRDtBQUN0RCwyQkFBMkI7QUFDM0Isc0RBQXNEO0FBQ3RELHNEQUFzRDtBQUN0RCxnRUFBZ0U7QUFDaEUsMEVBQTBFO0FBQzFFLHdEQUF3RDtBQUN4RCw0QkFBNEI7QUFDNUIscUNBQXFDO0FBQ3JDLHlCQUF5QjtBQUN6Qiw2QkFBNkI7QUFDN0IsdUNBQXVDO0FBQ3ZDLDRDQUE0QztBQUM1Qyw2QkFBNkI7QUFFN0IsK0NBQStDO0FBQy9DLGdEQUFnRDtBQUNoRCxRQUFRO0FBRVIsK0RBQStEO0FBQy9ELDREQUE0RDtBQUM1RCxRQUFRO0FBRVIsaURBQWlEO0FBQ2pELG1GQUFtRjtBQUNuRixRQUFRO0FBRVIsb0RBQW9EO0FBQ3BELHVFQUF1RTtBQUN2RSxRQUFRO0FBRVIsZ0RBQWdEO0FBQ2hELHdFQUF3RTtBQUN4RSxRQUFRO0FBRVIsOERBQThEO0FBQzlELG9GQUFvRjtBQUNwRixRQUFRO0FBRVIsZ0VBQWdFO0FBQ2hFLDREQUE0RDtBQUM1RCxRQUFRO0FBRVIsNkRBQTZEO0FBQzdELHlEQUF5RDtBQUN6RCxRQUFRO0FBRVIsdURBQXVEO0FBQ3ZELGdDQUFnQztBQUNoQyxRQUFRO0FBRVIseURBQXlEO0FBQ3pELGlGQUFpRjtBQUNqRixRQUFRO0FBRVIscURBQXFEO0FBQ3JELGdFQUFnRTtBQUNoRSxRQUFRO0FBRVIsMERBQTBEO0FBQzFELDREQUE0RDtBQUM1RCxRQUFRO0FBRVIsa0RBQWtEO0FBQ2xELDZFQUE2RTtBQUM3RSxRQUFRIn0=