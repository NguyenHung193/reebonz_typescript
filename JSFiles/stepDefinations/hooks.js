"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const basePage_1 = require("../pageObjects/basePage");
const womanBagsPage_1 = require("../pageObjects/womanBagsPage");
let base = new basePage_1.basePage();
let womanPage = new womanBagsPage_1.womanBagsPage();
cucumber_1.Before({ tags: "@calculatortesting" }, function () {
    // This hook will be executed before scenarios tagged with @foo
    protractor_1.browser
        .manage()
        .window()
        .maximize();
});
cucumber_1.After({ tags: "@calculatortesting" }, function () {
    // This hook will be executed before scenarios tagged with @foo
    console.log("Test is completed");
});
cucumber_1.Before({ tags: "@registerAndAddress" }, () => __awaiter(this, void 0, void 0, function* () {
    yield protractor_1.browser.waitForAngularEnabled(false);
    yield protractor_1.browser.manage().deleteAllCookies();
    // await browser.executeScript("window.sessionStorage.clear()");
    // await browser.executeScript("window.localStorage.clear()");
    yield protractor_1.browser.refresh();
}));
cucumber_1.Before({ tags: "@checkoutUsingCreditCard" }, () => __awaiter(this, void 0, void 0, function* () {
    yield protractor_1.browser.waitForAngularEnabled(false);
    yield protractor_1.browser.manage().deleteAllCookies();
    yield protractor_1.browser.refresh();
}));
cucumber_1.Before({ tags: "@checkoutUsingFullCredits" }, () => __awaiter(this, void 0, void 0, function* () {
    yield protractor_1.browser.waitForAngularEnabled(false);
    yield protractor_1.browser.manage().deleteAllCookies();
    // await browser.executeScript('window.sessionStorage.clear();');
    // await browser.executeScript('window.localStorage.clear();');
    yield protractor_1.browser.refresh();
}));
cucumber_1.Before({ tags: "@LoginFunction" }, () => __awaiter(this, void 0, void 0, function* () {
    yield protractor_1.browser.waitForAngularEnabled(false);
    yield protractor_1.browser.manage().deleteAllCookies();
    // await browser.executeScript('window.sessionStorage.clear();');
    // await browser.executeScript('window.localStorage.clear();');
    yield protractor_1.browser.refresh();
}));
cucumber_1.Before({ tags: "@loginWithExistUser" }, () => __awaiter(this, void 0, void 0, function* () {
    yield protractor_1.browser.waitForAngularEnabled(false);
    yield protractor_1.browser.manage().deleteAllCookies();
    // await browser.executeScript('window.sessionStorage.clear();');
    // await browser.executeScript('window.localStorage.clear();');
    yield protractor_1.browser.refresh();
}));
cucumber_1.Before({ tags: "@default" }, () => __awaiter(this, void 0, void 0, function* () {
    yield protractor_1.browser.waitForAngularEnabled(false);
    yield protractor_1.browser.manage().deleteAllCookies();
    // await browser.executeScript('window.sessionStorage.clear();');
    // await browser.executeScript('window.localStorage.clear();');
    yield protractor_1.browser.refresh();
}));
cucumber_1.Before({ tags: "@UIMyAccountPage" }, () => __awaiter(this, void 0, void 0, function* () {
    yield protractor_1.browser.waitForAngularEnabled(false);
    yield protractor_1.browser.manage().deleteAllCookies();
    // await browser.executeScript('window.sessionStorage.clear();');
    // await browser.executeScript('window.localStorage.clear();');
    yield protractor_1.browser.refresh();
}));
cucumber_1.Before({ tags: "@UIAddressPageExistUser" }, () => __awaiter(this, void 0, void 0, function* () {
    yield protractor_1.browser.waitForAngularEnabled(false);
    yield protractor_1.browser.manage().deleteAllCookies();
    // await browser.executeScript('window.sessionStorage.clear();');
    // await browser.executeScript('window.localStorage.clear();');
    yield protractor_1.browser.refresh();
}));
cucumber_1.Before({ tags: "@UIAddressPageNewUser" }, () => __awaiter(this, void 0, void 0, function* () {
    yield protractor_1.browser.waitForAngularEnabled(false);
    yield protractor_1.browser.manage().deleteAllCookies();
    // await browser.executeScript('window.sessionStorage.clear();');
    // await browser.executeScript('window.localStorage.clear();');
    yield protractor_1.browser.refresh();
}));
cucumber_1.After(function (scenario) {
    return __awaiter(this, void 0, void 0, function* () {
        // This hook will be executed before scenarios tagged with @foo
        console.log("Test is completed");
        if (scenario.result.status === cucumber_1.Status.FAILED) {
            //code to take screesnhot
            const screenshot = yield protractor_1.browser.takeScreenshot();
            this.attach(screenshot, "image/png");
        }
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9va3MuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zdGVwRGVmaW5hdGlvbnMvaG9va3MudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHVDQUFpRDtBQUNqRCwyQ0FBcUM7QUFFckMsc0RBQW1EO0FBQ25ELGdFQUE2RDtBQUU3RCxJQUFJLElBQUksR0FBRyxJQUFJLG1CQUFRLEVBQUUsQ0FBQztBQUMxQixJQUFJLFNBQVMsR0FBRyxJQUFJLDZCQUFhLEVBQUUsQ0FBQztBQUNwQyxpQkFBTSxDQUFDLEVBQUUsSUFBSSxFQUFFLG9CQUFvQixFQUFFLEVBQUU7SUFDckMsK0RBQStEO0lBQy9ELG9CQUFPO1NBQ0osTUFBTSxFQUFFO1NBQ1IsTUFBTSxFQUFFO1NBQ1IsUUFBUSxFQUFFLENBQUM7QUFDaEIsQ0FBQyxDQUFDLENBQUM7QUFFSCxnQkFBSyxDQUFDLEVBQUUsSUFBSSxFQUFFLG9CQUFvQixFQUFFLEVBQUU7SUFDcEMsK0RBQStEO0lBQy9ELE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQztBQUNuQyxDQUFDLENBQUMsQ0FBQztBQUVILGlCQUFNLENBQUMsRUFBRSxJQUFJLEVBQUUscUJBQXFCLEVBQUUsRUFBRSxHQUFTLEVBQUU7SUFDakQsTUFBTSxvQkFBTyxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzNDLE1BQU0sb0JBQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQzFDLGdFQUFnRTtJQUNoRSw4REFBOEQ7SUFDOUQsTUFBTSxvQkFBTyxDQUFDLE9BQU8sRUFBRSxDQUFDO0FBQzFCLENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxpQkFBTSxDQUFDLEVBQUUsSUFBSSxFQUFFLDBCQUEwQixFQUFFLEVBQUUsR0FBUyxFQUFFO0lBQ3RELE1BQU0sb0JBQU8sQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMzQyxNQUFNLG9CQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUMxQyxNQUFNLG9CQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7QUFDMUIsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGlCQUFNLENBQUMsRUFBRSxJQUFJLEVBQUUsMkJBQTJCLEVBQUUsRUFBRSxHQUFTLEVBQUU7SUFDdkQsTUFBTSxvQkFBTyxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzNDLE1BQU0sb0JBQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQzFDLGlFQUFpRTtJQUNqRSwrREFBK0Q7SUFDL0QsTUFBTSxvQkFBTyxDQUFDLE9BQU8sRUFBRSxDQUFDO0FBQzFCLENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxpQkFBTSxDQUFDLEVBQUUsSUFBSSxFQUFFLGdCQUFnQixFQUFFLEVBQUUsR0FBUyxFQUFFO0lBQzVDLE1BQU0sb0JBQU8sQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMzQyxNQUFNLG9CQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUMxQyxpRUFBaUU7SUFDakUsK0RBQStEO0lBQy9ELE1BQU0sb0JBQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQztBQUMxQixDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsaUJBQU0sQ0FBQyxFQUFFLElBQUksRUFBRSxxQkFBcUIsRUFBRSxFQUFFLEdBQVMsRUFBRTtJQUNqRCxNQUFNLG9CQUFPLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDM0MsTUFBTSxvQkFBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLGdCQUFnQixFQUFFLENBQUM7SUFDMUMsaUVBQWlFO0lBQ2pFLCtEQUErRDtJQUMvRCxNQUFNLG9CQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7QUFDMUIsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGlCQUFNLENBQUMsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLEVBQUUsR0FBUyxFQUFFO0lBQ3RDLE1BQU0sb0JBQU8sQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMzQyxNQUFNLG9CQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUMxQyxpRUFBaUU7SUFDakUsK0RBQStEO0lBQy9ELE1BQU0sb0JBQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQztBQUMxQixDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsaUJBQU0sQ0FBQyxFQUFFLElBQUksRUFBRSxrQkFBa0IsRUFBRSxFQUFFLEdBQVMsRUFBRTtJQUM5QyxNQUFNLG9CQUFPLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDM0MsTUFBTSxvQkFBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLGdCQUFnQixFQUFFLENBQUM7SUFDMUMsaUVBQWlFO0lBQ2pFLCtEQUErRDtJQUMvRCxNQUFNLG9CQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7QUFDMUIsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGlCQUFNLENBQUMsRUFBRSxJQUFJLEVBQUUseUJBQXlCLEVBQUUsRUFBRSxHQUFTLEVBQUU7SUFDckQsTUFBTSxvQkFBTyxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzNDLE1BQU0sb0JBQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQzFDLGlFQUFpRTtJQUNqRSwrREFBK0Q7SUFDL0QsTUFBTSxvQkFBTyxDQUFDLE9BQU8sRUFBRSxDQUFDO0FBQzFCLENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxpQkFBTSxDQUFDLEVBQUUsSUFBSSxFQUFFLHVCQUF1QixFQUFFLEVBQUUsR0FBUyxFQUFFO0lBQ25ELE1BQU0sb0JBQU8sQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMzQyxNQUFNLG9CQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUMxQyxpRUFBaUU7SUFDakUsK0RBQStEO0lBQy9ELE1BQU0sb0JBQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQztBQUMxQixDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZ0JBQUssQ0FBQyxVQUFlLFFBQVE7O1FBQzNCLCtEQUErRDtRQUMvRCxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDakMsSUFBSSxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sS0FBSyxpQkFBTSxDQUFDLE1BQU0sRUFBRTtZQUM1Qyx5QkFBeUI7WUFDekIsTUFBTSxVQUFVLEdBQUcsTUFBTSxvQkFBTyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBRWxELElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1NBQ3RDO0lBQ0gsQ0FBQztDQUFBLENBQUMsQ0FBQyJ9