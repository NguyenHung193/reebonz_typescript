"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const register_1 = require("../pageObjects/register");
const chai_1 = __importDefault(require("chai"));
const random_1 = require("../stepDefinations/random");
const basePage_1 = require("../pageObjects/basePage");
const womanBagsPage_1 = require("../pageObjects/womanBagsPage");
const addressProfilePage_1 = require("../pageObjects/addressProfilePage");
const titanPage_1 = require("../pageObjects/titanPage");
var expect = chai_1.default.expect;
let registerPage = new register_1.register();
let rd = new random_1.random();
let base = new basePage_1.basePage();
let womanPage = new womanBagsPage_1.womanBagsPage();
let addressPage = new addressProfilePage_1.addressProfilePage();
let titan = new titanPage_1.titanPage();
//<-------------------------------------//--------------------------------------->>
//login steps
cucumber_1.When("I click on Login button", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndClick(registerPage.loginButton);
}));
cucumber_1.Then("The Login popup is displayed", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndGetText(registerPage.loginTitle, "Sign In");
}));
cucumber_1.When("I enter email user {string}", (string) => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(registerPage.loginEmail, "bi.nguyen+" + string + rd.random + "@reebonz.com");
}));
cucumber_1.When("I enter user {string} for {string}", (string, string2) => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(registerPage.loginEmail, string);
}));
cucumber_1.Then("I enter password - login page", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(registerPage.loginPassword, "123456");
}));
cucumber_1.When("I click on login submit", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndClick(registerPage.loginSubmit);
}));
cucumber_1.Then("I login successfully", () => __awaiter(this, void 0, void 0, function* () {
    yield protractor_1.browser.sleep(1000);
    yield base.waitForElementAndGetText(womanPage.userName, "Bi");
}));
cucumber_1.Then("It shows nameError", () => __awaiter(this, void 0, void 0, function* () {
    yield base.checkElementVisible(registerPage.userNameError);
}));
cucumber_1.Then("It shows passwordError", () => __awaiter(this, void 0, void 0, function* () {
    yield base.checkElementVisible(registerPage.passwordError);
}));
//<-------------------------------------//--------------------------------------->>
//register steps
cucumber_1.Given("I will navigate to Reebonz site", () => __awaiter(this, void 0, void 0, function* () {
    yield protractor_1.browser.waitForAngularEnabled(false);
    yield protractor_1.browser.get(registerPage.UATURL);
    let isVisible = yield womanPage.userName.isPresent();
    if (isVisible) {
        // element is visible
        yield protractor_1.browser
            .actions()
            .mouseMove(womanPage.userName)
            .perform();
        yield base.checkElementVisible(womanPage.logoutButton);
        yield base.waitForElementAndClick(womanPage.logoutButton);
    }
    else {
        // element is not visible
    }
}));
cucumber_1.When("I click on register button", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndClick(registerPage.registerButton);
}));
cucumber_1.Then("The register popup is displayed", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndGetText(registerPage.registerTitle, "Register");
}));
cucumber_1.When("I enter FirstName", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(registerPage.firstName, "Bi");
}));
cucumber_1.When("I enter LastName", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(registerPage.lastName, rd.random);
}));
//enter mutiple lastName - outline
cucumber_1.When("I enter lastName {string}", (string) => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(registerPage.lastName, string + rd.random);
}));
cucumber_1.When("I enter email address", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(registerPage.emailAddress, rd.randomEmail);
    yield console.log(rd.randomEmail);
}));
//enter mutilple email - outline
cucumber_1.When("I enter email {string}", (string) => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(registerPage.emailAddress, "bi.nguyen+" + string + rd.random + "@reebonz.com");
}));
cucumber_1.When("I enter password", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(registerPage.password, "123456");
}));
cucumber_1.When("I select country", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(registerPage.country, "Singapore");
}));
//mutilpe countries - outline
cucumber_1.When("I select {string}", (countries) => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(registerPage.country, countries);
}));
cucumber_1.When("I click on Register button", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndClick(registerPage.submit);
}));
cucumber_1.Then("I register successful", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndGetText(womanPage.userName, "Bi");
}));
cucumber_1.Then("I am checking WhatsApp based on {string}", (string) => __awaiter(this, void 0, void 0, function* () {
    if (string === "Singapore" ||
        string === "Australia" ||
        string === "Indonesia" ||
        string === "HongKong") {
        yield base.checkElementVisible(womanPage.whatsApp);
        yield base.checkElementHavingHref(womanPage.whatsApp);
    }
    else {
        yield base.checkElementInVisible(womanPage.whatsApp);
    }
}));
//<-------------------------------------//--------------------------------------->>
//add shipment address steps
cucumber_1.Given("I logged in successful", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndGetText(womanPage.userName, "Bi");
}));
cucumber_1.When("I am navigate to addAddress page", () => __awaiter(this, void 0, void 0, function* () {
    yield protractor_1.browser.get("https://uat-www.reebonz-dev.com/sg/account/addresses");
    yield base.popUpCookies.isPresent().then((isVisible) => __awaiter(this, void 0, void 0, function* () {
        if (isVisible) {
            // element is visible
            yield base.closePopUpCookies();
        }
        else {
            // element is not visible
        }
    }));
    // await base.closePopup();
}));
cucumber_1.When("I click on add new address button", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndClick(addressPage.addNewAddressButton);
}));
cucumber_1.When("I enter fullname", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(addressPage.fullName, "Bi" + " " + rd.random);
}));
cucumber_1.When("I enter address", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(addressPage.address, "15 Ta My Duat");
}));
cucumber_1.When("I enter country", () => __awaiter(this, void 0, void 0, function* () { }));
cucumber_1.When("I enter postcode", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(addressPage.postalCode, "088278");
}));
cucumber_1.When("I enter postcode {string}", (string) => __awaiter(this, void 0, void 0, function* () {
    if (string !== "Hong Kong" && string !== "Macau") {
        yield base.waitForElementAndSendkeys(addressPage.postalCode, "088278");
    }
}));
cucumber_1.When("I enter phonenumber", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(addressPage.phoneNumber, "0905495922");
}));
cucumber_1.When("I enter city {string}", (string) => __awaiter(this, void 0, void 0, function* () {
    if (string !== "Singapore" && string !== "Hong Kong" && string !== "Macau") {
        yield base.waitForElementAndSendkeys(addressPage.cityName, "Reebonz");
    }
}));
cucumber_1.When("I click on submit button", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndClick(addressPage.submitButton);
}));
cucumber_1.Then("I add new address successful", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndGetText(addressPage.assertName, "Bi" + " " + rd.random);
}));
cucumber_1.Then("I add new address successful - Assert {string}", (string) => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndGetText(addressPage.assertName, "Bi" + " " + string + rd.random);
}));
//<-----------------------------//----------------------------->//
//add credits step
cucumber_1.Given("Direct to Titan page", () => __awaiter(this, void 0, void 0, function* () {
    yield protractor_1.browser.get(titan.titanCreditsUrl);
}));
cucumber_1.Then("I click on Login button at Titan page", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndClick(titan.loginButton);
}));
cucumber_1.Then("I enter email for Titan", () => __awaiter(this, void 0, void 0, function* () {
    yield protractor_1.browser.sleep(2000);
    let isVisible = yield titan.userName.isPresent();
    if (isVisible) {
        // element is visible
        yield base.waitForElementAndSendkeys(titan.userName, "bi.nguyen@reebonz.com");
        yield base.waitForElementAndClick(titan.nextButtonEmail);
    }
    else {
        // element is not visible
    }
}));
// await base.waitForElementAndSendkeys(titan.userName,"bi.nguyen@reebonz.com")
cucumber_1.Then("I enter password for Titan", () => __awaiter(this, void 0, void 0, function* () {
    yield protractor_1.browser.sleep(3000);
    let isVisible = yield titan.password.isPresent();
    if (isVisible) {
        // element is visible
        yield base.waitForElementAndSendkeys(titan.password, "Enouvo123");
        yield base.waitForElementAndClick(titan.nextButtonPassword);
    }
    else {
        // element is not visible
    }
}));
// await base.waitForElementAndSendkeys(titan.password,"Enouvo123")
cucumber_1.Then("I enter email address to add credits", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(titan.emailToAddcredits, rd.randomEmail);
}));
cucumber_1.Then("I enter email address to add credits {string}", (string) => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(titan.emailToAddcredits, "bi.nguyen+" + string + rd.random + "@reebonz.com");
}));
cucumber_1.Then("I click on search button on Titan page", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndClick(titan.searchButton);
}));
cucumber_1.Then("I click on add button on Titan page", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndClick(titan.addButton);
}));
cucumber_1.Then("the add credit popup is shown", () => __awaiter(this, void 0, void 0, function* () {
    yield base.switchIframe(titan.addCreditsIframe);
}));
cucumber_1.Then("I select Category on Titan page", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(titan.selectCategory, "RB Loyalty Credits");
}));
cucumber_1.Then("I add credits on Titan page", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(titan.credits, "500");
}));
cucumber_1.Then("I add full credits on Titan page", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(titan.credits, "500000");
}));
cucumber_1.Then("I click on submit credits button", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndClick(titan.submitButton);
}));
cucumber_1.Then("I add credits successful", () => __awaiter(this, void 0, void 0, function* () {
    yield base.switchParrentWindow();
    yield base.waitForElementToCheckContainText(titan.assertCredits, "500");
    yield protractor_1.browser.executeScript("window.sessionStorage.clear()");
    yield protractor_1.browser.executeScript("window.localStorage.clear()");
    //  await base.waitForElementAndClick(titan.logoutButton);
    yield protractor_1.browser.driver.manage().deleteAllCookies();
    //  await browser.sleep(5000)
    yield protractor_1.browser.get(addressPage.creditProfileUrl);
    yield base.waitForElementAndGetText(womanPage.userName, "Bi");
}));
//<-------------------------------------//--------------------------------------->>
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVnaXN0ZXIuc3RlcC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3N0ZXBEZWZpbmF0aW9ucy9yZWdpc3Rlci5zdGVwLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFBQSx1Q0FBNkM7QUFDN0MsMkNBQWlFO0FBQ2pFLHNEQUFtRDtBQUNuRCxnREFBd0I7QUFDeEIsc0RBQW1EO0FBQ25ELHNEQUFtRDtBQUNuRCxnRUFBNkQ7QUFDN0QsMEVBQXVFO0FBQ3ZFLHdEQUFxRDtBQUVyRCxJQUFJLE1BQU0sR0FBRyxjQUFJLENBQUMsTUFBTSxDQUFDO0FBQ3pCLElBQUksWUFBWSxHQUFHLElBQUksbUJBQVEsRUFBRSxDQUFDO0FBQ2xDLElBQUksRUFBRSxHQUFHLElBQUksZUFBTSxFQUFFLENBQUM7QUFDdEIsSUFBSSxJQUFJLEdBQUcsSUFBSSxtQkFBUSxFQUFFLENBQUM7QUFDMUIsSUFBSSxTQUFTLEdBQUcsSUFBSSw2QkFBYSxFQUFFLENBQUM7QUFDcEMsSUFBSSxXQUFXLEdBQUcsSUFBSSx1Q0FBa0IsRUFBRSxDQUFDO0FBQzNDLElBQUksS0FBSyxHQUFHLElBQUkscUJBQVMsRUFBRSxDQUFDO0FBRTVCLG1GQUFtRjtBQUNuRixhQUFhO0FBRWIsZUFBSSxDQUFDLHlCQUF5QixFQUFFLEdBQVMsRUFBRTtJQUN6QyxNQUFNLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLENBQUM7QUFDOUQsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyw4QkFBOEIsRUFBRSxHQUFTLEVBQUU7SUFDOUMsTUFBTSxJQUFJLENBQUMsd0JBQXdCLENBQUMsWUFBWSxDQUFDLFVBQVUsRUFBRSxTQUFTLENBQUMsQ0FBQztBQUMxRSxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLDZCQUE2QixFQUFFLENBQU0sTUFBTSxFQUFDLEVBQUU7SUFDakQsTUFBTSxJQUFJLENBQUMseUJBQXlCLENBQ2xDLFlBQVksQ0FBQyxVQUFVLEVBQ3ZCLFlBQVksR0FBRyxNQUFNLEdBQUcsRUFBRSxDQUFDLE1BQU0sR0FBRyxjQUFjLENBQ25ELENBQUM7QUFDSixDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLG9DQUFvQyxFQUFFLENBQU8sTUFBTSxFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQ25FLE1BQU0sSUFBSSxDQUFDLHlCQUF5QixDQUFDLFlBQVksQ0FBQyxVQUFVLEVBQUUsTUFBTSxDQUFDLENBQUM7QUFDeEUsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQywrQkFBK0IsRUFBRSxHQUFTLEVBQUU7SUFDL0MsTUFBTSxJQUFJLENBQUMseUJBQXlCLENBQUMsWUFBWSxDQUFDLGFBQWEsRUFBRSxRQUFRLENBQUMsQ0FBQztBQUM3RSxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLHlCQUF5QixFQUFFLEdBQVMsRUFBRTtJQUN6QyxNQUFNLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLENBQUM7QUFDOUQsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyxzQkFBc0IsRUFBRSxHQUFTLEVBQUU7SUFDdEMsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMxQixNQUFNLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO0FBQ2hFLENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsb0JBQW9CLEVBQUUsR0FBUyxFQUFFO0lBQ3BDLE1BQU0sSUFBSSxDQUFDLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQztBQUM3RCxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLHdCQUF3QixFQUFFLEdBQVMsRUFBRTtJQUN4QyxNQUFNLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLENBQUM7QUFDN0QsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILG1GQUFtRjtBQUNuRixnQkFBZ0I7QUFFaEIsZ0JBQUssQ0FBQyxpQ0FBaUMsRUFBRSxHQUFTLEVBQUU7SUFDbEQsTUFBTSxvQkFBTyxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzNDLE1BQU0sb0JBQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBRXZDLElBQUksU0FBUyxHQUFHLE1BQU0sU0FBUyxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUNyRCxJQUFJLFNBQVMsRUFBRTtRQUNiLHFCQUFxQjtRQUNyQixNQUFNLG9CQUFPO2FBQ1YsT0FBTyxFQUFFO2FBQ1QsU0FBUyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUM7YUFDN0IsT0FBTyxFQUFFLENBQUM7UUFDYixNQUFNLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDdkQsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxDQUFDO0tBQzNEO1NBQU07UUFDTCx5QkFBeUI7S0FDMUI7QUFDSCxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLDRCQUE0QixFQUFFLEdBQVMsRUFBRTtJQUM1QyxNQUFNLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLENBQUM7QUFDakUsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyxpQ0FBaUMsRUFBRSxHQUFTLEVBQUU7SUFDakQsTUFBTSxJQUFJLENBQUMsd0JBQXdCLENBQUMsWUFBWSxDQUFDLGFBQWEsRUFBRSxVQUFVLENBQUMsQ0FBQztBQUM5RSxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLG1CQUFtQixFQUFFLEdBQVMsRUFBRTtJQUNuQyxNQUFNLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO0FBQ3JFLENBQUMsQ0FBQSxDQUFDLENBQUM7QUFDSCxlQUFJLENBQUMsa0JBQWtCLEVBQUUsR0FBUyxFQUFFO0lBQ2xDLE1BQU0sSUFBSSxDQUFDLHlCQUF5QixDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQ3pFLENBQUMsQ0FBQSxDQUFDLENBQUM7QUFDSCxrQ0FBa0M7QUFDbEMsZUFBSSxDQUFDLDJCQUEyQixFQUFFLENBQU0sTUFBTSxFQUFDLEVBQUU7SUFDL0MsTUFBTSxJQUFJLENBQUMseUJBQXlCLENBQ2xDLFlBQVksQ0FBQyxRQUFRLEVBQ3JCLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUNuQixDQUFDO0FBQ0osQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyx1QkFBdUIsRUFBRSxHQUFTLEVBQUU7SUFDdkMsTUFBTSxJQUFJLENBQUMseUJBQXlCLENBQ2xDLFlBQVksQ0FBQyxZQUFZLEVBQ3pCLEVBQUUsQ0FBQyxXQUFXLENBQ2YsQ0FBQztJQUNGLE1BQU0sT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLENBQUM7QUFDcEMsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUNILGdDQUFnQztBQUNoQyxlQUFJLENBQUMsd0JBQXdCLEVBQUUsQ0FBTSxNQUFNLEVBQUMsRUFBRTtJQUM1QyxNQUFNLElBQUksQ0FBQyx5QkFBeUIsQ0FDbEMsWUFBWSxDQUFDLFlBQVksRUFDekIsWUFBWSxHQUFHLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxHQUFHLGNBQWMsQ0FDbkQsQ0FBQztBQUNKLENBQUMsQ0FBQSxDQUFDLENBQUM7QUFDSCxlQUFJLENBQUMsa0JBQWtCLEVBQUUsR0FBUyxFQUFFO0lBQ2xDLE1BQU0sSUFBSSxDQUFDLHlCQUF5QixDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsUUFBUSxDQUFDLENBQUM7QUFDeEUsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUNILGVBQUksQ0FBQyxrQkFBa0IsRUFBRSxHQUFTLEVBQUU7SUFDbEMsTUFBTSxJQUFJLENBQUMseUJBQXlCLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsQ0FBQztBQUMxRSxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBQ0gsNkJBQTZCO0FBQzdCLGVBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFNLFNBQVMsRUFBQyxFQUFFO0lBQzFDLE1BQU0sSUFBSSxDQUFDLHlCQUF5QixDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsU0FBUyxDQUFDLENBQUM7QUFDeEUsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUNILGVBQUksQ0FBQyw0QkFBNEIsRUFBRSxHQUFTLEVBQUU7SUFDNUMsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQ3pELENBQUMsQ0FBQSxDQUFDLENBQUM7QUFDSCxlQUFJLENBQUMsdUJBQXVCLEVBQUUsR0FBUyxFQUFFO0lBQ3ZDLE1BQU0sSUFBSSxDQUFDLHdCQUF3QixDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7QUFDaEUsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQywwQ0FBMEMsRUFBRSxDQUFNLE1BQU0sRUFBQyxFQUFFO0lBQzlELElBQ0UsTUFBTSxLQUFLLFdBQVc7UUFDdEIsTUFBTSxLQUFLLFdBQVc7UUFDdEIsTUFBTSxLQUFLLFdBQVc7UUFDdEIsTUFBTSxLQUFLLFVBQVUsRUFDckI7UUFDQSxNQUFNLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDbkQsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0tBQ3ZEO1NBQU07UUFDTCxNQUFNLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7S0FDdEQ7QUFDSCxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsbUZBQW1GO0FBQ25GLDRCQUE0QjtBQUU1QixnQkFBSyxDQUFDLHdCQUF3QixFQUFFLEdBQVMsRUFBRTtJQUN6QyxNQUFNLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO0FBQ2hFLENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsa0NBQWtDLEVBQUUsR0FBUyxFQUFFO0lBQ2xELE1BQU0sb0JBQU8sQ0FBQyxHQUFHLENBQUMsc0RBQXNELENBQUMsQ0FBQztJQUMxRSxNQUFNLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQU0sU0FBUyxFQUFDLEVBQUU7UUFDekQsSUFBSSxTQUFTLEVBQUU7WUFDYixxQkFBcUI7WUFDckIsTUFBTSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztTQUNoQzthQUFNO1lBQ0wseUJBQXlCO1NBQzFCO0lBQ0gsQ0FBQyxDQUFBLENBQUMsQ0FBQztJQUNILDJCQUEyQjtBQUM3QixDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLG1DQUFtQyxFQUFFLEdBQVMsRUFBRTtJQUNuRCxNQUFNLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsQ0FBQztBQUNyRSxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLGtCQUFrQixFQUFFLEdBQVMsRUFBRTtJQUNsQyxNQUFNLElBQUksQ0FBQyx5QkFBeUIsQ0FDbEMsV0FBVyxDQUFDLFFBQVEsRUFDcEIsSUFBSSxHQUFHLEdBQUcsR0FBRyxFQUFFLENBQUMsTUFBTSxDQUN2QixDQUFDO0FBQ0osQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyxpQkFBaUIsRUFBRSxHQUFTLEVBQUU7SUFDakMsTUFBTSxJQUFJLENBQUMseUJBQXlCLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxlQUFlLENBQUMsQ0FBQztBQUM3RSxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLGlCQUFpQixFQUFFLEdBQVMsRUFBRSxnREFBRSxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRXhDLGVBQUksQ0FBQyxrQkFBa0IsRUFBRSxHQUFTLEVBQUU7SUFDbEMsTUFBTSxJQUFJLENBQUMseUJBQXlCLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxRQUFRLENBQUMsQ0FBQztBQUN6RSxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLDJCQUEyQixFQUFFLENBQU0sTUFBTSxFQUFDLEVBQUU7SUFDL0MsSUFBSSxNQUFNLEtBQUssV0FBVyxJQUFJLE1BQU0sS0FBSyxPQUFPLEVBQUU7UUFDaEQsTUFBTSxJQUFJLENBQUMseUJBQXlCLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxRQUFRLENBQUMsQ0FBQztLQUN4RTtBQUNILENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMscUJBQXFCLEVBQUUsR0FBUyxFQUFFO0lBQ3JDLE1BQU0sSUFBSSxDQUFDLHlCQUF5QixDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsWUFBWSxDQUFDLENBQUM7QUFDOUUsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyx1QkFBdUIsRUFBRSxDQUFNLE1BQU0sRUFBQyxFQUFFO0lBQzNDLElBQUksTUFBTSxLQUFLLFdBQVcsSUFBSSxNQUFNLEtBQUssV0FBVyxJQUFJLE1BQU0sS0FBSyxPQUFPLEVBQUU7UUFDMUUsTUFBTSxJQUFJLENBQUMseUJBQXlCLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRSxTQUFTLENBQUMsQ0FBQztLQUN2RTtBQUNILENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsMEJBQTBCLEVBQUUsR0FBUyxFQUFFO0lBQzFDLE1BQU0sSUFBSSxDQUFDLHNCQUFzQixDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsQ0FBQztBQUM5RCxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLDhCQUE4QixFQUFFLEdBQVMsRUFBRTtJQUM5QyxNQUFNLElBQUksQ0FBQyx3QkFBd0IsQ0FDakMsV0FBVyxDQUFDLFVBQVUsRUFDdEIsSUFBSSxHQUFHLEdBQUcsR0FBRyxFQUFFLENBQUMsTUFBTSxDQUN2QixDQUFDO0FBQ0osQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyxnREFBZ0QsRUFBRSxDQUFNLE1BQU0sRUFBQyxFQUFFO0lBQ3BFLE1BQU0sSUFBSSxDQUFDLHdCQUF3QixDQUNqQyxXQUFXLENBQUMsVUFBVSxFQUN0QixJQUFJLEdBQUcsR0FBRyxHQUFHLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUNoQyxDQUFDO0FBQ0osQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUNILGtFQUFrRTtBQUNsRSxrQkFBa0I7QUFDbEIsZ0JBQUssQ0FBQyxzQkFBc0IsRUFBRSxHQUFTLEVBQUU7SUFDdkMsTUFBTSxvQkFBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLENBQUM7QUFDM0MsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyx1Q0FBdUMsRUFBRSxHQUFTLEVBQUU7SUFDdkQsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0FBQ3ZELENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMseUJBQXlCLEVBQUUsR0FBUyxFQUFFO0lBQ3pDLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDMUIsSUFBSSxTQUFTLEdBQUcsTUFBTSxLQUFLLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ2pELElBQUksU0FBUyxFQUFFO1FBQ2IscUJBQXFCO1FBQ3JCLE1BQU0sSUFBSSxDQUFDLHlCQUF5QixDQUNsQyxLQUFLLENBQUMsUUFBUSxFQUNkLHVCQUF1QixDQUN4QixDQUFDO1FBQ0YsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDO0tBQzFEO1NBQU07UUFDTCx5QkFBeUI7S0FDMUI7QUFDSCxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBQ0gsK0VBQStFO0FBRS9FLGVBQUksQ0FBQyw0QkFBNEIsRUFBRSxHQUFTLEVBQUU7SUFDNUMsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMxQixJQUFJLFNBQVMsR0FBRyxNQUFNLEtBQUssQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDakQsSUFBSSxTQUFTLEVBQUU7UUFDYixxQkFBcUI7UUFDckIsTUFBTSxJQUFJLENBQUMseUJBQXlCLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxXQUFXLENBQUMsQ0FBQztRQUNsRSxNQUFNLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsa0JBQWtCLENBQUMsQ0FBQztLQUM3RDtTQUFNO1FBQ0wseUJBQXlCO0tBQzFCO0FBQ0gsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUNILG1FQUFtRTtBQUVuRSxlQUFJLENBQUMsc0NBQXNDLEVBQUUsR0FBUyxFQUFFO0lBQ3RELE1BQU0sSUFBSSxDQUFDLHlCQUF5QixDQUFDLEtBQUssQ0FBQyxpQkFBaUIsRUFBRSxFQUFFLENBQUMsV0FBVyxDQUFDLENBQUM7QUFDaEYsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQywrQ0FBK0MsRUFBRSxDQUFNLE1BQU0sRUFBQyxFQUFFO0lBQ25FLE1BQU0sSUFBSSxDQUFDLHlCQUF5QixDQUNsQyxLQUFLLENBQUMsaUJBQWlCLEVBQ3ZCLFlBQVksR0FBRyxNQUFNLEdBQUcsRUFBRSxDQUFDLE1BQU0sR0FBRyxjQUFjLENBQ25ELENBQUM7QUFDSixDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLHdDQUF3QyxFQUFFLEdBQVMsRUFBRTtJQUN4RCxNQUFNLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7QUFDeEQsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyxxQ0FBcUMsRUFBRSxHQUFTLEVBQUU7SUFDckQsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0FBQ3JELENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsK0JBQStCLEVBQUUsR0FBUyxFQUFFO0lBQy9DLE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztBQUNsRCxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLGlDQUFpQyxFQUFFLEdBQVMsRUFBRTtJQUNqRCxNQUFNLElBQUksQ0FBQyx5QkFBeUIsQ0FDbEMsS0FBSyxDQUFDLGNBQWMsRUFDcEIsb0JBQW9CLENBQ3JCLENBQUM7QUFDSixDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLDZCQUE2QixFQUFFLEdBQVMsRUFBRTtJQUM3QyxNQUFNLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDO0FBQzdELENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsa0NBQWtDLEVBQUUsR0FBUyxFQUFFO0lBQ2xELE1BQU0sSUFBSSxDQUFDLHlCQUF5QixDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsUUFBUSxDQUFDLENBQUM7QUFDaEUsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyxrQ0FBa0MsRUFBRSxHQUFTLEVBQUU7SUFDbEQsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO0FBQ3hELENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsMEJBQTBCLEVBQUUsR0FBUyxFQUFFO0lBQzFDLE1BQU0sSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7SUFDakMsTUFBTSxJQUFJLENBQUMsZ0NBQWdDLENBQUMsS0FBSyxDQUFDLGFBQWEsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUN4RSxNQUFNLG9CQUFPLENBQUMsYUFBYSxDQUFDLCtCQUErQixDQUFDLENBQUM7SUFDN0QsTUFBTSxvQkFBTyxDQUFDLGFBQWEsQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDO0lBQzNELDBEQUEwRDtJQUMxRCxNQUFNLG9CQUFPLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLGdCQUFnQixFQUFFLENBQUM7SUFDakQsNkJBQTZCO0lBQzdCLE1BQU0sb0JBQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDaEQsTUFBTSxJQUFJLENBQUMsd0JBQXdCLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQztBQUNoRSxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBQ0gsbUZBQW1GIn0=