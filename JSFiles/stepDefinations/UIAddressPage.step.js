"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const register_1 = require("../pageObjects/register");
const chai_1 = __importDefault(require("chai"));
const random_1 = require("./random");
const basePage_1 = require("../pageObjects/basePage");
const womanBagsPage_1 = require("../pageObjects/womanBagsPage");
const addressProfilePage_1 = require("../pageObjects/addressProfilePage");
const titanPage_1 = require("../pageObjects/titanPage");
const myAccountPage_1 = require("../pageObjects/myAccountPage");
var expect = chai_1.default.expect;
let registerPage = new register_1.register();
let rd = new random_1.random();
let base = new basePage_1.basePage();
let womanPage = new womanBagsPage_1.womanBagsPage();
let addressPage = new addressProfilePage_1.addressProfilePage();
let titan = new titanPage_1.titanPage();
let MyAccount = new myAccountPage_1.myAccountPage();
cucumber_1.When("I hover userName and click on address Popup", () => __awaiter(this, void 0, void 0, function* () {
    let unVisible = yield addressPage.addressLabel.isPresent();
    if (!unVisible) {
        let isVisible = yield womanPage.userName.isPresent();
        if (isVisible) {
            // element is visible
            yield protractor_1.browser
                .actions()
                .mouseMove(womanPage.userName)
                .perform();
            yield base.checkElementVisible(womanPage.address);
            yield base.waitForElementAndClick(womanPage.address);
        }
        else {
            yield console.log("element is not visible");
            // element is not visible
        }
    }
    else {
        //do the next step
    }
}));
cucumber_1.Then("I direct to my address page", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElement(addressPage.addressLabel);
}));
cucumber_1.Then("I am checking exist name on Address page {string}", (string) => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndGetText(addressPage.subName, string);
}));
cucumber_1.Then("I am checking exist address on Address page {string}", (string) => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndGetText(addressPage.subAddress, string);
}));
cucumber_1.Then("I am checking exist country on Address page  {string}", (string) => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndGetText(addressPage.subCountry, string);
}));
cucumber_1.Then("I am checking post code on Address page {string} for {string}", (string, string2) => __awaiter(this, void 0, void 0, function* () {
    if (string2 !== "Hong Kong") {
        yield base.waitForElementAndGetText(addressPage.subPostCode, string);
    }
}));
cucumber_1.Then("I am checking exist phone on Address page {string}", (string) => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndGetText(addressPage.subPhone, string);
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVUlBZGRyZXNzUGFnZS5zdGVwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3RlcERlZmluYXRpb25zL1VJQWRkcmVzc1BhZ2Uuc3RlcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBQUEsdUNBQTZDO0FBQzdDLDJDQUFpRTtBQUNqRSxzREFBbUQ7QUFDbkQsZ0RBQXdCO0FBQ3hCLHFDQUFrQztBQUNsQyxzREFBbUQ7QUFDbkQsZ0VBQTZEO0FBQzdELDBFQUF1RTtBQUN2RSx3REFBcUQ7QUFFckQsZ0VBQTZEO0FBQzdELElBQUksTUFBTSxHQUFHLGNBQUksQ0FBQyxNQUFNLENBQUM7QUFDekIsSUFBSSxZQUFZLEdBQUcsSUFBSSxtQkFBUSxFQUFFLENBQUM7QUFDbEMsSUFBSSxFQUFFLEdBQUcsSUFBSSxlQUFNLEVBQUUsQ0FBQztBQUN0QixJQUFJLElBQUksR0FBRyxJQUFJLG1CQUFRLEVBQUUsQ0FBQztBQUMxQixJQUFJLFNBQVMsR0FBRyxJQUFJLDZCQUFhLEVBQUUsQ0FBQztBQUNwQyxJQUFJLFdBQVcsR0FBRyxJQUFJLHVDQUFrQixFQUFFLENBQUM7QUFDM0MsSUFBSSxLQUFLLEdBQUcsSUFBSSxxQkFBUyxFQUFFLENBQUM7QUFDNUIsSUFBSSxTQUFTLEdBQUcsSUFBSSw2QkFBYSxFQUFFLENBQUM7QUFFcEMsZUFBSSxDQUFDLDZDQUE2QyxFQUFFLEdBQVMsRUFBRTtJQUM3RCxJQUFJLFNBQVMsR0FBRyxNQUFNLFdBQVcsQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDM0QsSUFBSSxDQUFDLFNBQVMsRUFBRTtRQUNkLElBQUksU0FBUyxHQUFHLE1BQU0sU0FBUyxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNyRCxJQUFJLFNBQVMsRUFBRTtZQUNiLHFCQUFxQjtZQUNyQixNQUFNLG9CQUFPO2lCQUNWLE9BQU8sRUFBRTtpQkFDVCxTQUFTLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQztpQkFDN0IsT0FBTyxFQUFFLENBQUM7WUFDYixNQUFNLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDbEQsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ3REO2FBQU07WUFDTCxNQUFNLE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLENBQUMsQ0FBQztZQUM1Qyx5QkFBeUI7U0FDMUI7S0FDRjtTQUFNO1FBQ0wsa0JBQWtCO0tBQ25CO0FBQ0gsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyw2QkFBNkIsRUFBRSxHQUFTLEVBQUU7SUFDN0MsTUFBTSxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsQ0FBQztBQUN0RCxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLG1EQUFtRCxFQUFFLENBQU0sTUFBTSxFQUFDLEVBQUU7SUFDdkUsTUFBTSxJQUFJLENBQUMsd0JBQXdCLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztBQUNuRSxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLHNEQUFzRCxFQUFFLENBQU0sTUFBTSxFQUFDLEVBQUU7SUFDMUUsTUFBTSxJQUFJLENBQUMsd0JBQXdCLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxNQUFNLENBQUMsQ0FBQztBQUN0RSxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLHVEQUF1RCxFQUFFLENBQU0sTUFBTSxFQUFDLEVBQUU7SUFDM0UsTUFBTSxJQUFJLENBQUMsd0JBQXdCLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxNQUFNLENBQUMsQ0FBQztBQUN0RSxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUNGLCtEQUErRCxFQUMvRCxDQUFPLE1BQU0sRUFBRSxPQUFPLEVBQUUsRUFBRTtJQUN4QixJQUFJLE9BQU8sS0FBSyxXQUFXLEVBQUU7UUFDM0IsTUFBTSxJQUFJLENBQUMsd0JBQXdCLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUMsQ0FBQztLQUN0RTtBQUNILENBQUMsQ0FBQSxDQUNGLENBQUM7QUFFRixlQUFJLENBQUMsb0RBQW9ELEVBQUUsQ0FBTSxNQUFNLEVBQUMsRUFBRTtJQUN4RSxNQUFNLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0FBQ3BFLENBQUMsQ0FBQSxDQUFDLENBQUMifQ==