"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const register_1 = require("../pageObjects/register");
const chai_1 = __importDefault(require("chai"));
const random_1 = require("./random");
const basePage_1 = require("../pageObjects/basePage");
const womanBagsPage_1 = require("../pageObjects/womanBagsPage");
const addressProfilePage_1 = require("../pageObjects/addressProfilePage");
const titanPage_1 = require("../pageObjects/titanPage");
const myAccountPage_1 = require("../pageObjects/myAccountPage");
const UIUserPopUp_1 = require("../pageObjects/UIUserPopUp");
var expect = chai_1.default.expect;
let registerPage = new register_1.register();
let rd = new random_1.random();
let base = new basePage_1.basePage();
let womanPage = new womanBagsPage_1.womanBagsPage();
let addressPage = new addressProfilePage_1.addressProfilePage();
let titan = new titanPage_1.titanPage();
let MyAccount = new myAccountPage_1.myAccountPage();
let UserPopUp = new UIUserPopUp_1.UIUserPopUp();
cucumber_1.When("I hover userName", () => __awaiter(this, void 0, void 0, function* () {
    base.waitForElement(womanPage.userName);
    let isVisible = yield womanPage.userName.isPresent();
    if (isVisible) {
        // element is visible
        yield protractor_1.browser
            .actions()
            .mouseMove(womanPage.userName)
            .perform();
        yield base.waitForElement(MyAccount.userName);
    }
    else {
        yield console.log("element is not visible");
        // element is not visible
    }
}));
cucumber_1.Then("I am checking order", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElement(UserPopUp.orders);
    yield base.checkElementVisible(UserPopUp.orders);
}));
cucumber_1.Then("I am checking sellback based on {string}", (string) => __awaiter(this, void 0, void 0, function* () {
    if (string === "Singapore") {
        yield base.waitForElement(UserPopUp.sellback);
        yield base.checkElementVisible(UserPopUp.sellback);
    }
}));
cucumber_1.Then("I am checking white glove based on {string}", (string) => __awaiter(this, void 0, void 0, function* () {
    if (string === "Singapore") {
        yield base.waitForElement(UserPopUp.whiteGlove);
        yield base.checkElementVisible(UserPopUp.whiteGlove);
    }
}));
cucumber_1.Then("I am checking credits", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElement(UserPopUp.credits);
    yield base.checkElementVisible(UserPopUp.credits);
}));
cucumber_1.Then("I am checking wishlist", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElement(UserPopUp.wishlist);
    yield base.checkElementVisible(UserPopUp.wishlist);
}));
cucumber_1.Then("I am checking address", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElement(UserPopUp.address);
    yield base.checkElementVisible(UserPopUp.address);
}));
cucumber_1.Then("I am checking inbox", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElement(UserPopUp.inbox);
    yield base.checkElementVisible(UserPopUp.inbox);
}));
cucumber_1.Then("I am checking boutique | follow", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElement(UserPopUp.boutique);
    yield base.checkElementVisible(UserPopUp.boutique);
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVUlVc2VyUG9wVXAuc3RlcC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3N0ZXBEZWZpbmF0aW9ucy9VSVVzZXJQb3BVcC5zdGVwLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFBQSx1Q0FBNkM7QUFDN0MsMkNBQWlFO0FBQ2pFLHNEQUFtRDtBQUNuRCxnREFBd0I7QUFDeEIscUNBQWtDO0FBQ2xDLHNEQUFtRDtBQUNuRCxnRUFBNkQ7QUFDN0QsMEVBQXVFO0FBQ3ZFLHdEQUFxRDtBQUVyRCxnRUFBNkQ7QUFDN0QsNERBQXlEO0FBQ3pELElBQUksTUFBTSxHQUFHLGNBQUksQ0FBQyxNQUFNLENBQUM7QUFDekIsSUFBSSxZQUFZLEdBQUcsSUFBSSxtQkFBUSxFQUFFLENBQUM7QUFDbEMsSUFBSSxFQUFFLEdBQUcsSUFBSSxlQUFNLEVBQUUsQ0FBQztBQUN0QixJQUFJLElBQUksR0FBRyxJQUFJLG1CQUFRLEVBQUUsQ0FBQztBQUMxQixJQUFJLFNBQVMsR0FBRyxJQUFJLDZCQUFhLEVBQUUsQ0FBQztBQUNwQyxJQUFJLFdBQVcsR0FBRyxJQUFJLHVDQUFrQixFQUFFLENBQUM7QUFDM0MsSUFBSSxLQUFLLEdBQUcsSUFBSSxxQkFBUyxFQUFFLENBQUM7QUFDNUIsSUFBSSxTQUFTLEdBQUcsSUFBSSw2QkFBYSxFQUFFLENBQUM7QUFDcEMsSUFBSSxTQUFTLEdBQUcsSUFBSSx5QkFBVyxFQUFFLENBQUM7QUFFbEMsZUFBSSxDQUFDLGtCQUFrQixFQUFFLEdBQVMsRUFBRTtJQUNsQyxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN4QyxJQUFJLFNBQVMsR0FBRyxNQUFNLFNBQVMsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDckQsSUFBSSxTQUFTLEVBQUU7UUFDYixxQkFBcUI7UUFDckIsTUFBTSxvQkFBTzthQUNWLE9BQU8sRUFBRTthQUNULFNBQVMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDO2FBQzdCLE9BQU8sRUFBRSxDQUFDO1FBQ2IsTUFBTSxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztLQUMvQztTQUFNO1FBQ0wsTUFBTSxPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixDQUFDLENBQUM7UUFDNUMseUJBQXlCO0tBQzFCO0FBQ0gsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyxxQkFBcUIsRUFBRSxHQUFTLEVBQUU7SUFDckMsTUFBTSxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUM1QyxNQUFNLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDbkQsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQywwQ0FBMEMsRUFBRSxDQUFNLE1BQU0sRUFBQyxFQUFFO0lBQzlELElBQUksTUFBTSxLQUFLLFdBQVcsRUFBRTtRQUMxQixNQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzlDLE1BQU0sSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztLQUNwRDtBQUNILENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsNkNBQTZDLEVBQUUsQ0FBTSxNQUFNLEVBQUMsRUFBRTtJQUNqRSxJQUFJLE1BQU0sS0FBSyxXQUFXLEVBQUU7UUFDMUIsTUFBTSxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNoRCxNQUFNLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUM7S0FDdEQ7QUFDSCxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLHVCQUF1QixFQUFFLEdBQVMsRUFBRTtJQUN2QyxNQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzdDLE1BQU0sSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztBQUNwRCxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLHdCQUF3QixFQUFFLEdBQVMsRUFBRTtJQUN4QyxNQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzlDLE1BQU0sSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUNyRCxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLHVCQUF1QixFQUFFLEdBQVMsRUFBRTtJQUN2QyxNQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzdDLE1BQU0sSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztBQUNwRCxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLHFCQUFxQixFQUFFLEdBQVMsRUFBRTtJQUNyQyxNQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzNDLE1BQU0sSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztBQUNsRCxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLGlDQUFpQyxFQUFFLEdBQVMsRUFBRTtJQUNqRCxNQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzlDLE1BQU0sSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUNyRCxDQUFDLENBQUEsQ0FBQyxDQUFDIn0=