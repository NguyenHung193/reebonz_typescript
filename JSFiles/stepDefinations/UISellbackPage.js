"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const register_1 = require("../pageObjects/register");
const chai_1 = __importDefault(require("chai"));
const random_1 = require("../stepDefinations/random");
const basePage_1 = require("../pageObjects/basePage");
const womanBagsPage_1 = require("../pageObjects/womanBagsPage");
const addressProfilePage_1 = require("../pageObjects/addressProfilePage");
const titanPage_1 = require("../pageObjects/titanPage");
var expect = chai_1.default.expect;
let registerPage = new register_1.register();
let rd = new random_1.random();
let base = new basePage_1.basePage();
let womanPage = new womanBagsPage_1.womanBagsPage();
let addressPage = new addressProfilePage_1.addressProfilePage();
let titan = new titanPage_1.titanPage();
//<------------------------------------------//--------------------------------------------------------->>
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVUlTZWxsYmFja1BhZ2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zdGVwRGVmaW5hdGlvbnMvVUlTZWxsYmFja1BhZ2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFFQSxzREFBbUQ7QUFDbkQsZ0RBQXdCO0FBQ3hCLHNEQUFtRDtBQUNuRCxzREFBbUQ7QUFDbkQsZ0VBQTZEO0FBQzdELDBFQUF1RTtBQUN2RSx3REFBcUQ7QUFFckQsSUFBSSxNQUFNLEdBQUcsY0FBSSxDQUFDLE1BQU0sQ0FBQztBQUN6QixJQUFJLFlBQVksR0FBRyxJQUFJLG1CQUFRLEVBQUUsQ0FBQztBQUNsQyxJQUFJLEVBQUUsR0FBRyxJQUFJLGVBQU0sRUFBRSxDQUFDO0FBQ3RCLElBQUksSUFBSSxHQUFHLElBQUksbUJBQVEsRUFBRSxDQUFDO0FBQzFCLElBQUksU0FBUyxHQUFHLElBQUksNkJBQWEsRUFBRSxDQUFDO0FBQ3BDLElBQUksV0FBVyxHQUFHLElBQUksdUNBQWtCLEVBQUUsQ0FBQztBQUMzQyxJQUFJLEtBQUssR0FBRyxJQUFJLHFCQUFTLEVBQUUsQ0FBQztBQUU1QiwwR0FBMEcifQ==