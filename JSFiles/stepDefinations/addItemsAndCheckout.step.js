"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const womanBagsPage_1 = require("../pageObjects/womanBagsPage");
const basePage_1 = require("../pageObjects/basePage");
const productDetailsPage_1 = require("../pageObjects/productDetailsPage");
const checkoutPage_1 = require("../pageObjects/checkoutPage");
const thankyouPage_1 = require("../pageObjects/thankyouPage");
const chai_1 = __importDefault(require("chai"));
var expect = chai_1.default.expect;
let womanPage = new womanBagsPage_1.womanBagsPage();
let base = new basePage_1.basePage();
let productPage = new productDetailsPage_1.productDetailsPage();
let checkout = new checkoutPage_1.checkoutPage();
let thankyou = new thankyouPage_1.thankyouPage();
//<-------------------------------------//--------------------------------------->>
//add items steps
cucumber_1.Given("Direct to woman bags page", () => __awaiter(this, void 0, void 0, function* () {
    yield protractor_1.browser.waitForAngularEnabled(false);
    yield protractor_1.browser.get(womanPage.womanBagsUrl);
    yield base.popUpCookies.isPresent().then((isVisible) => __awaiter(this, void 0, void 0, function* () {
        if (isVisible) {
            // element is visible
            yield base.closePopUpCookies();
        }
        else {
            // element is not visible
        }
    }));
}));
cucumber_1.When("Click on product", () => __awaiter(this, void 0, void 0, function* () {
    // await base.waitForElementAndClick(womanPage.product);
}));
cucumber_1.When("Direct to product details page and check basketQty", () => __awaiter(this, void 0, void 0, function* () { }));
cucumber_1.When("Add product into basketQty", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElement(checkout.shoppingBag);
    let bagQty = yield base.checkElementIsPresentAndConvertToNumber(checkout.shoppingBag);
    yield console.log("+++++++++++" + bagQty);
    if (bagQty !== 0) {
        yield protractor_1.browser.sleep(1000);
    }
    else {
        for (let i = 1; i < 100; i++) {
            yield base.waitForElementAndClick(womanPage.productN(i));
            yield base.waitForElement(productPage.productName);
            let isVisible = yield productPage.addToBasketButton.isPresent();
            // let a = isVisible;
            if (isVisible) {
                // element is visible
                yield base.waitForElementAndClick(productPage.addToBasketButton);
                return;
            }
            else {
                // element is not visible
                yield protractor_1.browser.get(womanPage.womanBagsUrl);
            }
        }
    }
}));
cucumber_1.When("Hover over basket icon", () => __awaiter(this, void 0, void 0, function* () {
    yield protractor_1.browser
        .actions()
        .mouseMove(womanPage.basketQty)
        .perform();
}));
cucumber_1.When("Click on checkout button on popup", () => __awaiter(this, void 0, void 0, function* () {
    // await base.checkElementVisible(womanPage.viewBagButton);
    yield base.waitForElement(womanPage.checkoutPopupButton);
    yield base.waitForElementAndClick(womanPage.checkoutPopupButton);
}));
cucumber_1.Then("Direct to checkout page", () => __awaiter(this, void 0, void 0, function* () {
    yield protractor_1.browser.getCurrentUrl().then((result) => __awaiter(this, void 0, void 0, function* () {
        yield expect(result).contain("checkout");
    }));
    // await base.waitForElementAndGetText(checkout.checkoutTitle,"CHECKOUT")
}));
//<-------------------------------------//--------------------------------------->>
//checkout steps
cucumber_1.When("Select payment method", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndClick(checkout.selectPaymentMethod);
}));
cucumber_1.When("Choose Banktransfer method", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndClick(checkout.banktransfer);
}));
cucumber_1.When("Choose CreditCard method", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndClick(checkout.creditDebitCard);
}));
cucumber_1.When("I enter CardNumber", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(checkout.cardNumber, "4111111111111111");
}));
cucumber_1.When("I enter expiry date", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(checkout.expiryDate, "0330");
}));
cucumber_1.When("I enter security code", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(checkout.securityCode, "737");
}));
cucumber_1.When("I enter cardholder name", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndSendkeys(checkout.cardHolderName, "Bi Nguyen");
}));
cucumber_1.When("Save payment method", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndClick(checkout.saveMethodButton);
}));
//apply credit at checkout page
cucumber_1.When("Apply credits", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElement(checkout.checkCredits);
    let isSelected = yield checkout.checkCredits.isSelected();
    if (!isSelected) {
        yield base.waitForElementAndClick(checkout.checkCredits);
        let isSelect = yield checkout.checkCredits.isSelected();
        if (!isSelect) {
            yield base.waitForElementAndClick(checkout.checkCredits);
        }
    }
}));
cucumber_1.When("Apply policy", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndClick(checkout.policy);
    let isSelect = yield checkout.policy.isSelected();
    if (!isSelect) {
        yield base.waitForElementAndClick(checkout.policy);
    }
    let SubTotal = 0;
    let credits = 0;
    let discountCode = 0;
    let totalCheckout = 0;
    let shippingFee = 0;
    SubTotal = yield base.checkElementIsPresentAndConvertToNumber(checkout.SubTotal);
    yield console.log("subtotal" + SubTotal);
    totalCheckout = yield base.checkElementIsPresentAndConvertToNumber(checkout.totalCheckout);
    yield console.log("totalCheckout" + totalCheckout);
    discountCode = yield base.checkElementIsPresentAndConvertToNumber(checkout.discountCode);
    yield console.log("discountCode" + discountCode);
    credits = yield base.checkElementIsPresentAndConvertToNumber(checkout.creditApply);
    yield console.log("credits" + credits);
    shippingFee = yield base.checkElementIsPresentAndConvertToNumber(checkout.shippingFee);
    yield console.log("shippingFee" + shippingFee);
    yield expect(totalCheckout).to.equal(SubTotal + shippingFee - credits - discountCode);
}));
cucumber_1.When("Click on Comfirm and Pay button", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForElementAndClick(checkout.confirmAndPayButton);
    yield protractor_1.browser.sleep(3000);
    yield checkout.alertPopup.isPresent().then((isVisible) => __awaiter(this, void 0, void 0, function* () {
        if (isVisible) {
            // element is visible
            // await base.waitForElementAndClick(checkout.closePopup);
            yield protractor_1.browser.refresh();
            yield base.waitForElementAndClick(checkout.policy);
            yield base.waitForElementAndClick(checkout.confirmAndPayButton);
        }
        else {
            // element is not visible
        }
    }));
}));
cucumber_1.Then("Payment successful", () => __awaiter(this, void 0, void 0, function* () {
    yield base.waitForUrlAndAssert("thankyou");
    // await browser.manage().deleteAllCookies();
    // await browser.get("https://uat-consumer-api.reebonz-dev.com/api/cache/flush")
}));
//<-------------------------------------//--------------------------------------->>
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRkSXRlbXNBbmRDaGVja291dC5zdGVwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3RlcERlZmluYXRpb25zL2FkZEl0ZW1zQW5kQ2hlY2tvdXQuc3RlcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBQUEsdUNBQTZDO0FBQzdDLDJDQUFxQztBQUNyQyxnRUFBNkQ7QUFDN0Qsc0RBQW1EO0FBQ25ELDBFQUF1RTtBQUN2RSw4REFBMkQ7QUFDM0QsOERBQTJEO0FBRTNELGdEQUF3QjtBQUV4QixJQUFJLE1BQU0sR0FBRyxjQUFJLENBQUMsTUFBTSxDQUFDO0FBQ3pCLElBQUksU0FBUyxHQUFHLElBQUksNkJBQWEsRUFBRSxDQUFDO0FBQ3BDLElBQUksSUFBSSxHQUFHLElBQUksbUJBQVEsRUFBRSxDQUFDO0FBQzFCLElBQUksV0FBVyxHQUFHLElBQUksdUNBQWtCLEVBQUUsQ0FBQztBQUMzQyxJQUFJLFFBQVEsR0FBRyxJQUFJLDJCQUFZLEVBQUUsQ0FBQztBQUNsQyxJQUFJLFFBQVEsR0FBRyxJQUFJLDJCQUFZLEVBQUUsQ0FBQztBQUVsQyxtRkFBbUY7QUFDbkYsaUJBQWlCO0FBQ2pCLGdCQUFLLENBQUMsMkJBQTJCLEVBQUUsR0FBUyxFQUFFO0lBQzVDLE1BQU0sb0JBQU8sQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMzQyxNQUFNLG9CQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUMxQyxNQUFNLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQU0sU0FBUyxFQUFDLEVBQUU7UUFDekQsSUFBSSxTQUFTLEVBQUU7WUFDYixxQkFBcUI7WUFDckIsTUFBTSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztTQUNoQzthQUFNO1lBQ0wseUJBQXlCO1NBQzFCO0lBQ0gsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsa0JBQWtCLEVBQUUsR0FBUyxFQUFFO0lBQ2xDLHdEQUF3RDtBQUMxRCxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLG9EQUFvRCxFQUFFLEdBQVMsRUFBRSxnREFBRSxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRTNFLGVBQUksQ0FBQyw0QkFBNEIsRUFBRSxHQUFTLEVBQUU7SUFDNUMsTUFBTSxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUNoRCxJQUFJLE1BQU0sR0FBRyxNQUFNLElBQUksQ0FBQyx1Q0FBdUMsQ0FDN0QsUUFBUSxDQUFDLFdBQVcsQ0FDckIsQ0FBQztJQUNGLE1BQU0sT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDLENBQUM7SUFFMUMsSUFBSSxNQUFNLEtBQUssQ0FBQyxFQUFFO1FBQ2hCLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDM0I7U0FBTTtRQUNMLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDNUIsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3pELE1BQU0sSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDbkQsSUFBSSxTQUFTLEdBQUcsTUFBTSxXQUFXLENBQUMsaUJBQWlCLENBQUMsU0FBUyxFQUFFLENBQUM7WUFDaEUscUJBQXFCO1lBQ3JCLElBQUksU0FBUyxFQUFFO2dCQUNiLHFCQUFxQjtnQkFFckIsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLENBQUM7Z0JBQ2pFLE9BQU87YUFDUjtpQkFBTTtnQkFDTCx5QkFBeUI7Z0JBQ3pCLE1BQU0sb0JBQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxDQUFDO2FBQzNDO1NBQ0Y7S0FDRjtBQUNILENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsd0JBQXdCLEVBQUUsR0FBUyxFQUFFO0lBQ3hDLE1BQU0sb0JBQU87U0FDVixPQUFPLEVBQUU7U0FDVCxTQUFTLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQztTQUM5QixPQUFPLEVBQUUsQ0FBQztBQUNmLENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsbUNBQW1DLEVBQUUsR0FBUyxFQUFFO0lBQ25ELDJEQUEyRDtJQUMzRCxNQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLENBQUM7SUFDekQsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLENBQUM7QUFDbkUsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyx5QkFBeUIsRUFBRSxHQUFTLEVBQUU7SUFDekMsTUFBTSxvQkFBTyxDQUFDLGFBQWEsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFNLE1BQU0sRUFBQyxFQUFFO1FBQ2hELE1BQU0sTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUMzQyxDQUFDLENBQUEsQ0FBQyxDQUFDO0lBQ0gseUVBQXlFO0FBQzNFLENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxtRkFBbUY7QUFDbkYsZ0JBQWdCO0FBRWhCLGVBQUksQ0FBQyx1QkFBdUIsRUFBRSxHQUFTLEVBQUU7SUFDdkMsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLENBQUM7QUFDbEUsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyw0QkFBNEIsRUFBRSxHQUFTLEVBQUU7SUFDNUMsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO0FBQzNELENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsMEJBQTBCLEVBQUUsR0FBUyxFQUFFO0lBQzFDLE1BQU0sSUFBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQztBQUM5RCxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLG9CQUFvQixFQUFFLEdBQVMsRUFBRTtJQUNwQyxNQUFNLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLGtCQUFrQixDQUFDLENBQUM7QUFDaEYsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyxxQkFBcUIsRUFBRSxHQUFTLEVBQUU7SUFDckMsTUFBTSxJQUFJLENBQUMseUJBQXlCLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSxNQUFNLENBQUMsQ0FBQztBQUNwRSxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLHVCQUF1QixFQUFFLEdBQVMsRUFBRTtJQUN2QyxNQUFNLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxRQUFRLENBQUMsWUFBWSxFQUFFLEtBQUssQ0FBQyxDQUFDO0FBQ3JFLENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMseUJBQXlCLEVBQUUsR0FBUyxFQUFFO0lBQ3pDLE1BQU0sSUFBSSxDQUFDLHlCQUF5QixDQUFDLFFBQVEsQ0FBQyxjQUFjLEVBQUUsV0FBVyxDQUFDLENBQUM7QUFDN0UsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyxxQkFBcUIsRUFBRSxHQUFTLEVBQUU7SUFDckMsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLENBQUM7QUFDL0QsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILCtCQUErQjtBQUMvQixlQUFJLENBQUMsZUFBZSxFQUFFLEdBQVMsRUFBRTtJQUMvQixNQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ2pELElBQUksVUFBVSxHQUFHLE1BQU0sUUFBUSxDQUFDLFlBQVksQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUMxRCxJQUFJLENBQUMsVUFBVSxFQUFFO1FBQ2YsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQzNELElBQUksUUFBUSxHQUFHLE1BQU0sUUFBUSxDQUFDLFlBQVksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUN4RCxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2IsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO1NBQzFEO0tBQ0E7QUFFSCxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBQ0gsZUFBSSxDQUFDLGNBQWMsRUFBRSxHQUFTLEVBQUU7SUFDOUIsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ25ELElBQUksUUFBUSxHQUFHLE1BQU0sUUFBUSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUNsRCxJQUFJLENBQUMsUUFBUSxFQUFFO1FBQ2IsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0tBQ3BEO0lBRUQsSUFBSSxRQUFRLEdBQUcsQ0FBQyxDQUFDO0lBQ2pCLElBQUksT0FBTyxHQUFHLENBQUMsQ0FBQztJQUNoQixJQUFJLFlBQVksR0FBRyxDQUFDLENBQUM7SUFDckIsSUFBSSxhQUFhLEdBQUcsQ0FBQyxDQUFDO0lBQ3RCLElBQUksV0FBVyxHQUFHLENBQUMsQ0FBQztJQUVwQixRQUFRLEdBQUcsTUFBTSxJQUFJLENBQUMsdUNBQXVDLENBQzNELFFBQVEsQ0FBQyxRQUFRLENBQ2xCLENBQUM7SUFDRixNQUFNLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxDQUFDO0lBRXpDLGFBQWEsR0FBRyxNQUFNLElBQUksQ0FBQyx1Q0FBdUMsQ0FDaEUsUUFBUSxDQUFDLGFBQWEsQ0FDdkIsQ0FBQztJQUNGLE1BQU0sT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEdBQUcsYUFBYSxDQUFDLENBQUM7SUFFbkQsWUFBWSxHQUFHLE1BQU0sSUFBSSxDQUFDLHVDQUF1QyxDQUMvRCxRQUFRLENBQUMsWUFBWSxDQUN0QixDQUFDO0lBQ0YsTUFBTSxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsR0FBRyxZQUFZLENBQUMsQ0FBQztJQUVqRCxPQUFPLEdBQUcsTUFBTSxJQUFJLENBQUMsdUNBQXVDLENBQzFELFFBQVEsQ0FBQyxXQUFXLENBQ3JCLENBQUM7SUFDRixNQUFNLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxDQUFDO0lBRXZDLFdBQVcsR0FBRyxNQUFNLElBQUksQ0FBQyx1Q0FBdUMsQ0FDOUQsUUFBUSxDQUFDLFdBQVcsQ0FDckIsQ0FBQztJQUNGLE1BQU0sT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEdBQUcsV0FBVyxDQUFDLENBQUM7SUFFL0MsTUFBTSxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FDbEMsUUFBUSxHQUFHLFdBQVcsR0FBRyxPQUFPLEdBQUcsWUFBWSxDQUNoRCxDQUFDO0FBQ0osQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyxpQ0FBaUMsRUFBRSxHQUFTLEVBQUU7SUFDakQsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLENBQUM7SUFDaEUsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMxQixNQUFNLFFBQVEsQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQU0sU0FBUyxFQUFDLEVBQUU7UUFDM0QsSUFBSSxTQUFTLEVBQUU7WUFDYixxQkFBcUI7WUFDckIsMERBQTBEO1lBQzFELE1BQU0sb0JBQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN4QixNQUFNLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDbkQsTUFBTSxJQUFJLENBQUMsc0JBQXNCLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLENBQUM7U0FDakU7YUFBTTtZQUNMLHlCQUF5QjtTQUMxQjtJQUNILENBQUMsQ0FBQSxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLG9CQUFvQixFQUFFLEdBQVMsRUFBRTtJQUNwQyxNQUFNLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUMzQyw2Q0FBNkM7SUFDN0MsZ0ZBQWdGO0FBQ2xGLENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxtRkFBbUYifQ==