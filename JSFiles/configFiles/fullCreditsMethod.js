"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const reporter = __importStar(require("cucumber-html-reporter"));
exports.config = {
    // The address of a running selenium server.
    // seleniumAddress: 'http://localhost:4444/wd/hub',
    directConnect: true,
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    // Capabilities to be passed to the webdriver instance.
    capabilities: {
        browserName: 'chrome',
        chromeOptions: {
            args: [
                '--disable-infobars',
                '--disable-extensions',
                'verbose',
                'log-path=./reports/chromedriver.log',
                "--headless",
                "--no-sandbox",
                "--disable-dev-shm-usage",
                "--disable-extensions",
                "--disable-gpu",
                "--window-size=1920,1080",
                'lang=en-EN'
            ],
            prefs: {
                'profile.password_manager_enabled': false,
                'credentials_enable_service': false,
                'password_manager_enabled': false,
                intl: { accept_languages: "en-EN" }
            }
        },
    },
    // Spec patterns are relative to the configuration file location passed
    // to protractor (in this example conf.js).
    // They may include glob patterns.
    specs: ['../../features/*.feature'],
    SELENIUM_PROMISE_MANAGER: false,
    cucumberOpts: {
        // require step definitions
        tags: "@checkoutUsingFullCredits",
        format: 'json:./report/fullCreditsMethod.json',
        require: [
            '../stepDefinations/*.js',
        ]
    },
    onComplete: () => {
        var options = {
            theme: 'bootstrap',
            jsonFile: './report/fullCreditsMethod.json',
            output: './report/fullCreditsMethod.html',
            reportSuiteAsScenarios: true,
            launchReport: true,
            metadata: {
                "App Version": "0.3.2",
                "Test Environment": "UAT",
                "Browser": "Chrome  73.0.3683.103",
                "Platform": "Windows 10",
                "Parallel": "Scenarios",
                "Executed": "Remote"
            }
        };
        reporter.generate(options);
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZnVsbENyZWRpdHNNZXRob2QuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9jb25maWdGaWxlcy9mdWxsQ3JlZGl0c01ldGhvZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFDQSxpRUFBbUQ7QUFLeEMsUUFBQSxNQUFNLEdBQVc7SUFDeEIsNENBQTRDO0lBQzdDLG1EQUFtRDtJQUNuRCxhQUFhLEVBQUMsSUFBSTtJQUNsQixTQUFTLEVBQUMsUUFBUTtJQUNsQixhQUFhLEVBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQywrQkFBK0IsQ0FBQztJQUc5RCx1REFBdUQ7SUFDdkQsWUFBWSxFQUFFO1FBQ1osV0FBVyxFQUFFLFFBQVE7UUFDckIsYUFBYSxFQUFFO1lBQ2IsSUFBSSxFQUFFO2dCQUNGLG9CQUFvQjtnQkFDcEIsc0JBQXNCO2dCQUN0QixTQUFTO2dCQUNULHFDQUFxQztnQkFDckMsWUFBWTtnQkFDWixjQUFjO2dCQUNkLHlCQUF5QjtnQkFDekIsc0JBQXNCO2dCQUN0QixlQUFlO2dCQUNmLHlCQUF5QjtnQkFDekIsWUFBWTthQUNmO1lBQ0QsS0FBSyxFQUFFO2dCQUNILGtDQUFrQyxFQUFFLEtBQUs7Z0JBQ3pDLDRCQUE0QixFQUFFLEtBQUs7Z0JBQ25DLDBCQUEwQixFQUFFLEtBQUs7Z0JBQ2pDLElBQUksRUFBRSxFQUFFLGdCQUFnQixFQUFFLE9BQU8sRUFBRTthQUN0QztTQUNKO0tBQ0E7SUFFRCx1RUFBdUU7SUFDdkUsMkNBQTJDO0lBQzNDLGtDQUFrQztJQUNsQyxLQUFLLEVBQUUsQ0FBQywwQkFBMEIsQ0FBQztJQUNuQyx3QkFBd0IsRUFBRSxLQUFLO0lBQy9CLFlBQVksRUFBRTtRQUNWLDJCQUEyQjtRQUMzQixJQUFJLEVBQUMsMkJBQTJCO1FBQ2hDLE1BQU0sRUFBQyxzQ0FBc0M7UUFHN0MsT0FBTyxFQUFFO1lBQ1AseUJBQXlCO1NBRTFCO0tBQ0Y7SUFDRCxVQUFVLEVBQUUsR0FBRyxFQUFFO1FBQ2YsSUFBSSxPQUFPLEdBQUc7WUFDWixLQUFLLEVBQUUsV0FBVztZQUNsQixRQUFRLEVBQUUsaUNBQWlDO1lBQzNDLE1BQU0sRUFBRSxpQ0FBaUM7WUFDekMsc0JBQXNCLEVBQUUsSUFBSTtZQUM1QixZQUFZLEVBQUUsSUFBSTtZQUNsQixRQUFRLEVBQUU7Z0JBQ04sYUFBYSxFQUFDLE9BQU87Z0JBQ3JCLGtCQUFrQixFQUFFLEtBQUs7Z0JBQ3pCLFNBQVMsRUFBRSx1QkFBdUI7Z0JBQ2xDLFVBQVUsRUFBRSxZQUFZO2dCQUN4QixVQUFVLEVBQUUsV0FBVztnQkFDdkIsVUFBVSxFQUFFLFFBQVE7YUFDdkI7U0FDSixDQUFDO1FBRUYsUUFBUSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUczQixDQUFDO0NBSUosQ0FBQyJ9