"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const reporter = __importStar(require("cucumber-html-reporter"));
exports.config = {
    // The address of a running selenium server.
    // seleniumAddress: 'http://localhost:4444/wd/hub',
    directConnect: true,
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    // Capabilities to be passed to the webdriver instance.
    capabilities: {
        browserName: 'chrome',
        chromeOptions: {
            args: [
                '--disable-infobars',
                '--disable-extensions',
                'verbose',
                'log-path=./reports/chromedriver.log',
                "--headless",
                "--no-sandbox",
                "--disable-dev-shm-usage",
                "--disable-extensions",
                "--disable-gpu",
                "--window-size=1920,1080"
            ],
            prefs: {
                'profile.password_manager_enabled': false,
                'credentials_enable_service': false,
                'password_manager_enabled': false
            }
        },
    },
    // Spec patterns are relative to the configuration file location passed
    // to protractor (in this example conf.js).
    // They may include glob patterns.
    specs: ['../../features/*.feature'],
    SELENIUM_PROMISE_MANAGER: false,
    cucumberOpts: {
        // require step definitions
        tags: "@checkoutUsingCreditCard",
        format: 'json:./report/creditCardMethod.json',
        require: [
            '../stepDefinations/*.js',
        ]
    },
    onComplete: () => {
        var options = {
            theme: 'bootstrap',
            jsonFile: './report/creditCardMethod.json',
            output: './report/creditCardMethod.html',
            reportSuiteAsScenarios: true,
            launchReport: true,
            metadata: {
                "Test Environment": "UAT",
                "Browser": "Chrome  73.0.3683.103",
                "Platform": "Windows 10",
                "Parallel": "Scenarios",
                "Executed": "Remote"
            }
        };
        reporter.generate(options);
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3JlZGl0Q2FyZE1ldGhvZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL2NvbmZpZ0ZpbGVzL2NyZWRpdENhcmRNZXRob2QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQ0EsaUVBQW1EO0FBS3hDLFFBQUEsTUFBTSxHQUFXO0lBQ3hCLDRDQUE0QztJQUM3QyxtREFBbUQ7SUFDbkQsYUFBYSxFQUFDLElBQUk7SUFDbEIsU0FBUyxFQUFDLFFBQVE7SUFDbEIsYUFBYSxFQUFFLE9BQU8sQ0FBQyxPQUFPLENBQUMsK0JBQStCLENBQUM7SUFHOUQsdURBQXVEO0lBQ3ZELFlBQVksRUFBRTtRQUNaLFdBQVcsRUFBRSxRQUFRO1FBQ3JCLGFBQWEsRUFBRTtZQUNiLElBQUksRUFBRTtnQkFDRixvQkFBb0I7Z0JBQ3BCLHNCQUFzQjtnQkFDdEIsU0FBUztnQkFDVCxxQ0FBcUM7Z0JBQ3JDLFlBQVk7Z0JBQ1osY0FBYztnQkFDZCx5QkFBeUI7Z0JBQ3pCLHNCQUFzQjtnQkFDdEIsZUFBZTtnQkFDZix5QkFBeUI7YUFDNUI7WUFDRCxLQUFLLEVBQUU7Z0JBQ0gsa0NBQWtDLEVBQUUsS0FBSztnQkFDekMsNEJBQTRCLEVBQUUsS0FBSztnQkFDbkMsMEJBQTBCLEVBQUUsS0FBSzthQUNwQztTQUNKO0tBQ0E7SUFFRCx1RUFBdUU7SUFDdkUsMkNBQTJDO0lBQzNDLGtDQUFrQztJQUNsQyxLQUFLLEVBQUUsQ0FBQywwQkFBMEIsQ0FBQztJQUNuQyx3QkFBd0IsRUFBRSxLQUFLO0lBQy9CLFlBQVksRUFBRTtRQUNWLDJCQUEyQjtRQUMzQixJQUFJLEVBQUMsMEJBQTBCO1FBQy9CLE1BQU0sRUFBQyxxQ0FBcUM7UUFHNUMsT0FBTyxFQUFFO1lBQ1AseUJBQXlCO1NBRTFCO0tBQ0Y7SUFDRCxVQUFVLEVBQUUsR0FBRyxFQUFFO1FBQ2YsSUFBSSxPQUFPLEdBQUc7WUFDWixLQUFLLEVBQUUsV0FBVztZQUNsQixRQUFRLEVBQUUsZ0NBQWdDO1lBQzFDLE1BQU0sRUFBRSxnQ0FBZ0M7WUFDeEMsc0JBQXNCLEVBQUUsSUFBSTtZQUM1QixZQUFZLEVBQUUsSUFBSTtZQUNsQixRQUFRLEVBQUU7Z0JBQ04sa0JBQWtCLEVBQUUsS0FBSztnQkFDekIsU0FBUyxFQUFFLHVCQUF1QjtnQkFDbEMsVUFBVSxFQUFFLFlBQVk7Z0JBQ3hCLFVBQVUsRUFBRSxXQUFXO2dCQUN2QixVQUFVLEVBQUUsUUFBUTthQUN2QjtTQUNKLENBQUM7UUFFRixRQUFRLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBRzNCLENBQUM7Q0FJSixDQUFDIn0=