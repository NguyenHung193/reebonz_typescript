"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const reporter = __importStar(require("cucumber-html-reporter"));
exports.config = {
    // The address of a running selenium server.
    // seleniumAddress: 'http://localhost:4444/wd/hub',
    directConnect: true,
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    // Capabilities to be passed to the webdriver instance.
    capabilities: {
        browserName: 'chrome',
        chromeOptions: {
            args: [
                '--disable-infobars',
                '--disable-extensions',
                'verbose',
                'log-path=./reports/chromedriver.log',
                "--headless",
                "--no-sandbox",
                "--disable-dev-shm-usage",
                "--disable-extensions",
                "--disable-gpu",
                "--window-size=1280,720"
            ],
            prefs: {
                'profile.password_manager_enabled': false,
                'credentials_enable_service': false,
                'password_manager_enabled': false
            }
        },
    },
    // Spec patterns are relative to the configuration file location passed
    // to protractor (in this example conf.js).
    // They may include glob patterns.
    specs: ['../../features/*.feature'],
    SELENIUM_PROMISE_MANAGER: false,
    cucumberOpts: {
        // require step definitions
        tags: "@UIUserPopup",
        format: 'json:./report/UIUserPopUp.json',
        require: [
            '../stepDefinations/*.js',
        ]
    },
    onComplete: () => {
        var options = {
            theme: 'bootstrap',
            jsonFile: './report/UIUserPopUp.json',
            output: './report/UIUserPopUp.html',
            reportSuiteAsScenarios: true,
            launchReport: true,
            metadata: {
                "App Version": "0.3.2",
                "Test Environment": "UAT",
                "Browser": "Chrome  73.0.3683.103",
                "Platform": "Windows 10",
                "Parallel": "Scenarios",
                "Executed": "Remote"
            }
        };
        reporter.generate(options);
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVUlVc2VyUG9wdXBDb25maWcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9jb25maWdGaWxlcy9VSVVzZXJQb3B1cENvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFDQSxpRUFBbUQ7QUFLeEMsUUFBQSxNQUFNLEdBQVc7SUFDeEIsNENBQTRDO0lBQzdDLG1EQUFtRDtJQUNuRCxhQUFhLEVBQUMsSUFBSTtJQUNsQixTQUFTLEVBQUMsUUFBUTtJQUNsQixhQUFhLEVBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQywrQkFBK0IsQ0FBQztJQUc5RCx1REFBdUQ7SUFDdkQsWUFBWSxFQUFFO1FBQ1osV0FBVyxFQUFFLFFBQVE7UUFDckIsYUFBYSxFQUFFO1lBQ2IsSUFBSSxFQUFFO2dCQUNGLG9CQUFvQjtnQkFDcEIsc0JBQXNCO2dCQUN0QixTQUFTO2dCQUNULHFDQUFxQztnQkFDckMsWUFBWTtnQkFDWixjQUFjO2dCQUNkLHlCQUF5QjtnQkFDekIsc0JBQXNCO2dCQUN0QixlQUFlO2dCQUNmLHdCQUF3QjthQUMzQjtZQUNELEtBQUssRUFBRTtnQkFDSCxrQ0FBa0MsRUFBRSxLQUFLO2dCQUN6Qyw0QkFBNEIsRUFBRSxLQUFLO2dCQUNuQywwQkFBMEIsRUFBRSxLQUFLO2FBQ3BDO1NBQ0o7S0FDQTtJQUVELHVFQUF1RTtJQUN2RSwyQ0FBMkM7SUFDM0Msa0NBQWtDO0lBQ2xDLEtBQUssRUFBRSxDQUFDLDBCQUEwQixDQUFDO0lBQ25DLHdCQUF3QixFQUFFLEtBQUs7SUFJL0IsWUFBWSxFQUFFO1FBQ1YsMkJBQTJCO1FBQzNCLElBQUksRUFBQyxjQUFjO1FBQ25CLE1BQU0sRUFBQyxnQ0FBZ0M7UUFHdkMsT0FBTyxFQUFFO1lBQ1AseUJBQXlCO1NBRTFCO0tBQ0Y7SUFDRCxVQUFVLEVBQUUsR0FBRyxFQUFFO1FBQ2YsSUFBSSxPQUFPLEdBQUc7WUFDWixLQUFLLEVBQUUsV0FBVztZQUNsQixRQUFRLEVBQUUsMkJBQTJCO1lBQ3JDLE1BQU0sRUFBRSwyQkFBMkI7WUFDbkMsc0JBQXNCLEVBQUUsSUFBSTtZQUM1QixZQUFZLEVBQUUsSUFBSTtZQUNsQixRQUFRLEVBQUU7Z0JBQ04sYUFBYSxFQUFDLE9BQU87Z0JBQ3JCLGtCQUFrQixFQUFFLEtBQUs7Z0JBQ3pCLFNBQVMsRUFBRSx1QkFBdUI7Z0JBQ2xDLFVBQVUsRUFBRSxZQUFZO2dCQUN4QixVQUFVLEVBQUUsV0FBVztnQkFDdkIsVUFBVSxFQUFFLFFBQVE7YUFDdkI7U0FDSixDQUFDO1FBRUYsUUFBUSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUczQixDQUFDO0NBSUosQ0FBQyJ9