"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
class myAccountPage {
    constructor() {
        this.label = protractor_1.element(protractor_1.by.xpath("//h3[@class='inlineBlock']"));
        this.userName = protractor_1.element(protractor_1.by.xpath("//div[@class='userMenuWrapper']//h3//a[@class='userName']"));
        this.firstName = protractor_1.element(protractor_1.by.xpath("//input[@id='firstName']"));
        this.lastName = protractor_1.element(protractor_1.by.xpath("//input[@id='lastName']"));
        this.country = protractor_1.element(protractor_1.by.xpath("//input[@id='countryName']"));
        this.language = protractor_1.element(protractor_1.by.xpath("//select[@id='language']//option[1]"));
        this.email = protractor_1.element(protractor_1.by.xpath("//input[@id='Email']"));
        this.phoneNumber = protractor_1.element(protractor_1.by.xpath("//input[@id='TelephoneNumber']"));
        this.gender = protractor_1.element(protractor_1.by.xpath("//select[@id='gender']"));
        this.saveProfileButton = protractor_1.element(protractor_1.by.xpath("//button[@class='btn min150 saveProfile']"));
        this.oldPassword = protractor_1.element(protractor_1.by.xpath("//input[@id='oldPassword']"));
        this.newPassword = protractor_1.element(protractor_1.by.xpath("//input[@id='newPassword']"));
        this.confirmPassword = protractor_1.element(protractor_1.by.xpath("//input[@id='confirmNewPassword']"));
        this.saveChangePassword = protractor_1.element(protractor_1.by.xpath("//button[@class='btn min150 changePassword']"));
    }
}
exports.myAccountPage = myAccountPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlBY2NvdW50UGFnZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3BhZ2VPYmplY3RzL215QWNjb3VudFBhZ2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSwyQ0FBd0Q7QUFFeEQsTUFBYSxhQUFhO0lBZXhCO1FBQ0UsSUFBSSxDQUFDLEtBQUssR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsNEJBQTRCLENBQUMsQ0FBQyxDQUFDO1FBQzdELElBQUksQ0FBQyxRQUFRLEdBQUcsb0JBQU8sQ0FDckIsZUFBRSxDQUFDLEtBQUssQ0FBQywyREFBMkQsQ0FBQyxDQUN0RSxDQUFDO1FBQ0YsSUFBSSxDQUFDLFNBQVMsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxDQUFDO1FBQy9ELElBQUksQ0FBQyxRQUFRLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQztRQUM3RCxJQUFJLENBQUMsT0FBTyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDLENBQUM7UUFDL0QsSUFBSSxDQUFDLFFBQVEsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMscUNBQXFDLENBQUMsQ0FBQyxDQUFDO1FBQ3pFLElBQUksQ0FBQyxLQUFLLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQztRQUN2RCxJQUFJLENBQUMsV0FBVyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDLENBQUM7UUFDdkUsSUFBSSxDQUFDLE1BQU0sR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxDQUFDO1FBQzFELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxvQkFBTyxDQUM5QixlQUFFLENBQUMsS0FBSyxDQUFDLDJDQUEyQyxDQUFDLENBQ3RELENBQUM7UUFDRixJQUFJLENBQUMsV0FBVyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLFdBQVcsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsNEJBQTRCLENBQUMsQ0FBQyxDQUFDO1FBQ25FLElBQUksQ0FBQyxlQUFlLEdBQUcsb0JBQU8sQ0FDNUIsZUFBRSxDQUFDLEtBQUssQ0FBQyxtQ0FBbUMsQ0FBQyxDQUM5QyxDQUFDO1FBQ0YsSUFBSSxDQUFDLGtCQUFrQixHQUFHLG9CQUFPLENBQy9CLGVBQUUsQ0FBQyxLQUFLLENBQUMsOENBQThDLENBQUMsQ0FDekQsQ0FBQztJQUNKLENBQUM7Q0FDRjtBQXZDRCxzQ0F1Q0MifQ==