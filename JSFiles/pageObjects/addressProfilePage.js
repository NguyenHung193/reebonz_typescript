"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
class addressProfilePage {
    constructor() {
        this.addAddressProfilePageUrl =
            "https://staging-www.reebonz-dev.com/sg/account/addresses";
        this.addNewAddressButton = protractor_1.element(protractor_1.by.xpath("//a[@class='btn btn-xs btnTextOnly gold addNewAddress js-showAddressBox']"));
        this.fullName = protractor_1.element(protractor_1.by.xpath("//input[@id='address_name']"));
        this.address = protractor_1.element(protractor_1.by.xpath("//input[@id='address_Line1']"));
        this.selectCountry = protractor_1.element(protractor_1.by.xpath("//input[@name='address_Line1']"));
        this.postalCode = protractor_1.element(protractor_1.by.xpath("//input[@id='address_PostalCode']"));
        this.phoneNumber = protractor_1.element(protractor_1.by.xpath("//input[@id='TelephoneNumber']"));
        this.submitButton = protractor_1.element(protractor_1.by.xpath("//input[@id='addressButton']"));
        this.assertName = protractor_1.element(protractor_1.by.xpath("//span[@class='js-Name']"));
        this.ProfileUrl = "https://staging-www.reebonz-dev.com/sg/account/profile";
        this.emailProfile = protractor_1.element(protractor_1.by.xpath("//input[@id='Email']"));
        this.cityName = protractor_1.element(protractor_1.by.xpath("//input[@id='address_TownOrCity']"));
        this.creditProfileUrl =
            "https://uat-www.reebonz-dev.com/sg/account/credits";
        this.addressLabel = protractor_1.element(protractor_1.by.xpath("//h3[contains(text(),'Addresses')]"));
        this.addNewAddressButtonExist = protractor_1.element(protractor_1.by.xpath("//a[@class='btn btn-xs btnTextOnly gold addNewAddress js-showAddressBox']"));
        this.subName = protractor_1.element(protractor_1.by.xpath("//div[@class='addressDetail']//span[@class='js-Name']"));
        this.subAddress = protractor_1.element(protractor_1.by.xpath("//div[@class='addressDetail']//span[@class='js-Line1']"));
        this.subCountry = protractor_1.element(protractor_1.by.xpath("//div[@class='addressDetail']//span[@class='js-CountryName']"));
        this.subPostCode = protractor_1.element(protractor_1.by.xpath("//div[@class='addressDetail']//span[@class='js-PostalCode']"));
        this.subPhone = protractor_1.element(protractor_1.by.xpath("//div[@class='addressDetail']//span[@class='js-ContactNumber']"));
    }
}
exports.addressProfilePage = addressProfilePage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRkcmVzc1Byb2ZpbGVQYWdlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vcGFnZU9iamVjdHMvYWRkcmVzc1Byb2ZpbGVQYWdlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsMkNBQXdEO0FBRXhELE1BQWEsa0JBQWtCO0lBdUI3QjtRQUNFLElBQUksQ0FBQyx3QkFBd0I7WUFDM0IsMERBQTBELENBQUM7UUFDN0QsSUFBSSxDQUFDLG1CQUFtQixHQUFHLG9CQUFPLENBQ2hDLGVBQUUsQ0FBQyxLQUFLLENBQ04sMkVBQTJFLENBQzVFLENBQ0YsQ0FBQztRQUNGLElBQUksQ0FBQyxRQUFRLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDZCQUE2QixDQUFDLENBQUMsQ0FBQztRQUNqRSxJQUFJLENBQUMsT0FBTyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDLENBQUM7UUFDakUsSUFBSSxDQUFDLGFBQWEsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsZ0NBQWdDLENBQUMsQ0FBQyxDQUFDO1FBQ3pFLElBQUksQ0FBQyxVQUFVLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLG1DQUFtQyxDQUFDLENBQUMsQ0FBQztRQUN6RSxJQUFJLENBQUMsV0FBVyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDLENBQUM7UUFDdkUsSUFBSSxDQUFDLFlBQVksR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsOEJBQThCLENBQUMsQ0FBQyxDQUFDO1FBQ3RFLElBQUksQ0FBQyxVQUFVLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDBCQUEwQixDQUFDLENBQUMsQ0FBQztRQUNoRSxJQUFJLENBQUMsVUFBVSxHQUFHLHdEQUF3RCxDQUFDO1FBQzNFLElBQUksQ0FBQyxZQUFZLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQztRQUM5RCxJQUFJLENBQUMsUUFBUSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDLENBQUM7UUFDdkUsSUFBSSxDQUFDLGdCQUFnQjtZQUNuQixvREFBb0QsQ0FBQztRQUN2RCxJQUFJLENBQUMsWUFBWSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDLENBQUM7UUFDNUUsSUFBSSxDQUFDLHdCQUF3QixHQUFHLG9CQUFPLENBQ3JDLGVBQUUsQ0FBQyxLQUFLLENBQ04sMkVBQTJFLENBQzVFLENBQ0YsQ0FBQztRQUNGLElBQUksQ0FBQyxPQUFPLEdBQUcsb0JBQU8sQ0FDcEIsZUFBRSxDQUFDLEtBQUssQ0FBQyx1REFBdUQsQ0FBQyxDQUNsRSxDQUFDO1FBQ0YsSUFBSSxDQUFDLFVBQVUsR0FBRyxvQkFBTyxDQUN2QixlQUFFLENBQUMsS0FBSyxDQUFDLHdEQUF3RCxDQUFDLENBQ25FLENBQUM7UUFDRixJQUFJLENBQUMsVUFBVSxHQUFHLG9CQUFPLENBQ3ZCLGVBQUUsQ0FBQyxLQUFLLENBQUMsOERBQThELENBQUMsQ0FDekUsQ0FBQztRQUNGLElBQUksQ0FBQyxXQUFXLEdBQUcsb0JBQU8sQ0FDeEIsZUFBRSxDQUFDLEtBQUssQ0FBQyw2REFBNkQsQ0FBQyxDQUN4RSxDQUFDO1FBQ0YsSUFBSSxDQUFDLFFBQVEsR0FBRyxvQkFBTyxDQUNyQixlQUFFLENBQUMsS0FBSyxDQUFDLGdFQUFnRSxDQUFDLENBQzNFLENBQUM7SUFDSixDQUFDO0NBQ0Y7QUFqRUQsZ0RBaUVDIn0=