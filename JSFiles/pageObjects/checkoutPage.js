"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
class checkoutPage {
    constructor() {
        this.checkCredits = protractor_1.element(protractor_1.by.xpath("//input[@class='js-credits']"));
        this.addCoupon = protractor_1.element(protractor_1.by.xpath("//a[@class='addNow underline textGrey js-addPromoCode']"));
        this.selectPaymentMethod = protractor_1.element(protractor_1.by.xpath("//a[@class='btn btn-xs changeBtn hidden-xs btnTextOnly gold js-showPaymentBox']"));
        this.creditDebitCard = protractor_1.element(protractor_1.by.xpath("//div[@id='headingCreditCard']//input[@name='paymentType']"));
        this.IPP = protractor_1.element(protractor_1.by.xpath("//div[@id='headingIPP']//input[@name='paymentType']"));
        this.paypal = protractor_1.element(protractor_1.by.xpath("//div[@id='headingIPP']//input[@name='paymentType']"));
        this.banktransfer = protractor_1.element(protractor_1.by.xpath("//div[@id='headingBankTransfer']//input[@name='paymentType']"));
        this.saveMethodButton = protractor_1.element(protractor_1.by.xpath("//a[@class='btn js-paymentMethodBtn']"));
        this.policy = protractor_1.element(protractor_1.by.xpath("//input[@class='js-policy']"));
        this.checkoutUrl = "https://uat-www.reebonz-dev.com/sg/checkout";
        this.checkoutTitle = protractor_1.element(protractor_1.by.xpath("//h1[contains(text(),'Checkout')]"));
        this.confirmAndPayButton = protractor_1.element(protractor_1.by.xpath("//a[@class='btn js-placeOrder']"));
        this.changeButton = protractor_1.element(protractor_1.by.xpath("//a[@class='btn btn-xs changeBtn hidden-xs btnTextOnly gold js-showPaymentBox']"));
        this.cardNumber = protractor_1.element(protractor_1.by.xpath("//input[@id='cc_number']"));
        this.expiryDate = protractor_1.element(protractor_1.by.xpath("//input[@id='cc_expiry_date']"));
        this.securityCode = protractor_1.element(protractor_1.by.xpath("//input[@id='cc_cvc']"));
        this.cardHolderName = protractor_1.element(protractor_1.by.xpath("//input[@id='cc_name']"));
        this.alertPopup = protractor_1.element(protractor_1.by.xpath("//div[@class='alertWrapper']"));
        this.closePopup = protractor_1.element(protractor_1.by.xpath("//div[@class='alertWrapper']//a"));
        this.SubTotal = protractor_1.element(protractor_1.by.xpath("//div[@class='col-xs-7 alignR uppercase']//strong[1]"));
        this.totalCheckout = protractor_1.element(protractor_1.by.xpath("//div[@class='col-xs-7 alignR']//strong[1]"));
        this.creditApply = protractor_1.element(protractor_1.by.xpath("//div[@class='col-xs-7 alignR availableCredits']"));
        this.discountCode = protractor_1.element(protractor_1.by.xpath("//li[@class='coupon']//div[@class='row']//div[@class='col-xs-7 alignR']"));
        this.shippingFee = protractor_1.element(protractor_1.by.xpath("//li[@class='shipping']//div//div[@class='col-xs-7 alignR']"));
        this.shoppingBag = protractor_1.element(protractor_1.by.xpath("//span[@class='cartCount js-cartCount']"));
    }
}
exports.checkoutPage = checkoutPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hlY2tvdXRQYWdlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vcGFnZU9iamVjdHMvY2hlY2tvdXRQYWdlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsMkNBQXdEO0FBRXhELE1BQWEsWUFBWTtJQTRCdkI7UUFDRSxJQUFJLENBQUMsWUFBWSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDLENBQUM7UUFDdEUsSUFBSSxDQUFDLFNBQVMsR0FBRyxvQkFBTyxDQUN0QixlQUFFLENBQUMsS0FBSyxDQUFDLHlEQUF5RCxDQUFDLENBQ3BFLENBQUM7UUFDRixJQUFJLENBQUMsbUJBQW1CLEdBQUcsb0JBQU8sQ0FDaEMsZUFBRSxDQUFDLEtBQUssQ0FDTixpRkFBaUYsQ0FDbEYsQ0FDRixDQUFDO1FBQ0YsSUFBSSxDQUFDLGVBQWUsR0FBRyxvQkFBTyxDQUM1QixlQUFFLENBQUMsS0FBSyxDQUFDLDREQUE0RCxDQUFDLENBQ3ZFLENBQUM7UUFDRixJQUFJLENBQUMsR0FBRyxHQUFHLG9CQUFPLENBQ2hCLGVBQUUsQ0FBQyxLQUFLLENBQUMscURBQXFELENBQUMsQ0FDaEUsQ0FBQztRQUNGLElBQUksQ0FBQyxNQUFNLEdBQUcsb0JBQU8sQ0FDbkIsZUFBRSxDQUFDLEtBQUssQ0FBQyxxREFBcUQsQ0FBQyxDQUNoRSxDQUFDO1FBQ0YsSUFBSSxDQUFDLFlBQVksR0FBRyxvQkFBTyxDQUN6QixlQUFFLENBQUMsS0FBSyxDQUFDLDhEQUE4RCxDQUFDLENBQ3pFLENBQUM7UUFDRixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsb0JBQU8sQ0FDN0IsZUFBRSxDQUFDLEtBQUssQ0FBQyx1Q0FBdUMsQ0FBQyxDQUNsRCxDQUFDO1FBQ0YsSUFBSSxDQUFDLE1BQU0sR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsNkJBQTZCLENBQUMsQ0FBQyxDQUFDO1FBQy9ELElBQUksQ0FBQyxXQUFXLEdBQUcsNkNBQTZDLENBQUM7UUFDakUsSUFBSSxDQUFDLGFBQWEsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsbUNBQW1DLENBQUMsQ0FBQyxDQUFDO1FBQzVFLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxvQkFBTyxDQUNoQyxlQUFFLENBQUMsS0FBSyxDQUFDLGlDQUFpQyxDQUFDLENBQzVDLENBQUM7UUFDRixJQUFJLENBQUMsWUFBWSxHQUFHLG9CQUFPLENBQ3pCLGVBQUUsQ0FBQyxLQUFLLENBQ04saUZBQWlGLENBQ2xGLENBQ0YsQ0FBQztRQUNGLElBQUksQ0FBQyxVQUFVLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDBCQUEwQixDQUFDLENBQUMsQ0FBQztRQUNoRSxJQUFJLENBQUMsVUFBVSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQywrQkFBK0IsQ0FBQyxDQUFDLENBQUM7UUFDckUsSUFBSSxDQUFDLFlBQVksR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDO1FBQy9ELElBQUksQ0FBQyxjQUFjLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHdCQUF3QixDQUFDLENBQUMsQ0FBQztRQUNsRSxJQUFJLENBQUMsVUFBVSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLFVBQVUsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsaUNBQWlDLENBQUMsQ0FBQyxDQUFDO1FBQ3ZFLElBQUksQ0FBQyxRQUFRLEdBQUcsb0JBQU8sQ0FDckIsZUFBRSxDQUFDLEtBQUssQ0FBQyxzREFBc0QsQ0FBQyxDQUNqRSxDQUFDO1FBQ0YsSUFBSSxDQUFDLGFBQWEsR0FBRyxvQkFBTyxDQUMxQixlQUFFLENBQUMsS0FBSyxDQUFDLDRDQUE0QyxDQUFDLENBQ3ZELENBQUM7UUFDRixJQUFJLENBQUMsV0FBVyxHQUFHLG9CQUFPLENBQ3hCLGVBQUUsQ0FBQyxLQUFLLENBQUMsa0RBQWtELENBQUMsQ0FDN0QsQ0FBQztRQUNGLElBQUksQ0FBQyxZQUFZLEdBQUcsb0JBQU8sQ0FDekIsZUFBRSxDQUFDLEtBQUssQ0FDTix5RUFBeUUsQ0FDMUUsQ0FDRixDQUFDO1FBQ0YsSUFBSSxDQUFDLFdBQVcsR0FBRyxvQkFBTyxDQUN4QixlQUFFLENBQUMsS0FBSyxDQUFDLDZEQUE2RCxDQUFDLENBQ3hFLENBQUM7UUFDRixJQUFJLENBQUMsV0FBVyxHQUFHLG9CQUFPLENBQ3hCLGVBQUUsQ0FBQyxLQUFLLENBQUMseUNBQXlDLENBQUMsQ0FDcEQsQ0FBQztJQUNKLENBQUM7Q0FDRjtBQTNGRCxvQ0EyRkMifQ==