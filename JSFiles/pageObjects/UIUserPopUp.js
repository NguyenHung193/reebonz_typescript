"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
class UIUserPopUp {
    constructor() {
        this.logoutButton = protractor_1.element(protractor_1.by.xpath("//a[@class='js-logout']"));
        this.userName = protractor_1.element(protractor_1.by.xpath("//span[@class='js-name']"));
        this.address = protractor_1.element(protractor_1.by.xpath("//a[contains(text(),'Addresses')]"));
        this.credits = protractor_1.element(protractor_1.by.xpath("//a[contains(text(),'Credits')]"));
        this.wishlist = protractor_1.element(protractor_1.by.xpath("//a[contains(text(),'Wishlist')]"));
        this.whiteGlove = protractor_1.element(protractor_1.by.xpath("//li[@class='js-menuWhiteGlove']//a[contains(text(),'White Glove')]"));
        this.sellback = protractor_1.element(protractor_1.by.xpath("//div[@class='userMenuWrapper']//a[contains(text(),'Orders')]/parent::li/following-sibling::li[1]//a[contains(text(),'Sell Back')]"));
        this.orders = protractor_1.element(protractor_1.by.xpath("//div[@class='userMenuWrapper']//a[contains(text(),'Orders')]"));
        this.inbox = protractor_1.element(protractor_1.by.xpath("//div[@class='userMenuWrapper']//a[contains(text(),'Inbox')]"));
        this.boutique = protractor_1.element(protractor_1.by.xpath("//div[@class='userMenuWrapper']//a[contains(text(),'Boutiques I Follow')]"));
    }
}
exports.UIUserPopUp = UIUserPopUp;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVUlVc2VyUG9wVXAuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9wYWdlT2JqZWN0cy9VSVVzZXJQb3BVcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLDJDQUF3RDtBQUV4RCxNQUFhLFdBQVc7SUFhdEI7UUFDRSxJQUFJLENBQUMsWUFBWSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLENBQUM7UUFDakUsSUFBSSxDQUFDLFFBQVEsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxDQUFDO1FBQzlELElBQUksQ0FBQyxPQUFPLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLG1DQUFtQyxDQUFDLENBQUMsQ0FBQztRQUN0RSxJQUFJLENBQUMsT0FBTyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLFFBQVEsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsa0NBQWtDLENBQUMsQ0FBQyxDQUFDO1FBQ3RFLElBQUksQ0FBQyxVQUFVLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHFFQUFxRSxDQUFDLENBQUMsQ0FBQztRQUMzRyxJQUFJLENBQUMsUUFBUSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxvSUFBb0ksQ0FBQyxDQUFDLENBQUM7UUFDeEssSUFBSSxDQUFDLE1BQU0sR0FBRyxvQkFBTyxDQUNuQixlQUFFLENBQUMsS0FBSyxDQUNOLCtEQUErRCxDQUNoRSxDQUNGLENBQUM7UUFDRixJQUFJLENBQUMsS0FBSyxHQUFHLG9CQUFPLENBQ2xCLGVBQUUsQ0FBQyxLQUFLLENBQUMsOERBQThELENBQUMsQ0FDekUsQ0FBQztRQUNGLElBQUksQ0FBQyxRQUFRLEdBQUcsb0JBQU8sQ0FDckIsZUFBRSxDQUFDLEtBQUssQ0FDTiwyRUFBMkUsQ0FDNUUsQ0FDRixDQUFDO0lBQ0osQ0FBQztDQUNGO0FBbkNELGtDQW1DQyJ9