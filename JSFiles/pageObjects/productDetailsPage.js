"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
class productDetailsPage {
    constructor() {
        this.basketQty = protractor_1.element(protractor_1.by.xpath("//div[@class='navWrapper']/a[@href='/sg/cart']"));
        this.addToBasketButton = protractor_1.element(protractor_1.by.xpath("//button[@name='AddToBasket']"));
        this.productName = protractor_1.element(protractor_1.by.xpath("//div[@class='productInfo']//h2"));
    }
}
exports.productDetailsPage = productDetailsPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZHVjdERldGFpbHNQYWdlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vcGFnZU9iamVjdHMvcHJvZHVjdERldGFpbHNQYWdlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsMkNBQXdEO0FBRXhELE1BQWEsa0JBQWtCO0lBSzdCO1FBQ0UsSUFBSSxDQUFDLFNBQVMsR0FBRyxvQkFBTyxDQUN0QixlQUFFLENBQUMsS0FBSyxDQUFDLGdEQUFnRCxDQUFDLENBQzNELENBQUM7UUFDRixJQUFJLENBQUMsaUJBQWlCLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLCtCQUErQixDQUFDLENBQUMsQ0FBQztRQUM1RSxJQUFJLENBQUMsV0FBVyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDLENBQUM7SUFDMUUsQ0FBQztDQUNGO0FBWkQsZ0RBWUMifQ==