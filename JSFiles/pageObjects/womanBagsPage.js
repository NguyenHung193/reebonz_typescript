"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
class womanBagsPage {
    constructor() {
        this.womanBagsUrl =
            "https://uat-www.reebonz-dev.com/sg/women/designer-bags";
        this.product = protractor_1.element(protractor_1.by.xpath("(//img[@class='js-imgPreload'])[1]"));
        this.productN = (numb) => {
            return protractor_1.element(protractor_1.by.xpath("(//img[@class='js-imgPreload'])[" + numb + "]"));
        };
        this.product2 = protractor_1.element(protractor_1.by.xpath("(//img[@class='js-imgPreload'])[4]"));
        this.product3 = protractor_1.element(protractor_1.by.xpath("(//img[@class='js-imgPreload'])[8]"));
        this.basketQty = protractor_1.element(protractor_1.by.xpath("//a[@class='showHeaderPopup js-showMinicart']"));
        this.userName = protractor_1.element(protractor_1.by.xpath("//span[@class='js-name']"));
        this.checkoutPopupButton = protractor_1.element(protractor_1.by.xpath("//a[@class='btn btn-sm btn-block js-cartBtn']"));
        this.logoutButton = protractor_1.element(protractor_1.by.xpath("//a[@class='js-logout']"));
        this.viewBagButton = protractor_1.element(protractor_1.by.xpath("btn btn-sm btn-block white js-cartBtn"));
        this.address = protractor_1.element(protractor_1.by.xpath("//a[contains(text(),'Addresses')]"));
        this.credits = protractor_1.element(protractor_1.by.xpath("//a[contains(text(),'Credits')]"));
        this.wishlist = protractor_1.element(protractor_1.by.xpath("//a[contains(text(),'Wishlist')]"));
        this.whiteGlove = protractor_1.element(protractor_1.by.xpath("//li[@class='js-menuWhiteGlove']//a[contains(text(),'White Glove')]"));
        this.sellback = protractor_1.element(protractor_1.by.xpath("//li[@class='js-menuSellback']//a[contains(text(),'Sell Back')]"));
        this.orders = protractor_1.element(protractor_1.by.xpath("//li[@class='js-menuSellback']//a[contains(text(),'Sell Back')]"));
        this.whatsApp = protractor_1.element(protractor_1.by.xpath("//span[@class='reebonzTel']//a[@class='js-waGNumber']"));
    }
}
exports.womanBagsPage = womanBagsPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid29tYW5CYWdzUGFnZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3BhZ2VPYmplY3RzL3dvbWFuQmFnc1BhZ2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSwyQ0FBd0Q7QUFFeEQsTUFBYSxhQUFhO0lBc0J4QjtRQUNFLElBQUksQ0FBQyxZQUFZO1lBQ2Ysd0RBQXdELENBQUM7UUFDM0QsSUFBSSxDQUFDLE9BQU8sR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsb0NBQW9DLENBQUMsQ0FBQyxDQUFDO1FBRXZFLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxJQUFZLEVBQUUsRUFBRTtZQUMvQixPQUFPLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxrQ0FBa0MsR0FBRyxJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUM1RSxDQUFDLENBQUM7UUFFRixJQUFJLENBQUMsUUFBUSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDLENBQUM7UUFDeEUsSUFBSSxDQUFDLFFBQVEsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsb0NBQW9DLENBQUMsQ0FBQyxDQUFDO1FBRXhFLElBQUksQ0FBQyxTQUFTLEdBQUcsb0JBQU8sQ0FDdEIsZUFBRSxDQUFDLEtBQUssQ0FBQywrQ0FBK0MsQ0FBQyxDQUMxRCxDQUFDO1FBQ0YsSUFBSSxDQUFDLFFBQVEsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxDQUFDO1FBQzlELElBQUksQ0FBQyxtQkFBbUIsR0FBRyxvQkFBTyxDQUNoQyxlQUFFLENBQUMsS0FBSyxDQUFDLCtDQUErQyxDQUFDLENBQzFELENBQUM7UUFDRixJQUFJLENBQUMsWUFBWSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLENBQUM7UUFDakUsSUFBSSxDQUFDLGFBQWEsR0FBRyxvQkFBTyxDQUMxQixlQUFFLENBQUMsS0FBSyxDQUFDLHVDQUF1QyxDQUFDLENBQ2xELENBQUM7UUFDRixJQUFJLENBQUMsT0FBTyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDLENBQUM7UUFDdEUsSUFBSSxDQUFDLE9BQU8sR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsaUNBQWlDLENBQUMsQ0FBQyxDQUFDO1FBQ3BFLElBQUksQ0FBQyxRQUFRLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLGtDQUFrQyxDQUFDLENBQUMsQ0FBQztRQUN0RSxJQUFJLENBQUMsVUFBVSxHQUFHLG9CQUFPLENBQ3ZCLGVBQUUsQ0FBQyxLQUFLLENBQ04scUVBQXFFLENBQ3RFLENBQ0YsQ0FBQztRQUNGLElBQUksQ0FBQyxRQUFRLEdBQUcsb0JBQU8sQ0FDckIsZUFBRSxDQUFDLEtBQUssQ0FDTixpRUFBaUUsQ0FDbEUsQ0FDRixDQUFDO1FBQ0YsSUFBSSxDQUFDLE1BQU0sR0FBRyxvQkFBTyxDQUNuQixlQUFFLENBQUMsS0FBSyxDQUNOLGlFQUFpRSxDQUNsRSxDQUNGLENBQUM7UUFDRixJQUFJLENBQUMsUUFBUSxHQUFHLG9CQUFPLENBQ3JCLGVBQUUsQ0FBQyxLQUFLLENBQUMsdURBQXVELENBQUMsQ0FDbEUsQ0FBQztJQUNKLENBQUM7Q0FDRjtBQW5FRCxzQ0FtRUMifQ==