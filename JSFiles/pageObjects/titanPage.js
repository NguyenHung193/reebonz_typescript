"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
class titanPage {
    constructor() {
        this.titanCreditsUrl =
            "http://credit.uat.titan.reebonz-dev.com/credit_admin";
        this.loginButton = protractor_1.element(protractor_1.by.xpath("//a[contains(text(),'LOGIN')]"));
        this.userName = protractor_1.element(protractor_1.by.xpath("//input[@id='identifierId']"));
        this.password = protractor_1.element(protractor_1.by.xpath("//input[@name='password']"));
        this.emailToAddcredits = protractor_1.element(protractor_1.by.xpath("//input[@id='email']"));
        this.searchButton = protractor_1.element(protractor_1.by.xpath("//input[@name='commit']"));
        this.addButton = protractor_1.element(protractor_1.by.xpath("//a[@class='popup-add-credit']"));
        this.selectCategory = protractor_1.element(protractor_1.by.xpath("//select[@id='category']"));
        this.credits = protractor_1.element(protractor_1.by.xpath("//input[@id='points']"));
        this.submitButton = protractor_1.element(protractor_1.by.xpath("//input[@value='Submit']"));
        this.nextButtonEmail = protractor_1.element(protractor_1.by.xpath("//div[@id='identifierNext']"));
        this.nextButtonPassword = protractor_1.element(protractor_1.by.xpath("//div[@id='passwordNext']"));
        this.assertCredits = protractor_1.element(protractor_1.by.xpath("(//tr//td)[5]"));
        this.addCreditsIframe = protractor_1.element(protractor_1.by.id("fancybox-frame"));
        this.logoutButton = protractor_1.element(protractor_1.by.xpath("//div[@class='wrapper']//a[2]"));
        this.signInLabelLoginTitan = protractor_1.element(protractor_1.by.xpath("//content[contains(text(),'Sign in')]"));
    }
}
exports.titanPage = titanPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGl0YW5QYWdlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vcGFnZU9iamVjdHMvdGl0YW5QYWdlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsMkNBQXdEO0FBRXhELE1BQWEsU0FBUztJQWtCcEI7UUFDRSxJQUFJLENBQUMsZUFBZTtZQUNsQixzREFBc0QsQ0FBQztRQUN6RCxJQUFJLENBQUMsV0FBVyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQywrQkFBK0IsQ0FBQyxDQUFDLENBQUM7UUFDdEUsSUFBSSxDQUFDLFFBQVEsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsNkJBQTZCLENBQUMsQ0FBQyxDQUFDO1FBQ2pFLElBQUksQ0FBQyxRQUFRLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDJCQUEyQixDQUFDLENBQUMsQ0FBQztRQUMvRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsWUFBWSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLENBQUM7UUFDakUsSUFBSSxDQUFDLFNBQVMsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsZ0NBQWdDLENBQUMsQ0FBQyxDQUFDO1FBQ3JFLElBQUksQ0FBQyxjQUFjLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDBCQUEwQixDQUFDLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsT0FBTyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUM7UUFDMUQsSUFBSSxDQUFDLFlBQVksR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxDQUFDO1FBQ2xFLElBQUksQ0FBQyxlQUFlLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDZCQUE2QixDQUFDLENBQUMsQ0FBQztRQUN4RSxJQUFJLENBQUMsa0JBQWtCLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDJCQUEyQixDQUFDLENBQUMsQ0FBQztRQUN6RSxJQUFJLENBQUMsYUFBYSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO1FBQ3hELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxFQUFFLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1FBQ3pELElBQUksQ0FBQyxZQUFZLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLCtCQUErQixDQUFDLENBQUMsQ0FBQztRQUN2RSxJQUFJLENBQUMscUJBQXFCLEdBQUcsb0JBQU8sQ0FDbEMsZUFBRSxDQUFDLEtBQUssQ0FBQyx1Q0FBdUMsQ0FBQyxDQUNsRCxDQUFDO0lBQ0osQ0FBQztDQUNGO0FBdkNELDhCQXVDQyJ9