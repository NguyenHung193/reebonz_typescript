"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
const chai_1 = __importDefault(require("chai"));
var expect = chai_1.default.expect;
class basePage {
    constructor() {
        this.overlay = protractor_1.element(protractor_1.by.xpath("//div[@id='overlay']"));
        this.waitForElementAndSendkeys = (elementWait, text) => __awaiter(this, void 0, void 0, function* () {
            let EC = protractor_1.protractor.ExpectedConditions;
            yield protractor_1.browser.wait(EC.invisibilityOf(this.overlay), 100000);
            yield this.waitForElement(elementWait);
            yield elementWait.click();
            yield protractor_1.browser.sleep(1000);
            yield elementWait.sendKeys(text);
        });
        this.waitForElementAndClick = (elementWait) => __awaiter(this, void 0, void 0, function* () {
            let EC = protractor_1.protractor.ExpectedConditions;
            yield protractor_1.browser.wait(EC.invisibilityOf(this.overlay), 100000);
            yield protractor_1.browser.wait(EC.presenceOf(elementWait), 100000);
            yield protractor_1.browser.wait(EC.visibilityOf(elementWait), 100000);
            yield protractor_1.browser.wait(EC.elementToBeClickable(elementWait), 100000);
            yield protractor_1.browser.sleep(1000);
            yield elementWait.click();
        });
        this.waitForElementAndGetText = (elementWait, result) => __awaiter(this, void 0, void 0, function* () {
            let EC = protractor_1.protractor.ExpectedConditions;
            yield protractor_1.browser.wait(EC.invisibilityOf(this.overlay), 100000);
            yield protractor_1.browser.wait(EC.visibilityOf(elementWait), 100000);
            let isVisiable = yield elementWait.getText();
            console.log("name" + isVisiable);
            yield expect(isVisiable).to.equal(result);
        });
        this.waitForElementToCheckContainText = (elementWait, result) => __awaiter(this, void 0, void 0, function* () {
            let EC = protractor_1.protractor.ExpectedConditions;
            // await browser.wait(EC.invisibilityOf(this.overlay), 100000);
            yield protractor_1.browser.wait(EC.visibilityOf(elementWait), 100000);
            yield elementWait.getText().then(function (text) {
                expect(text).contain(result);
            });
        });
        //function for only check Reebonz email user
        this.getElementValue = (elementWait, text) => __awaiter(this, void 0, void 0, function* () {
            let EC = protractor_1.protractor.ExpectedConditions;
            yield protractor_1.browser.wait(EC.invisibilityOf(this.overlay), 100000);
            yield protractor_1.browser.wait(EC.visibilityOf(elementWait), 100000);
            let result = yield elementWait.getAttribute("value");
            yield expect(result).to.equal(text);
            return result;
        });
        this.switchIframe = (elementWait) => __awaiter(this, void 0, void 0, function* () {
            let EC = protractor_1.protractor.ExpectedConditions;
            yield protractor_1.browser.wait(EC.invisibilityOf(this.overlay), 100000);
            yield protractor_1.browser.wait(EC.visibilityOf(elementWait), 100000);
            yield protractor_1.browser.switchTo().frame(elementWait.getWebElement());
        });
        this.switchParrentWindow = () => __awaiter(this, void 0, void 0, function* () {
            let EC = protractor_1.protractor.ExpectedConditions;
            yield protractor_1.browser.wait(EC.invisibilityOf(this.overlay), 100000);
            yield protractor_1.browser.wait(EC.visibilityOf(protractor_1.element(protractor_1.by.xpath("//body"))), 100000);
            yield protractor_1.browser.switchTo().defaultContent();
        });
        this.popUpCookies = protractor_1.element(protractor_1.by.xpath("//button[@class='btn white min200']"));
        this.closePopUpCookiesFunction = () => __awaiter(this, void 0, void 0, function* () {
            let visiable = this.popUpCookies.isPresent();
            if (visiable) {
                yield this.closePopUpCookies();
            }
        });
        this.closePopUpCookies = () => __awaiter(this, void 0, void 0, function* () {
            let EC = protractor_1.protractor.ExpectedConditions;
            // await browser.wait(EC.invisibilityOf(this.overlay), 100000);
            let popup = yield protractor_1.browser.wait(EC.visibilityOf(this.popUpCookies), 50000);
            if (popup) {
                this.popUpCookies.click();
            }
        });
        this.waitForUrlAndAssert = (string) => __awaiter(this, void 0, void 0, function* () {
            let EC = protractor_1.protractor.ExpectedConditions;
            yield protractor_1.browser.wait(EC.urlContains(string), 100000);
        });
        this.clearCache = () => __awaiter(this, void 0, void 0, function* () {
            yield protractor_1.browser.get("https://uat-www.reebonz-dev.com/sg");
            yield protractor_1.browser.executeScript("window.localStorage.clear();");
            yield protractor_1.browser.executeScript("window.sessionStorage.clear();");
            yield protractor_1.browser.driver.manage().deleteAllCookies();
        });
        this.clearAPIUatUrl =
            "https://uat-consumer-api.reebonz-dev.com/api/cache/flush";
        this.clearAPIUat = protractor_1.element(protractor_1.by.xpath("//pre[contains(text(),'true')]"));
        this.checkElementVisible = (elementWait) => __awaiter(this, void 0, void 0, function* () {
            let result = yield elementWait.isPresent();
            yield expect(result).to.equal(true);
            return result;
        });
        this.checkElementInVisible = (elementWait) => __awaiter(this, void 0, void 0, function* () {
            let result = yield elementWait.isPresent();
            yield console.log("element" + elementWait + "visiable=" + result);
            yield expect(result).to.equal(false);
            return result;
        });
        this.waitForElement = (elementWait) => __awaiter(this, void 0, void 0, function* () {
            let EC = protractor_1.protractor.ExpectedConditions;
            yield protractor_1.browser.wait(EC.visibilityOf(elementWait), 60000);
            yield protractor_1.browser.sleep(1000);
            let result = yield elementWait.isPresent();
            yield expect(result).to.equal(true);
            return result;
        });
        this.convertToNumber = function (value) {
            value = value.match(/\d\.*/g);
            value = value.join("");
            value = Number(value);
            return value;
        };
        this.checkElementIsPresentAndConvertToNumber = (elementWait) => __awaiter(this, void 0, void 0, function* () {
            let isVisible = yield elementWait.isPresent();
            if (isVisible) {
                let result = yield elementWait.getText();
                let value = 0;
                value = yield this.convertToNumber(result);
                return value;
            }
            else {
                let value = 0;
                return value;
            }
        });
        this.checkElementHavingHref = (elementWait) => __awaiter(this, void 0, void 0, function* () {
            let EC = protractor_1.protractor.ExpectedConditions;
            let result = yield elementWait.getAttribute("href");
            yield console.log("+++++++++++++++" + result);
            yield expect(result).to.not.equal(null);
        });
        this.checkElementDisplay = (elementWait) => __awaiter(this, void 0, void 0, function* () {
            let result = yield elementWait.isDisplayed();
            yield expect(result).to.equal(true);
            return result;
        });
    }
}
exports.basePage = basePage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZVBhZ2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9wYWdlT2JqZWN0cy9iYXNlUGFnZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBQUEsMkNBQTZFO0FBQzdFLGdEQUF3QjtBQUV4QixJQUFJLE1BQU0sR0FBRyxjQUFJLENBQUMsTUFBTSxDQUFDO0FBRXpCLE1BQWEsUUFBUTtJQXdCbkI7UUFDRSxJQUFJLENBQUMsT0FBTyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUM7UUFFekQsSUFBSSxDQUFDLHlCQUF5QixHQUFHLENBQy9CLFdBQTBCLEVBQzFCLElBQVksRUFDWixFQUFFO1lBQ0YsSUFBSSxFQUFFLEdBQUcsdUJBQVUsQ0FBQyxrQkFBa0IsQ0FBQztZQUN2QyxNQUFNLG9CQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQzVELE1BQU0sSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN2QyxNQUFNLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUMxQixNQUFNLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzFCLE1BQU0sV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNuQyxDQUFDLENBQUEsQ0FBQztRQUNGLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxDQUFPLFdBQTBCLEVBQUUsRUFBRTtZQUNqRSxJQUFJLEVBQUUsR0FBRyx1QkFBVSxDQUFDLGtCQUFrQixDQUFDO1lBQ3ZDLE1BQU0sb0JBQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDNUQsTUFBTSxvQkFBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ3ZELE1BQU0sb0JBQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQztZQUN6RCxNQUFNLG9CQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQyxXQUFXLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQztZQUNqRSxNQUFNLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzFCLE1BQU0sV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzVCLENBQUMsQ0FBQSxDQUFDO1FBQ0YsSUFBSSxDQUFDLHdCQUF3QixHQUFHLENBQzlCLFdBQTBCLEVBQzFCLE1BQWMsRUFDZCxFQUFFO1lBQ0YsSUFBSSxFQUFFLEdBQUcsdUJBQVUsQ0FBQyxrQkFBa0IsQ0FBQztZQUN2QyxNQUFNLG9CQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQzVELE1BQU0sb0JBQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQztZQUN6RCxJQUFJLFVBQVUsR0FBRyxNQUFNLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUM3QyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBQyxVQUFVLENBQUMsQ0FBQztZQUMvQixNQUFNLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRTVDLENBQUMsQ0FBQSxDQUFDO1FBQ0YsSUFBSSxDQUFDLGdDQUFnQyxHQUFHLENBQ3RDLFdBQTBCLEVBQzFCLE1BQWMsRUFDZCxFQUFFO1lBQ0YsSUFBSSxFQUFFLEdBQUcsdUJBQVUsQ0FBQyxrQkFBa0IsQ0FBQztZQUN2QywrREFBK0Q7WUFDL0QsTUFBTSxvQkFBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ3pELE1BQU0sV0FBVyxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFTLElBQUk7Z0JBQzVDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDL0IsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUEsQ0FBQztRQUNGLDRDQUE0QztRQUM1QyxJQUFJLENBQUMsZUFBZSxHQUFHLENBQU8sV0FBMEIsRUFBRSxJQUFZLEVBQUUsRUFBRTtZQUN4RSxJQUFJLEVBQUUsR0FBRyx1QkFBVSxDQUFDLGtCQUFrQixDQUFDO1lBQ3ZDLE1BQU0sb0JBQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDNUQsTUFBTSxvQkFBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ3pELElBQUksTUFBTSxHQUFHLE1BQU0sV0FBVyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNyRCxNQUFNLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BDLE9BQU8sTUFBTSxDQUFDO1FBQ2hCLENBQUMsQ0FBQSxDQUFDO1FBQ0YsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFPLFdBQTBCLEVBQUUsRUFBRTtZQUN2RCxJQUFJLEVBQUUsR0FBRyx1QkFBVSxDQUFDLGtCQUFrQixDQUFDO1lBQ3ZDLE1BQU0sb0JBQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDNUQsTUFBTSxvQkFBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ3pELE1BQU0sb0JBQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxDQUFDLENBQUM7UUFDOUQsQ0FBQyxDQUFBLENBQUM7UUFDRixJQUFJLENBQUMsbUJBQW1CLEdBQUcsR0FBUyxFQUFFO1lBQ3BDLElBQUksRUFBRSxHQUFHLHVCQUFVLENBQUMsa0JBQWtCLENBQUM7WUFDdkMsTUFBTSxvQkFBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQztZQUM1RCxNQUFNLG9CQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQztZQUN6RSxNQUFNLG9CQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDNUMsQ0FBQyxDQUFBLENBQUM7UUFDRixJQUFJLENBQUMsWUFBWSxHQUFHLG9CQUFPLENBQ3pCLGVBQUUsQ0FBQyxLQUFLLENBQUMscUNBQXFDLENBQUMsQ0FDaEQsQ0FBQztRQUVGLElBQUksQ0FBQyx5QkFBeUIsR0FBRyxHQUFTLEVBQUU7WUFDMUMsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUM3QyxJQUFJLFFBQVEsRUFBRTtnQkFDWixNQUFNLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO2FBQ2hDO1FBQ0gsQ0FBQyxDQUFBLENBQUM7UUFFRixJQUFJLENBQUMsaUJBQWlCLEdBQUcsR0FBUyxFQUFFO1lBQ2xDLElBQUksRUFBRSxHQUFHLHVCQUFVLENBQUMsa0JBQWtCLENBQUM7WUFDdkMsK0RBQStEO1lBQy9ELElBQUksS0FBSyxHQUFHLE1BQU0sb0JBQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDMUUsSUFBSSxLQUFLLEVBQUU7Z0JBQ1QsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUMzQjtRQUNILENBQUMsQ0FBQSxDQUFDO1FBQ0YsSUFBSSxDQUFDLG1CQUFtQixHQUFHLENBQU8sTUFBYyxFQUFFLEVBQUU7WUFDbEQsSUFBSSxFQUFFLEdBQUcsdUJBQVUsQ0FBQyxrQkFBa0IsQ0FBQztZQUN2QyxNQUFNLG9CQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDckQsQ0FBQyxDQUFBLENBQUM7UUFDRixJQUFJLENBQUMsVUFBVSxHQUFHLEdBQVMsRUFBRTtZQUMzQixNQUFNLG9CQUFPLENBQUMsR0FBRyxDQUFDLG9DQUFvQyxDQUFDLENBQUM7WUFDeEQsTUFBTSxvQkFBTyxDQUFDLGFBQWEsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO1lBQzVELE1BQU0sb0JBQU8sQ0FBQyxhQUFhLENBQUMsZ0NBQWdDLENBQUMsQ0FBQztZQUM5RCxNQUFNLG9CQUFPLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDbkQsQ0FBQyxDQUFBLENBQUM7UUFFRixJQUFJLENBQUMsY0FBYztZQUNqQiwwREFBMEQsQ0FBQztRQUM3RCxJQUFJLENBQUMsV0FBVyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDLENBQUM7UUFFdkUsSUFBSSxDQUFDLG1CQUFtQixHQUFHLENBQU8sV0FBMEIsRUFBRSxFQUFFO1lBQzlELElBQUksTUFBTSxHQUFHLE1BQU0sV0FBVyxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQzNDLE1BQU0sTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDcEMsT0FBTyxNQUFNLENBQUM7UUFDaEIsQ0FBQyxDQUFBLENBQUM7UUFFRixJQUFJLENBQUMscUJBQXFCLEdBQUcsQ0FBTyxXQUEwQixFQUFFLEVBQUU7WUFDaEUsSUFBSSxNQUFNLEdBQUcsTUFBTSxXQUFXLENBQUMsU0FBUyxFQUFFLENBQUM7WUFDM0MsTUFBTSxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxXQUFXLEdBQUcsV0FBVyxHQUFHLE1BQU0sQ0FBQyxDQUFDO1lBRWxFLE1BQU0sTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDckMsT0FBTyxNQUFNLENBQUM7UUFDaEIsQ0FBQyxDQUFBLENBQUM7UUFFRixJQUFJLENBQUMsY0FBYyxHQUFHLENBQU8sV0FBMEIsRUFBRSxFQUFFO1lBQ3pELElBQUksRUFBRSxHQUFHLHVCQUFVLENBQUMsa0JBQWtCLENBQUM7WUFDdkMsTUFBTSxvQkFBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ3hELE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDMUIsSUFBSSxNQUFNLEdBQUcsTUFBTSxXQUFXLENBQUMsU0FBUyxFQUFFLENBQUM7WUFDM0MsTUFBTSxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNwQyxPQUFPLE1BQU0sQ0FBQztRQUNoQixDQUFDLENBQUEsQ0FBQztRQUNGLElBQUksQ0FBQyxlQUFlLEdBQUcsVUFBUyxLQUFVO1lBQ3hDLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzlCLEtBQUssR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3ZCLEtBQUssR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFdEIsT0FBTyxLQUFLLENBQUM7UUFDZixDQUFDLENBQUM7UUFDRixJQUFJLENBQUMsdUNBQXVDLEdBQUcsQ0FDN0MsV0FBMEIsRUFDMUIsRUFBRTtZQUNGLElBQUksU0FBUyxHQUFHLE1BQU0sV0FBVyxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBRTlDLElBQUksU0FBUyxFQUFFO2dCQUNiLElBQUksTUFBTSxHQUFHLE1BQU0sV0FBVyxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUN6QyxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUM7Z0JBQ2QsS0FBSyxHQUFHLE1BQU0sSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDM0MsT0FBTyxLQUFLLENBQUM7YUFDZDtpQkFBTTtnQkFDTCxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUM7Z0JBQ2QsT0FBTyxLQUFLLENBQUM7YUFDZDtRQUNILENBQUMsQ0FBQSxDQUFDO1FBQ0YsSUFBSSxDQUFDLHNCQUFzQixHQUFHLENBQU8sV0FBMEIsRUFBRSxFQUFFO1lBQ2pFLElBQUksRUFBRSxHQUFHLHVCQUFVLENBQUMsa0JBQWtCLENBQUM7WUFDdkMsSUFBSSxNQUFNLEdBQUcsTUFBTSxXQUFXLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3BELE1BQU0sT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsR0FBRyxNQUFNLENBQUMsQ0FBQztZQUU5QyxNQUFNLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQyxDQUFDLENBQUEsQ0FBQztRQUVGLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxDQUFPLFdBQTBCLEVBQUUsRUFBRTtZQUM5RCxJQUFJLE1BQU0sR0FBRyxNQUFNLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM3QyxNQUFNLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BDLE9BQU8sTUFBTSxDQUFDO1FBQ2hCLENBQUMsQ0FBQSxDQUFDO0lBQ0osQ0FBQztDQUNGO0FBdkxELDRCQXVMQyJ9