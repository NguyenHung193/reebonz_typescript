"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
class register {
    constructor() {
        this.UATURL = "https://uat-www.reebonz-dev.com/";
        this.registerButton = protractor_1.element(protractor_1.by.xpath("//a[@class='js-register']"));
        this.firstName = protractor_1.element(protractor_1.by.xpath("//input[@name='first_name']"));
        this.lastName = protractor_1.element(protractor_1.by.xpath("//input[@name='last_name']"));
        this.emailAddress = protractor_1.element(protractor_1.by.xpath("//input[@name='email']"));
        this.password = protractor_1.element(protractor_1.by.xpath("//input[@id='password']"));
        this.country = protractor_1.element(protractor_1.by.xpath("//select[@id='country']"));
        this.submit = protractor_1.element(protractor_1.by.xpath("//input[@value='Register with email']"));
        this.registerTitle = protractor_1.element(protractor_1.by.xpath("//h2[contains(text(),'Register')]"));
        this.loginButton = protractor_1.element(protractor_1.by.xpath("//a[@class='js-login']"));
        this.loginTitle = protractor_1.element(protractor_1.by.xpath("//h2[contains(text(),'Sign In')]"));
        this.loginEmail = protractor_1.element(protractor_1.by.xpath("//input[@id='username']"));
        this.loginPassword = protractor_1.element(protractor_1.by.xpath("//input[@id='password']"));
        this.loginSubmit = protractor_1.element(protractor_1.by.xpath("//input[@class='btn btn-sm js-submit']"));
        this.userNameError = protractor_1.element(protractor_1.by.xpath("//label[@id='username-error']"));
        this.passwordError = protractor_1.element(protractor_1.by.xpath("//label[@id='password-error']"));
    }
}
exports.register = register;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVnaXN0ZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9wYWdlT2JqZWN0cy9yZWdpc3Rlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLDJDQUF3RDtBQUV4RCxNQUFhLFFBQVE7SUFtQm5CO1FBQ0UsSUFBSSxDQUFDLE1BQU0sR0FBRyxrQ0FBa0MsQ0FBQztRQUNqRCxJQUFJLENBQUMsY0FBYyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQywyQkFBMkIsQ0FBQyxDQUFDLENBQUM7UUFDckUsSUFBSSxDQUFDLFNBQVMsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsNkJBQTZCLENBQUMsQ0FBQyxDQUFDO1FBQ2xFLElBQUksQ0FBQyxRQUFRLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDRCQUE0QixDQUFDLENBQUMsQ0FBQztRQUNoRSxJQUFJLENBQUMsWUFBWSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLENBQUM7UUFDaEUsSUFBSSxDQUFDLFFBQVEsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDO1FBQzdELElBQUksQ0FBQyxPQUFPLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQztRQUM1RCxJQUFJLENBQUMsTUFBTSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyx1Q0FBdUMsQ0FBQyxDQUFDLENBQUM7UUFDekUsSUFBSSxDQUFDLGFBQWEsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsbUNBQW1DLENBQUMsQ0FBQyxDQUFDO1FBQzVFLElBQUksQ0FBQyxXQUFXLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHdCQUF3QixDQUFDLENBQUMsQ0FBQztRQUMvRCxJQUFJLENBQUMsVUFBVSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDLENBQUM7UUFDeEUsSUFBSSxDQUFDLFVBQVUsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDO1FBQy9ELElBQUksQ0FBQyxhQUFhLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQztRQUNsRSxJQUFJLENBQUMsV0FBVyxHQUFHLG9CQUFPLENBQ3hCLGVBQUUsQ0FBQyxLQUFLLENBQUMsd0NBQXdDLENBQUMsQ0FDbkQsQ0FBQztRQUNGLElBQUksQ0FBQyxhQUFhLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLCtCQUErQixDQUFDLENBQUMsQ0FBQztRQUN4RSxJQUFJLENBQUMsYUFBYSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQywrQkFBK0IsQ0FBQyxDQUFDLENBQUM7SUFDMUUsQ0FBQztDQUNGO0FBdkNELDRCQXVDQyJ9