import {Config} from "protractor";
import * as reporter from "cucumber-html-reporter";




export let config: Config = {
    // The address of a running selenium server.
   // seleniumAddress: 'http://localhost:4444/wd/hub',
   directConnect:true,
   framework:'custom',
   frameworkPath: require.resolve('protractor-cucumber-framework'),
   
  
    // Capabilities to be passed to the webdriver instance.
    capabilities: {
      browserName: 'chrome',
      chromeOptions: {
        args: [
            '--disable-infobars',
            '--disable-extensions',
            'verbose',
            'log-path=./reports/chromedriver.log',
            "--headless",
            "--no-sandbox",
            "--disable-dev-shm-usage",
            "--disable-extensions",
            "--disable-gpu",
            "--window-size=1920,1080"
        ],
        prefs: {
            'profile.password_manager_enabled': false,
            'credentials_enable_service': false,
            'password_manager_enabled': false
        }
    },
    },
  
    // Spec patterns are relative to the configuration file location passed
    // to protractor (in this example conf.js).
    // They may include glob patterns.
    specs: ['../../features/*.feature'],
    SELENIUM_PROMISE_MANAGER: false,
    cucumberOpts: {
        // require step definitions
        tags:"@loginFunction" ,  
        format:'json:./report/login.json',
       
        
        require: [
          '../stepDefinations/*.js', // accepts a glob,
        
        ]
      },
      onComplete: () =>{
        var options = {
          theme: 'bootstrap',
          jsonFile: './report/login.json',
          output: './report/login.html',
          reportSuiteAsScenarios: true,
          launchReport: true,
          metadata: {
              "App Version":"0.3.2",
              "Test Environment": "UAT",
              "Browser": "Chrome  73.0.3683.103",
              "Platform": "Windows 10",
              "Parallel": "Scenarios",
              "Executed": "Remote"
          }
      };
   
      reporter.generate(options);


      }

  
  
  };
  