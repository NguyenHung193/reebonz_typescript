import { ElementFinder, element, by } from "protractor";

export class processToCheckout {
  checkoutButton: ElementFinder;

  constructor() {
    this.checkoutButton = element(by.xpath("//a[@class='btn js-login']"));
  }
}
