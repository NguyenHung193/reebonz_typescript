import { ElementFinder, element, by } from "protractor";

export class thankyouPage {
  countinueButton: ElementFinder;

  constructor() {
    this.countinueButton = element(by.xpath("//a[@class='btn']"));
  }
}
