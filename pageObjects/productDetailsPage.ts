import { ElementFinder, element, by } from "protractor";

export class productDetailsPage {
  addToBasketButton: ElementFinder;
  basketQty: ElementFinder;
  productName: ElementFinder;

  constructor() {
    this.basketQty = element(
      by.xpath("//div[@class='navWrapper']/a[@href='/sg/cart']")
    );
    this.addToBasketButton = element(by.xpath("//button[@name='AddToBasket']"));
    this.productName = element(by.xpath("//div[@class='productInfo']//h2"));
  }
}
