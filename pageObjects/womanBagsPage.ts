import { ElementFinder, element, by } from "protractor";

export class womanBagsPage {
  womanBagsUrl: string;
  basketQty: ElementFinder;
  product: ElementFinder;
  product2: ElementFinder;
  product3: ElementFinder;

  checkoutPopupButton: ElementFinder;
  //elements popUp
  logoutButton: ElementFinder;
  userName: ElementFinder;
  address: ElementFinder;
  credits: ElementFinder;
  wishlist: ElementFinder;
  whiteGlove: ElementFinder;
  sellback: ElementFinder;
  orders: ElementFinder;
  whatsApp: ElementFinder;

  viewBagButton: ElementFinder;
  productN: Function;

  constructor() {
    this.womanBagsUrl =
      "https://uat-www.reebonz-dev.com/sg/women/designer-bags";
    this.product = element(by.xpath("(//img[@class='js-imgPreload'])[1]"));

    this.productN = (numb: string) => {
      return element(by.xpath("(//img[@class='js-imgPreload'])[" + numb + "]"));
    };

    this.product2 = element(by.xpath("(//img[@class='js-imgPreload'])[4]"));
    this.product3 = element(by.xpath("(//img[@class='js-imgPreload'])[8]"));

    this.basketQty = element(
      by.xpath("//a[@class='showHeaderPopup js-showMinicart']")
    );
    this.userName = element(by.xpath("//span[@class='js-name']"));
    this.checkoutPopupButton = element(
      by.xpath("//a[@class='btn btn-sm btn-block js-cartBtn']")
    );
    this.logoutButton = element(by.xpath("//a[@class='js-logout']"));
    this.viewBagButton = element(
      by.xpath("btn btn-sm btn-block white js-cartBtn")
    );
    this.address = element(by.xpath("//a[contains(text(),'Addresses')]"));
    this.credits = element(by.xpath("//a[contains(text(),'Credits')]"));
    this.wishlist = element(by.xpath("//a[contains(text(),'Wishlist')]"));
    this.whiteGlove = element(
      by.xpath(
        "//li[@class='js-menuWhiteGlove']//a[contains(text(),'White Glove')]"
      )
    );
    this.sellback = element(
      by.xpath(
        "//li[@class='js-menuSellback']//a[contains(text(),'Sell Back')]"
      )
    );
    this.orders = element(
      by.xpath(
        "//li[@class='js-menuSellback']//a[contains(text(),'Sell Back')]"
      )
    );
    this.whatsApp = element(
      by.xpath("//span[@class='reebonzTel']//a[@class='js-waGNumber']")
    );
  }
}
