import { ElementFinder, element, by } from "protractor";

export class titanPage {
  titanCreditsUrl: string;
  loginButton: ElementFinder;
  userName: ElementFinder;
  password: ElementFinder;
  emailToAddcredits: ElementFinder;
  searchButton: ElementFinder;
  addButton: ElementFinder;
  selectCategory: ElementFinder;
  credits: ElementFinder;
  submitButton: ElementFinder;
  nextButtonEmail: ElementFinder;
  nextButtonPassword: ElementFinder;
  assertCredits: ElementFinder;
  addCreditsIframe: ElementFinder;
  logoutButton: ElementFinder;
  signInLabelLoginTitan: ElementFinder;

  constructor() {
    this.titanCreditsUrl =
      "http://credit.uat.titan.reebonz-dev.com/credit_admin";
    this.loginButton = element(by.xpath("//a[contains(text(),'LOGIN')]"));
    this.userName = element(by.xpath("//input[@id='identifierId']"));
    this.password = element(by.xpath("//input[@name='password']"));
    this.emailToAddcredits = element(by.xpath("//input[@id='email']"));
    this.searchButton = element(by.xpath("//input[@name='commit']"));
    this.addButton = element(by.xpath("//a[@class='popup-add-credit']"));
    this.selectCategory = element(by.xpath("//select[@id='category']"));
    this.credits = element(by.xpath("//input[@id='points']"));
    this.submitButton = element(by.xpath("//input[@value='Submit']"));
    this.nextButtonEmail = element(by.xpath("//div[@id='identifierNext']"));
    this.nextButtonPassword = element(by.xpath("//div[@id='passwordNext']"));
    this.assertCredits = element(by.xpath("(//tr//td)[5]"));
    this.addCreditsIframe = element(by.id("fancybox-frame"));
    this.logoutButton = element(by.xpath("//div[@class='wrapper']//a[2]"));
    this.signInLabelLoginTitan = element(
      by.xpath("//content[contains(text(),'Sign in')]")
    );
  }
}
