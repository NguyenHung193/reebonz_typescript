import { ElementFinder, browser, protractor, by, element } from "protractor";
import chai from "chai";
import { async } from "q";
var expect = chai.expect;

export class basePage {
  waitForElementAndSendkeys: Function;
  waitForElementAndClick: Function;
  waitForElementAndGetText: Function;
  waitForElementToCheckContainText: Function;
  getElementValue: Function;
  overlay: ElementFinder;
  switchIframe: Function;
  switchParrentWindow: Function;
  popUpCookies: ElementFinder;
  closePopUpCookies: Function;
  waitForUrlAndAssert: Function;
  clearCache: Function;
  clearAPIUatUrl: string;
  clearAPIUat: ElementFinder;
  checkElementVisible: Function;
  waitForElement: Function;
  convertToNumber: Function;
  checkElementIsPresentAndConvertToNumber: Function;
  checkElementInVisible: Function;
  checkElementHavingHref: Function;
  checkElementDisplay: Function;
  closePopUpCookiesFunction: Function;

  constructor() {
    this.overlay = element(by.xpath("//div[@id='overlay']"));

    this.waitForElementAndSendkeys = async (
      elementWait: ElementFinder,
      text: string
    ) => {
      let EC = protractor.ExpectedConditions;
      await browser.wait(EC.invisibilityOf(this.overlay), 100000);
      await this.waitForElement(elementWait);
      await elementWait.click();
      await browser.sleep(1000);
      await elementWait.sendKeys(text);
    };
    this.waitForElementAndClick = async (elementWait: ElementFinder) => {
      let EC = protractor.ExpectedConditions;
      await browser.wait(EC.invisibilityOf(this.overlay), 100000);
      await browser.wait(EC.presenceOf(elementWait), 100000);
      await browser.wait(EC.visibilityOf(elementWait), 100000);
      await browser.wait(EC.elementToBeClickable(elementWait), 100000);
      await browser.sleep(1000);
      await elementWait.click();
    };
    this.waitForElementAndGetText = async (
      elementWait: ElementFinder,
      result: string
    ) => {
      let EC = protractor.ExpectedConditions;
      await browser.wait(EC.invisibilityOf(this.overlay), 100000);
      await browser.wait(EC.visibilityOf(elementWait), 100000);
      let isVisiable = await elementWait.getText();
      console.log("name"+isVisiable);
      await expect(isVisiable).to.equal(result);
      
    };
    this.waitForElementToCheckContainText = async (
      elementWait: ElementFinder,
      result: string
    ) => {
      let EC = protractor.ExpectedConditions;
      // await browser.wait(EC.invisibilityOf(this.overlay), 100000);
      await browser.wait(EC.visibilityOf(elementWait), 100000);
      await elementWait.getText().then(function(text) {
        expect(text).contain(result);
      });
    };
    //function for only check Reebonz email user
    this.getElementValue = async (elementWait: ElementFinder, text: string) => {
      let EC = protractor.ExpectedConditions;
      await browser.wait(EC.invisibilityOf(this.overlay), 100000);
      await browser.wait(EC.visibilityOf(elementWait), 100000);
      let result = await elementWait.getAttribute("value");
      await expect(result).to.equal(text);
      return result;
    };
    this.switchIframe = async (elementWait: ElementFinder) => {
      let EC = protractor.ExpectedConditions;
      await browser.wait(EC.invisibilityOf(this.overlay), 100000);
      await browser.wait(EC.visibilityOf(elementWait), 100000);
      await browser.switchTo().frame(elementWait.getWebElement());
    };
    this.switchParrentWindow = async () => {
      let EC = protractor.ExpectedConditions;
      await browser.wait(EC.invisibilityOf(this.overlay), 100000);
      await browser.wait(EC.visibilityOf(element(by.xpath("//body"))), 100000);
      await browser.switchTo().defaultContent();
    };
    this.popUpCookies = element(
      by.xpath("//button[@class='btn white min200']")
    );

    this.closePopUpCookiesFunction = async () => {
      let visiable = this.popUpCookies.isPresent();
      if (visiable) {
        await this.closePopUpCookies();
      }
    };

    this.closePopUpCookies = async () => {
      let EC = protractor.ExpectedConditions;
      // await browser.wait(EC.invisibilityOf(this.overlay), 100000);
      let popup = await browser.wait(EC.visibilityOf(this.popUpCookies), 50000);
      if (popup) {
        this.popUpCookies.click();
      }
    };
    this.waitForUrlAndAssert = async (string: string) => {
      let EC = protractor.ExpectedConditions;
      await browser.wait(EC.urlContains(string), 100000);
    };
    this.clearCache = async () => {
      await browser.get("https://uat-www.reebonz-dev.com/sg");
      await browser.executeScript("window.localStorage.clear();");
      await browser.executeScript("window.sessionStorage.clear();");
      await browser.driver.manage().deleteAllCookies();
    };

    this.clearAPIUatUrl =
      "https://uat-consumer-api.reebonz-dev.com/api/cache/flush";
    this.clearAPIUat = element(by.xpath("//pre[contains(text(),'true')]"));

    this.checkElementVisible = async (elementWait: ElementFinder) => {
      let result = await elementWait.isPresent();
      await expect(result).to.equal(true);
      return result;
    };

    this.checkElementInVisible = async (elementWait: ElementFinder) => {
      let result = await elementWait.isPresent();
      await console.log("element" + elementWait + "visiable=" + result);

      await expect(result).to.equal(false);
      return result;
    };

    this.waitForElement = async (elementWait: ElementFinder) => {
      let EC = protractor.ExpectedConditions;
      await browser.wait(EC.visibilityOf(elementWait), 60000);
      await browser.sleep(1000);
      let result = await elementWait.isPresent();
      await expect(result).to.equal(true);
      return result;
    };
    this.convertToNumber = function(value: any) {
      value = value.match(/\d\.*/g);
      value = value.join("");
      value = Number(value);

      return value;
    };
    this.checkElementIsPresentAndConvertToNumber = async (
      elementWait: ElementFinder
    ) => {
      let isVisible = await elementWait.isPresent();

      if (isVisible) {
        let result = await elementWait.getText();
        let value = 0;
        value = await this.convertToNumber(result);
        return value;
      } else {
        let value = 0;
        return value;
      }
    };
    this.checkElementHavingHref = async (elementWait: ElementFinder) => {
      let EC = protractor.ExpectedConditions;
      let result = await elementWait.getAttribute("href");
      await console.log("+++++++++++++++" + result);

      await expect(result).to.not.equal(null);
    };

    this.checkElementDisplay = async (elementWait: ElementFinder) => {
      let result = await elementWait.isDisplayed();
      await expect(result).to.equal(true);
      return result;
    };
  }
}
