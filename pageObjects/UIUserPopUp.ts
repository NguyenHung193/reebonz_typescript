import { ElementFinder, element, by } from "protractor";

export class UIUserPopUp {
  //elements popUp
  logoutButton: ElementFinder;
  userName: ElementFinder;
  address: ElementFinder;
  credits: ElementFinder;
  wishlist: ElementFinder;
  whiteGlove: ElementFinder;
  sellback: ElementFinder;
  orders: ElementFinder;
  inbox: ElementFinder;
  boutique: ElementFinder;

  constructor() {
    this.logoutButton = element(by.xpath("//a[@class='js-logout']"));
    this.userName = element(by.xpath("//span[@class='js-name']"));
    this.address = element(by.xpath("//a[contains(text(),'Addresses')]"));
    this.credits = element(by.xpath("//a[contains(text(),'Credits')]"));
    this.wishlist = element(by.xpath("//a[contains(text(),'Wishlist')]"));
    this.whiteGlove = element(by.xpath("//li[@class='js-menuWhiteGlove']//a[contains(text(),'White Glove')]"));
    this.sellback = element(by.xpath("//div[@class='userMenuWrapper']//a[contains(text(),'Orders')]/parent::li/following-sibling::li[1]//a[contains(text(),'Sell Back')]"));
    this.orders = element(
      by.xpath(
        "//div[@class='userMenuWrapper']//a[contains(text(),'Orders')]"
      )
    );
    this.inbox = element(
      by.xpath("//div[@class='userMenuWrapper']//a[contains(text(),'Inbox')]")
    );
    this.boutique = element(
      by.xpath(
        "//div[@class='userMenuWrapper']//a[contains(text(),'Boutiques I Follow')]"
      )
    );
  }
}
