import { ElementFinder, element, by } from "protractor";

export class register {
  reebonzLink: ElementFinder;
  registerButton: ElementFinder;
  firstName: ElementFinder;
  lastName: ElementFinder;
  emailAddress: ElementFinder;
  password: ElementFinder;
  country: ElementFinder;
  submit: ElementFinder;
  registerTitle: ElementFinder;
  loginButton: ElementFinder;
  loginTitle: ElementFinder;
  loginEmail: ElementFinder;
  loginPassword: ElementFinder;
  loginSubmit: ElementFinder;
  userNameError: ElementFinder;
  passwordError: ElementFinder;
  UATURL: string;

  constructor() {
    this.UATURL = "https://uat-www.reebonz-dev.com/";
    this.registerButton = element(by.xpath("//a[@class='js-register']"));
    this.firstName = element(by.xpath("//input[@name='first_name']"));
    this.lastName = element(by.xpath("//input[@name='last_name']"));
    this.emailAddress = element(by.xpath("//input[@name='email']"));
    this.password = element(by.xpath("//input[@id='password']"));
    this.country = element(by.xpath("//select[@id='country']"));
    this.submit = element(by.xpath("//input[@value='Register with email']"));
    this.registerTitle = element(by.xpath("//h2[contains(text(),'Register')]"));
    this.loginButton = element(by.xpath("//a[@class='js-login']"));
    this.loginTitle = element(by.xpath("//h2[contains(text(),'Sign In')]"));
    this.loginEmail = element(by.xpath("//input[@id='username']"));
    this.loginPassword = element(by.xpath("//input[@id='password']"));
    this.loginSubmit = element(
      by.xpath("//input[@class='btn btn-sm js-submit']")
    );
    this.userNameError = element(by.xpath("//label[@id='username-error']"));
    this.passwordError = element(by.xpath("//label[@id='password-error']"));
  }
}
