import { ElementFinder, element, by } from "protractor";

export class myAccountPage {
  userName: ElementFinder;
  firstName: ElementFinder;
  lastName: ElementFinder;
  country: ElementFinder;
  language: ElementFinder;
  email: ElementFinder;
  phoneNumber: ElementFinder;
  gender: ElementFinder;
  saveProfileButton: ElementFinder;
  oldPassword: ElementFinder;
  newPassword: ElementFinder;
  confirmPassword: ElementFinder;
  saveChangePassword: ElementFinder;
  label: ElementFinder;
  constructor() {
    this.label = element(by.xpath("//h3[@class='inlineBlock']"));
    this.userName = element(
      by.xpath("//div[@class='userMenuWrapper']//h3//a[@class='userName']")
    );
    this.firstName = element(by.xpath("//input[@id='firstName']"));
    this.lastName = element(by.xpath("//input[@id='lastName']"));
    this.country = element(by.xpath("//input[@id='countryName']"));
    this.language = element(by.xpath("//select[@id='language']//option[1]"));
    this.email = element(by.xpath("//input[@id='Email']"));
    this.phoneNumber = element(by.xpath("//input[@id='TelephoneNumber']"));
    this.gender = element(by.xpath("//select[@id='gender']"));
    this.saveProfileButton = element(
      by.xpath("//button[@class='btn min150 saveProfile']")
    );
    this.oldPassword = element(by.xpath("//input[@id='oldPassword']"));
    this.newPassword = element(by.xpath("//input[@id='newPassword']"));
    this.confirmPassword = element(
      by.xpath("//input[@id='confirmNewPassword']")
    );
    this.saveChangePassword = element(
      by.xpath("//button[@class='btn min150 changePassword']")
    );
  }
}
