import { element, ElementFinder, by } from "protractor";

export class sellPage {
  sellPageMenu: ElementFinder;
  sellServiceSellNowButton: ElementFinder;
  sellServiceSellBackButton: ElementFinder;
  sellServiceWhiteGlovesButton: ElementFinder;
  benefitLabel: ElementFinder;
  sellToReebonzLabel: ElementFinder;
  sellBackLabel: ElementFinder;
  whiteGloveLabel: ElementFinder;
  sellbackHeader: ElementFinder;
  menuClearfix: ElementFinder;

  firstProduct: ElementFinder;
  sellToReebonzPDP: ElementFinder;
  conditionModal: ElementFinder;
  firstCondition: ElementFinder;
  sellNowButtonConditionModal: ElementFinder;
  itemSummaryLabel: ElementFinder;
  dropOffOption: ElementFinder;
  dropOffDate: ElementFinder;
  dropOffTime: ElementFinder;
  phoneNumber: ElementFinder;
  termAndConditions: ElementFinder;
  sellNowButtonOnConfirmationPage: ElementFinder;
  sellSubmmitedLabel: ElementFinder;
  titleSellListingPage: ElementFinder;
  sellNote: ElementFinder;
  searchBox: ElementFinder;
  firstProductBrand: ElementFinder;
  sendInMethod:ElementFinder;

  constructor() {
    this.sellPageMenu = element(by.xpath("//a[@id='subMenu_sell']"));
    this.sellServiceSellNowButton = element(
      by.xpath(
        "//td[@class='active']//a[@class='btn btnFull white'][contains(text(),'SELL NOW')]"
      )
    );
    this.sellServiceSellBackButton = element(by.xpath("//td[3]//div[1]//a[1]"));
    this.sellServiceWhiteGlovesButton = element(
      by.xpath("//td[4]//div[1]//a[1]")
    );
    this.benefitLabel = element(by.xpath("//h3[contains(text(),'Benefits')]"));
    this.sellToReebonzLabel = element(
      by.xpath(
        "//div[@class='headerTable active']//h3[contains(text(),'SELL TO REEBONZ')]"
      )
    );
    this.sellBackLabel = element(
      by.xpath("//div[@class='headerTable']//h3[contains(text(),'SELL BACK')]")
    );
    this.whiteGloveLabel = element(
      by.xpath(
        "//div[@class='headerTable']//h3[contains(text(),'WHITE GLOVE')]"
      )
    );
    this.sellbackHeader = element(
      by.xpath("//div[@id='header']//div[3]//div[1]//a[1]//img[1]")
    );
    this.menuClearfix = element(
      by.xpath("//div[@class='chooseSell']//div[@class='clearfix']")
    );
    this.firstProduct = element(
      by.xpath(
        "//div[@class='row products sellCatalogList js-productWrapper']//div[1]//div[1]//div[1]//div[1]//a[1]//img[1]"
      )
    );
    this.sellToReebonzPDP = element(
      by.xpath("//button[@class='btn sellToUs js-sellToUs fullWidth']")
    );
    this.conditionModal = element(
      by.xpath("//div[@class='modal-body sellToUsModal']")
    );
    this.firstCondition = element(
      by.xpath(
        "//div[@class='modal-body sellToUsModal']//div[2]//label[1]//input[1]"
      )
    );
    this.sellNowButtonConditionModal = element(
      by.xpath("//button[@class='btn js-sellbackSubmit']")
    );
    this.itemSummaryLabel = element(
      by.xpath("//h5[@class='accordionForMobile']")
    );
    this.dropOffOption = element(
      by.xpath(
        "//div[@class='col-md-8 leftSide']//div[1]//div[1]//div[1]//label[1]//input[1]"
      )
    );
    this.dropOffDate = element(
      by.xpath(
        "//form[@id='js-shippingform-drop_off']//select[@name='shippingDate']"
      )
    );
    this.dropOffTime = element(
      by.xpath(
        "//form[@id='js-shippingform-drop_off']//select[@name='shippingTime']"
      )
    );
    this.phoneNumber = element(
      by.xpath("//input[@placeholder='Mobile Number']")
    );
    this.termAndConditions = element(by.xpath("//input[@name='agreement']"));
    this.sellNowButtonOnConfirmationPage = element(
      by.xpath(
        "//div[@class='buttonWrapper hidden-xs']//div[@class='btn js-sellbackSubmit'][contains(text(),'Sell Now')]"
      )
    );
    this.sellSubmmitedLabel = element(
      by.xpath("//h3[contains(text(),'SELL SUBMITTED')]")
    );
    this.titleSellListingPage = element(by.xpath("//h1[@class='txtTitle']"));
    this.sellNote = element(by.xpath("//div[@class='sellNote']"));
    this.searchBox = element(by.xpath("//input[@id='query']"));
    this.firstProductBrand = element(
      by.xpath(
        "//div[@class='row products sellCatalogList js-productWrapper']//div[1]//div[1]//div[1]//div[2]//div[1]"
      )
    );
    this.sendInMethod=element(by.xpath("//input[@value='send_in']"))
  }
}
