import { ElementFinder, element, by } from "protractor";

export class checkoutPage {
  checkoutUrl: string;
  checkCredits: ElementFinder;
  addCoupon: ElementFinder;
  selectPaymentMethod: ElementFinder;
  creditDebitCard: ElementFinder;
  IPP: ElementFinder;
  paypal: ElementFinder;
  banktransfer: ElementFinder;
  saveMethodButton: ElementFinder;
  policy: ElementFinder;
  confirmAndPayButton: ElementFinder;
  checkoutTitle: ElementFinder;
  changeButton: ElementFinder;
  cardNumber: ElementFinder;
  expiryDate: ElementFinder;
  securityCode: ElementFinder;
  cardHolderName: ElementFinder;
  alertPopup: ElementFinder;
  closePopup: ElementFinder;
  SubTotal: ElementFinder;
  totalCheckout: ElementFinder;
  creditApply: ElementFinder;
  discountCode: ElementFinder;
  discountCodeLess: ElementFinder;
  shippingFee: ElementFinder;
  shoppingBag: ElementFinder;

  constructor() {
    this.checkCredits = element(by.xpath("//input[@class='js-credits']"));
    this.addCoupon = element(
      by.xpath("//a[@class='addNow underline textGrey js-addPromoCode']")
    );
    this.selectPaymentMethod = element(
      by.xpath(
        "//a[@class='btn btn-xs changeBtn hidden-xs btnTextOnly gold js-showPaymentBox']"
      )
    );
    this.creditDebitCard = element(
      by.xpath("//div[@id='headingCreditCard']//input[@name='paymentType']")
    );
    this.IPP = element(
      by.xpath("//div[@id='headingIPP']//input[@name='paymentType']")
    );
    this.paypal = element(
      by.xpath("//div[@id='headingIPP']//input[@name='paymentType']")
    );
    this.banktransfer = element(
      by.xpath("//div[@id='headingBankTransfer']//input[@name='paymentType']")
    );
    this.saveMethodButton = element(
      by.xpath("//a[@class='btn js-paymentMethodBtn']")
    );
    this.policy = element(by.xpath("//input[@class='js-policy']"));
    this.checkoutUrl = "https://uat-www.reebonz-dev.com/sg/checkout";
    this.checkoutTitle = element(by.xpath("//h1[contains(text(),'Checkout')]"));
    this.confirmAndPayButton = element(
      by.xpath("//a[@class='btn js-placeOrder']")
    );
    this.changeButton = element(
      by.xpath(
        "//a[@class='btn btn-xs changeBtn hidden-xs btnTextOnly gold js-showPaymentBox']"
      )
    );
    this.cardNumber = element(by.xpath("//input[@id='cc_number']"));
    this.expiryDate = element(by.xpath("//input[@id='cc_expiry_date']"));
    this.securityCode = element(by.xpath("//input[@id='cc_cvc']"));
    this.cardHolderName = element(by.xpath("//input[@id='cc_name']"));
    this.alertPopup = element(by.xpath("//div[@class='alertWrapper']"));
    this.closePopup = element(by.xpath("//div[@class='alertWrapper']//a"));
    this.SubTotal = element(
      by.xpath("//div[@class='col-xs-7 alignR uppercase']//strong[1]")
    );
    this.totalCheckout = element(
      by.xpath("//div[@class='col-xs-7 alignR']//strong[1]")
    );
    this.creditApply = element(
      by.xpath("//div[@class='col-xs-7 alignR availableCredits']")
    );
    this.discountCode = element(
      by.xpath(
        "//li[@class='coupon']//div[@class='row']//div[@class='col-xs-7 alignR']"
      )
    );
    this.shippingFee = element(
      by.xpath("//li[@class='shipping']//div//div[@class='col-xs-7 alignR']")
    );
    this.shoppingBag = element(
      by.xpath("//span[@class='cartCount js-cartCount']")
    );
  }
}
