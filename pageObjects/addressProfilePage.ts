import { ElementFinder, element, by } from "protractor";

export class addressProfilePage {
  addNewAddressButton: ElementFinder;
  fullName: ElementFinder;
  address: ElementFinder;
  selectCountry: ElementFinder;
  postalCode: ElementFinder;
  phoneNumber: ElementFinder;
  submitButton: ElementFinder;
  addAddressProfilePageUrl: string;
  assertName: ElementFinder;
  ProfileUrl: string;
  emailProfile: ElementFinder;
  cityName: ElementFinder;
  creditProfileUrl: string;

  addressLabel: ElementFinder;
  subName: ElementFinder;
  subAddress: ElementFinder;
  subCountry: ElementFinder;
  subPostCode: ElementFinder;
  subPhone: ElementFinder;
  addNewAddressButtonExist: ElementFinder;

  constructor() {
    this.addAddressProfilePageUrl =
      "https://staging-www.reebonz-dev.com/sg/account/addresses";
    this.addNewAddressButton = element(
      by.xpath(
        "//a[@class='btn btn-xs btnTextOnly gold addNewAddress js-showAddressBox']"
      )
    );
    this.fullName = element(by.xpath("//input[@id='address_name']"));
    this.address = element(by.xpath("//input[@id='address_Line1']"));
    this.selectCountry = element(by.xpath("//input[@name='address_Line1']"));
    this.postalCode = element(by.xpath("//input[@id='address_PostalCode']"));
    this.phoneNumber = element(by.xpath("//input[@id='TelephoneNumber']"));
    this.submitButton = element(by.xpath("//input[@id='addressButton']"));
    this.assertName = element(by.xpath("//span[@class='js-Name']"));
    this.ProfileUrl = "https://staging-www.reebonz-dev.com/sg/account/profile";
    this.emailProfile = element(by.xpath("//input[@id='Email']"));
    this.cityName = element(by.xpath("//input[@id='address_TownOrCity']"));
    this.creditProfileUrl =
      "https://uat-www.reebonz-dev.com/sg/account/credits";
    this.addressLabel = element(by.xpath("//h3[contains(text(),'Addresses')]"));
    this.addNewAddressButtonExist = element(
      by.xpath(
        "//a[@class='btn btn-xs btnTextOnly gold addNewAddress js-showAddressBox']"
      )
    );
    this.subName = element(
      by.xpath("//div[@class='addressDetail']//span[@class='js-Name']")
    );
    this.subAddress = element(
      by.xpath("//div[@class='addressDetail']//span[@class='js-Line1']")
    );
    this.subCountry = element(
      by.xpath("//div[@class='addressDetail']//span[@class='js-CountryName']")
    );
    this.subPostCode = element(
      by.xpath("//div[@class='addressDetail']//span[@class='js-PostalCode']")
    );
    this.subPhone = element(
      by.xpath("//div[@class='addressDetail']//span[@class='js-ContactNumber']")
    );
  }
}
