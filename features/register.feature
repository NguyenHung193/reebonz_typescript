@registerAndAddress
Feature: Checking register function and UI

    Scenario Outline: register successful functionality testing for "<countries>"

        Given I will navigate to Reebonz site
        When I click on register button
        Then The register popup is displayed
        When I enter FirstName
        And I enter lastName "<LastName>"
        And I enter email "<emailAddress>"
        And I enter password
        And I select "<countries>"
        And I click on Register button
        Then I register successful

        Given I logged in successful
        When I am navigate to addAddress page
        And I click on add new address button
        And I enter fullname
        And I enter address
        And I enter country
        And I enter postcode "<countries>"
        And I enter phonenumber
        And I enter city "<countries>"
        And I click on submit button
        Then I add new address successful - Assert "<LastName>"

        Examples:
            | countries     | LastName | emailAddress |
            | Singapore     | SG       | SG           |
            | Taiwan        | TW       | TW           |
            | Australia     | AU       | AU           |
            | Japan         | JP       | JP           |
            | Hong Kong     | HK       | HK           |
            | Malaysia      | MY       | MY           |
            | Thailand      | TH       | TH           |
            | United States | US       | US           |
            | Argentina     | AR       | AR           |
            | Macau         | MO       | MO           |
# Scenario: I am going to add shipment address

# Given I logged in successful
# When I am navigate to addAddress page
# And I click on add new address button
# And I enter fullname
# And I enter address
# And I enter country
# And I enter postcode
# And I enter phonenumber
# And I click on submit button
# Then I add new address successful


