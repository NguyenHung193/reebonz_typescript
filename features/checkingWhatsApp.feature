@checkingWhatsApp
Feature: Checking WhatsApp

    Scenario Outline: Checking whatsapp for all "<country>"
        Given I will navigate to Reebonz site
        When I click on Login button
        Then The Login popup is displayed
        When I enter user "<email>" for "<country>"
        And I enter password - login page
        And I click on login submit
        Then I login successfully
        And I am checking WhatsApp based on "<country>"


        Examples:
            | email                       | country       |
            | bi.nguyen+sgsb2@reebonz.com | Singapore     |
            | bi.nguyen+hksb2@reebonz.com | HongKong      |
            | bi.nguyen+ausb2@reebonz.com | Australia     |
            | bi.nguyen+ussb2@reebonz.com | United States |
            | bi.nguyen+twsb2@reebonz.com | Taiwan        |
            | bi.nguyen+idsb2@reebonz.com | Indonesia     |
