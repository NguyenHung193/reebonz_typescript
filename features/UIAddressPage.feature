@UIAddressPage
Feature: Checking UI Address page and adding address function

    @default
    Scenario Outline: Checking UI address page with exist users
        Given I will navigate to Reebonz site
        When I click on Login button
        Then The Login popup is displayed
        When I enter user "<email>" for "<country>"
        And I enter password - login page
        And I click on login submit
        Then I login successfully

        When I hover userName and click on address Popup
        Then I direct to my address page
        And I am checking exist name on Address page "<exist name>"
        And I am checking exist address on Address page "<exist address>"
        And I am checking exist country on Address page  "<exist country>"
        And I am checking post code on Address page "<exist post code>" for "<country>"
        And I am checking exist phone on Address page "<exist phone>"

        Examples:
            | email                       | country   | exist name | exist address | exist country | exist post code | exist phone |
            | bi.nguyen+sgsb2@reebonz.com | Singapore | Bi sgsb2   | 15 Ta My Duat | Singapore     | 3222            | 905495922   |
            | bi.nguyen+hksb2@reebonz.com | Hong Kong | Bi hksb2   | 15 Ta My Duat | Hong Kong     | 3222            | 905495922   |
            | bi.nguyen+ausb2@reebonz.com | Australia | Bi ausb2   | 15 Ta My Duat | Australia     | 3222            | 905495922   |

    @default
    Scenario Outline: Add address for new user based on "<countries>"

        Given I will navigate to Reebonz site
        When I click on register button
        Then The register popup is displayed
        When I enter FirstName
        And I enter lastName "<LastName>"
        And I enter email "<emailAddress>"
        And I enter password
        And I select "<countries>"
        And I click on Register button
        Then I register successful

        Given I logged in successful
        When I am navigate to addAddress page
        And I click on add new address button
        And I enter fullname
        And I enter address
        And I enter country
        And I enter postcode "<countries>"
        And I enter phonenumber
        And I enter city "<countries>"
        And I click on submit button
        Then I add new address successful - Assert "<LastName>"

        Examples:
            | countries     | LastName | emailAddress |
            | Singapore     | SG       | SGAddress    |
            | Taiwan        | TW       | TWAddress    |
            | Australia     | AU       | AUAddress    |
            | Japan         | JP       | JPAddress    |
            | Hong Kong     | HK       | HKAddress    |
            | Malaysia      | MY       | MYAddress    |
            | Thailand      | TH       | THAddress    |
            | United States | US       | USAddress    |
            | Argentina     | AR       | ARAddress    |
            | Macau         | MO       | MOAddress    |
