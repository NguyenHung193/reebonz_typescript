@UISellPage
Feature: Checking UI Sell page with existing user

      @default
      Scenario Outline: Checking UI Sell page with existing user
            Given I will navigate to Reebonz site
            When I click on Login button
            Then The Login popup is displayed
            When I enter user "<email>" for "<country>"
            And I enter password - login page
            And I click on login submit
            Then I login successfully

            When I hover on Sell Menu "<country>"
            And I click on Sell menu "<country>"
            Then I direct to Sell service page "<country>"
            And I assert Sell service element "<country>"

            When I click on SellNow button "<country>"
            And I direct to Sell product listing page "<country>"
            And I click on first product "<country>"
            And I direct to Sell product details page "<country>"
            And I click on Sell to Reebonz button "<country>"
            And I choose first Condition "<country>"
            And I click on Sell Now button on PDP "<country>"
            Then I direct to Sell confimation page "<country>"

            When I choose drop off method "<country>"
            And I select drop off date "<country>"
            And I select drop off time "<country>"
            And I enter phone number "<country>"
            And I select Terms and Conditions "<country>"
            And I click on Sell Now button on confirmation page "<country>"
            Then I direct to Sell submitted page "<country>"

            Examples:
                  | email                       | country   |
                  | bi.nguyen+sgsb2@reebonz.com | Singapore |
                  | bi.nguyen+hksb2@reebonz.com | Hong Kong |
                  | bi.nguyen+ausb2@reebonz.com | Australia |
                  | bi.nguyen+mysb2@reebonz.com | Malaysia  |
                  | bi.nguyen+idsb2@reebonz.com | Indonesia |

      @default
      Scenario Outline: Searching Sell products with anonymous user

            Given I will navigate to Reebonz site "<locale>"
            When I hover on Sell Menu "<country>"
            And I click on Sell menu "<country>"
            Then I direct to Sell service page "<country>"
            And I assert Sell service element "<country>"
            When I click on SellNow button "<country>"
            And I direct to Sell product listing page "<country>"
            And I enter "<Brand>" on search box "<country>"
            Then I direct to "<Brand>" page "<country>"

            Examples:
                  | email                       | country   | Brand | locale |
                  | bi.nguyen+sgsb2@reebonz.com | Singapore | FENDI | sg     |
                  | bi.nguyen+hksb2@reebonz.com | Hong Kong | FENDI | hk     |
                  | bi.nguyen+ausb2@reebonz.com | Australia | FENDI | au     |
                  | bi.nguyen+mysb2@reebonz.com | Malaysia  | FENDI | my     |
                  | bi.nguyen+idsb2@reebonz.com | Indonesia | FENDI | id     |