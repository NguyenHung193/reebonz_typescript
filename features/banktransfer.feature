@banktransfer
Feature: I am going to register new account

    Scenario: register successful functionality testing

        Given I will navigate to Reebonz site
        When I click on register button
        Then The register popup is displayed
        When I enter FirstName
        And I enter LastName
        And I enter email address
        And I enter password
        And I select country
        And I click on Register button
        Then I register successful

    Scenario: I am going to add shipment address

        Given I logged in successful
        When I am navigate to addAddress page
        And I click on add new address button
        And I enter fullname
        And I enter address
        And I enter country
        And I enter postcode
        And I enter phonenumber
        And I click on submit button
        Then I add new address successful

    Scenario: Adding some items from women bags page
        Given Direct to woman bags page
        When Click on product
        And Direct to product details page and check basketQty
        And Add product into basketQty
        And Hover over basket icon
        And Click on checkout button on popup
        Then Direct to checkout page

    Scenario: Checkout basket by Banktransfer method
        Given Direct to checkout page
        When Select payment method
        And Choose Banktransfer method
        And Save payment method
        And Apply policy
        And Click on Comfirm and Pay button
        Then Payment successful