@UIUserPopup
Feature: Checking UI on User popup

      @default
      Scenario Outline: Checking UI user popup with exist users
            Given I will navigate to Reebonz site
            When I click on Login button
            Then The Login popup is displayed
            When I enter user "<email>" for "<country>"
            And I enter password - login page
            And I click on login submit
            Then I login successfully

            When I hover userName
            Then I am checking order
            And I am checking sellback based on "<country>"
            And I am checking white glove based on "<country>"
            And I am checking credits
            And I am checking wishlist
            And I am checking address
            And I am checking inbox
            And I am checking boutique | follow



            Examples:
                  | email                       | country   |
                  | bi.nguyen+sgsb2@reebonz.com | Singapore |
                  | bi.nguyen+hksb2@reebonz.com | Hong Kong |
                  | bi.nguyen+ausb2@reebonz.com | Australia |