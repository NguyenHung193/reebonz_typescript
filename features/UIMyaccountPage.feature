@UIMyAccountPage
Feature: Checking UI myaccount page

      @default
      Scenario Outline: Checking UI myaccount page
            Given I will navigate to Reebonz site
            When I click on Login button
            Then The Login popup is displayed
            When I enter user "<email>" for "<country>"
            And I enter password - login page
            And I click on login submit
            Then I login successfully

            When I hover userName and click on userName Popup
            Then I direct to my account page
            And I am checking fistName on Myaccount page "<fistName>"
            And I am checking lastName on Myaccount page "<lastName>"
            And I am checking country on Myaccount page  "<country>"
            And I am checking language on Myaccount page "<language>"
            And I am checking email on Myaccount page "<email>"

            Examples:
                  | email                       | country   | fistName | lastName | language |
                  | bi.nguyen+sgsb2@reebonz.com | Singapore | Bi       | sgsb2    | en       |
                  | bi.nguyen+hksb2@reebonz.com | Hong Kong | Bi       | hksb2    | en       |
                  | bi.nguyen+ausb2@reebonz.com | Australia | Bi       | ausb2    | en       |