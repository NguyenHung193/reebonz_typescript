@checkoutUsingCreditCard
Feature: Checking out using credits card

   @default
   Scenario Outline: register successful functionality testing for "<countries>"

      Given I will navigate to Reebonz site
      When I click on register button
      Then The register popup is displayed
      When I enter FirstName
      And I enter lastName "<LastName>"
      And I enter email "<emailAddress>"
      And I enter password
      And I select "<countries>"
      And I click on Register button
      Then I register successful

      # Given I will navigate to Reebonz site
      #    When I click on Login button
      #    Then The Login popup is displayed
      #    When I enter email user "<emailAddress>"
      #    And I enter password - login page
      #    And I click on login submit
      #    Then I login successfully


      Given I logged in successful
      When I am navigate to addAddress page
      And I click on add new address button
      And I enter fullname
      And I enter address
      And I enter country
      And I enter postcode "<countries>"
      And I enter phonenumber
      And I enter city "<countries>"
      And I click on submit button
      Then I add new address successful - Assert "<LastName>"

      #  Given Direct to Titan page
      #     Then I click on Login button at Titan page
      #     And I enter email for Titan
      #     And I click on next button
      #     And I enter password for Titan
      #     And I click on next button
      #     And I enter email address to add credits
      #     And I click on search button on Titan page
      #     And I click on add button on Titan page
      #     Then the add credit popup is shown
      #     And I select Category on Titan page
      #     And I add full credits on Titan page
      #     And I click on submit credits button
      #     Then I add credits successful

      Given Direct to woman bags page
      When Click on product
      And Direct to product details page and check basketQty
      And Add product into basketQty
      And Hover over basket icon
      And Click on checkout button on popup
      Then Direct to checkout page

      Given Direct to checkout page
      When Select payment method
      And Choose CreditCard method
      And I enter CardNumber
      And I enter expiry date
      And I enter security code
      And I enter cardholder name
      And Save payment method
      And Apply policy
      And Click on Comfirm and Pay button
      Then Payment successful

      Examples:
         | countries     | LastName | emailAddress |
         | Singapore     | SG       | SGCCard      |
         | Taiwan        | TW       | TWCCard      |
         | Australia     | AU       | AUCCard      |
         | Hong Kong     | HK       | HKCCard      |
         | Malaysia      | MY       | MYCCard      |
         | United States | US       | USCCard      |
         | Macau         | MO       | MOCCard      |


