@loginFunction
Feature: Checking login function and UI


      Scenario Outline: Checking login successful with exist users
            Given I will navigate to Reebonz site
            When I click on Login button
            Then The Login popup is displayed
            When I enter user "<email>" for "<country>"
            And I enter password - login page
            And I click on login submit
            Then I login successfully

            Examples:
                  | email                       | country   |
                  | bi.nguyen+sgsb2@reebonz.com | Singapore |
                  | bi.nguyen+hksb2@reebonz.com | HongKong  |
                  | bi.nguyen+ausb2@reebonz.com | Australia |


      Scenario: Checking login fail with null data
            Given I will navigate to Reebonz site
            When I click on Login button
            Then The Login popup is displayed
            And I click on login submit
            Then It shows nameError
            And It shows passwordError