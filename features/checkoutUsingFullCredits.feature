@checkoutUsingFullCredits
Feature: Checking out using full credits and UI

      # Scenario Outline: checkout fullcredits with new user for "<countries>"

      # Given I will navigate to Reebonz site
      # When I click on register button
      # Then The register popup is displayed
      # When I enter FirstName
      # And I enter lastName "<LastName>"
      # And I enter email "<emailAddress>"
      # And I enter password
      # And I select "<countries>"
      # And I click on Register button
      # Then I register successful

      # # Given I will navigate to Reebonz site
      # #    When I click on Login button
      # #    Then The Login popup is displayed
      # #    When I enter email user "<emailAddress>"
      # #    And I enter password - login page
      # #    And I click on login submit
      # #    Then I login successfully

      # Given I logged in successful
      #     When I am navigate to addAddress page
      #     And I click on add new address button
      #     And I enter fullname
      #     And I enter address
      #     And I enter country
      #     And I enter postcode "<countries>"
      #     And I enter phonenumber
      #     And I enter city "<countries>"
      #     And I click on submit button
      #     Then I add new address successful - Assert "<LastName>"

      #  Given Direct to Titan page
      #     Then I click on Login button at Titan page
      #     And I enter email for Titan
      #     And I enter password for Titan
      #     And I enter email address to add credits "<emailAddress>"
      #     And I click on search button on Titan page
      #     And I click on add button on Titan page
      #     Then the add credit popup is shown
      #     And I select Category on Titan page
      #     And I add full credits on Titan page
      #     And I click on submit credits button
      #     Then I add credits successful

      # Given Direct to woman bags page
      #    When Click on product
      #    And Direct to product details page and check basketQty
      #    And Add product into basketQty
      #    And Hover over basket icon
      #    And Click on checkout button on popup
      #    Then Direct to checkout page

      # Given Direct to checkout page
      #    And Apply credits
      #    And Apply policy
      #    And Click on Comfirm and Pay button
      #    Then Payment successful

      # Examples:
      # | countries | LastName| emailAddress|
      # | Singapore  | SG| SG |
      # | Taiwan  | TW | TW |
      # | Australia  | AU | AU|
      # | Hong Kong | HK | HK |
      # | Malaysia | MY | MY |
      # | Thailand | TH | TH |
      # | United States | US | US |
      # | Argentina | AR | AR |
      # | Macau | MO | MO |

      Scenario Outline: checkout fullcredits with existing user for "<country>"

            Given I will navigate to Reebonz site
            When I click on Login button
            Then The Login popup is displayed
            When I enter user "<email>" for "<country>"
            And I enter password - login page
            And I click on login submit
            Then I login successfully

            Given Direct to woman bags page
            When Click on product
            And Direct to product details page and check basketQty
            And Add product into basketQty
            And Hover over basket icon
            And Click on checkout button on popup
            Then Direct to checkout page

            Given Direct to checkout page
            And Apply credits
            And Apply policy
            And Click on Comfirm and Pay button
            Then Payment successful

            Examples:
                  | email                       | country   |
                  | bi.nguyen+sgsb2@reebonz.com | Singapore |
                  | bi.nguyen+hksb2@reebonz.com | HongKong  |
                  | bi.nguyen+ausb2@reebonz.com | Australia |