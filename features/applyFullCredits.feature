@fullCredits
Feature: I am going to register new account

    Scenario: register successful functionality testing

        Given I will navigate to Reebonz site
        When I click on register button
        Then The register popup is displayed
        When I enter FirstName
        And I enter LastName
        And I enter email address
        And I enter password
        And I select country
        And I click on Register button
        Then I register successful

    Scenario: I am going to add shipment address

        Given I logged in successful
        When I am navigate to addAddress page
        And I click on add new address button
        And I enter fullname
        And I enter address
        And I enter country
        And I enter postcode
        And I enter phonenumber
        And I click on submit button
        Then I add new address successful

    Scenario: I am going to add credits at Titan page
        Given Direct to Titan page
        Then I click on Login button at Titan page
        And I enter email for Titan
        And I click on next button
        And I enter password for Titan
        And I click on next button
        And I enter email address to add credits
        And I click on search button on Titan page
        And I click on add button on Titan page
        Then the add credit popup is shown
        And I select Category on Titan page
        And I add full credits on Titan page
        And I click on submit credits button
        Then I add credits successful

    Scenario: Adding some items from women bags page
        Given Direct to woman bags page
        When Click on product
        And Direct to product details page and check basketQty
        And Add product into basketQty
        And Hover over basket icon
        And Click on checkout button on popup
        Then Direct to checkout page

    Scenario: Checkout basket by Banktransfer method
        Given Direct to checkout page
        When Select payment method
        And Choose Banktransfer method
        And Save payment method
        And Apply credits
        And Apply policy
        And Click on Comfirm and Pay button
        Then Payment successful