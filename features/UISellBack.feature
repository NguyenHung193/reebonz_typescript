@UISellbackWithNewUser
Feature: I am going to check UI sellback page with new user

    @default
    Scenario Outline: I am going to check UI sellback page with new user based on "<countries>"

        Given I will navigate to Reebonz site
        When I click on register button
        Then The register popup is displayed
        When I enter FirstName
        And I enter lastName "<LastName>"
        And I enter email "<emailAddress>"
        And I enter password
        And I select "<countries>"
        And I click on Register button
        Then I register successful

        Given I logged in successful
        When I hover on user
        And I click on Sellback item
        Then I am on Sellback page
        And I am checking elements on My items page
        When I go to Submmited items page
        Then I am checking elements on Submmited items page


        Examples:
            | countries     | LastName | emailAddress |
            | Singapore     | SG       | SG           |
            | Taiwan        | TW       | TW           |
            | Australia     | AU       | AU           |
            | Japan         | JP       | JP           |
            | Hong Kong     | HK       | HK           |
            | Malaysia      | MY       | MY           |
            | Thailand      | TH       | TH           |
            | United States | US       | US           |
            | Argentina     | AR       | AR           |
            | Macau         | MO       | MO           |